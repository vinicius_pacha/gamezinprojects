﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public class RoomEnemyPattern : MonoBehaviour
{
    public int minLevel;
    public string[] order = new string[] {"EnemyLevel1","EnemyLevel2","EnemyweaponLevel1","BlackEnvironment","EnemyLevel3","EnemyweaponLevel2","EnemyweaponLevel3"};
        
    [Button]
    public void GetMinLevel()
    {
        int minLv = 0;

        int children = transform.childCount;
        for (int i = 0; i < children; ++i)
        {
            var goName = transform.GetChild(i).gameObject.name;
            int j = 0;
            foreach (var s in order)
            {
                Debug.Log(goName);
                if (goName.Contains(s))
                {
                    Debug.Log(j);
                    break;
                    
                }

                j++;

            }

            if (j > minLv)
                minLv = j;
        }

        this.minLevel = 1 + (int)(minLv*2.5f);

    }

}
