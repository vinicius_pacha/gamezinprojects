﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEditor;
using UnityEngine;

public class ButtonObj : MonoBehaviour
{
    public Door door;
    public bool activated = false;
    public Jun_TweenRuntime tweenRuntime;
    public TweenShader ts;
    private float value = 0;
    public bool oneTime = false;
    public LayerMask contactMask;

    private void FixedUpdate()
    {
        if(activated && oneTime) return;
        
        var cols = Physics.OverlapSphere(transform.position, 1, contactMask);
        int collisions = 0;
        foreach (var c in cols)
        {
            if (!c.isTrigger)
            {
                collisions++;
                break;
            }
                
        }
        activated = collisions > 0;
        if (oneTime)
            enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (activated)
        {
            value += Time.deltaTime*5;
        }
        else
        {
            value -= Time.deltaTime*5;
        }

        door.open = activated;
        value = Mathf.Clamp01(value);
        tweenRuntime.previewValue = value;
        ts.SampleAt(value);
    }

}
