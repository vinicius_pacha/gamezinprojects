﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using BitStrap;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BreakableObject : InteractableObject
{
    public ParticleSystem ps;
    public float life = 2;
    public Image lifeImage;
    private float initialLife;
    public UnityEvent onBreak;
    void SetLifeImage()
    {
        if (canInteract)
        {
            
            float lifeValue = life / initialLife;
            lifeImage.fillAmount = lifeValue;
        
            lifeImage.transform.parent.gameObject.SetActive(lifeValue < 0.99f);
        }
            
    }

    private void Awake()
    {
        initialLife = life;
    }

    public override void Logic()
    {
        base.Logic();
        SetLifeImage();
        if ( squad.Count > 0)
        {
            
            float count = squad.Count;
            float length = places.Count;
            var damageVelocity = count / length;
            life -= Time.deltaTime * damageVelocity;

            for(int i =0; i<squad.Count; i++)
            {
                squad[i].squadState = Squad.SquadState.attacking;
            }
            
            if (life < 0 && canInteract)
            {
                UnparentSquads();
                canInteract = false;
                ps.transform.parent = null;
                ps.transform.localScale  = Vector3.one;
                ps.Play();
                onBreak.Invoke();
                gameObject.SetActive(false);
            }
        }
    }
}