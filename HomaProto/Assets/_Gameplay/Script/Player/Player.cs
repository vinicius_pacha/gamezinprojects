﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using BitStrap;
using ControlFreak2;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

public class Player : BitStrap.Singleton<Player>
{
    public PlayerRagdoll ragdoll;
    public PlayerAnimator playerAnimator;
    public PlayerState currentPlayerState;
    public TrailRenderer trail;
    public Rigidbody rbd;
    [FormerlySerializedAs("material")] 
    public Material playerMaterial;
    public Material playerUsedMaterial;
    public int squadCounter = 2;
    public float delayToTrigger;

    public Squad squadPrefab;
    public List<Squad> squad = new List<Squad>();
    public enum PlayerState
    {
        MENU,
        GAME,
        VICTORY,
        DEAD
    }

    
    private void Start()
    {
        if (PachaGamesManager.instance != null)
        {
            MenuWindow.instance.onShow.Register(Reset);
            InGameWindow.instance.onShow.Register(StartGame);
        }
        
        Application.targetFrameRate = 60;
    }
    void Reset()
    {
        agent.enabled = false;
        transform.position = Vector3.zero;
        agent.SetDestination(transform.position);
        rbd.velocity = Vector3.zero;
        playerAnimator.playerAnimator.SetBool("Dancing", false );
        trail.Clear();
        trail.enabled = false;
        Invoke("CheckMinions",0.2f);
        
        Revive();
    }

    void CheckMinions()
    {
        agent.enabled = true;
        foreach (var s in squad)
        {
            s.agent.enabled = false;
            s.transform.position = Vector3.zero;
            s.agent.enabled = true;
        }
        
        for (int i = squad.Count; i < UpgradeStatus.Instance.Unwrap().startMinionsLevel-1; i++)
        {
            var s = Instantiate(squadPrefab, Vector3.zero, Quaternion.identity);
            squad.Add(s);
        }
    }
    void StartGame()
    {
        CheckMinions();
        playerAnimator.playerAnimator.SetBool("Dancing", false );
        currentPlayerState = PlayerState.GAME;
        transform.position = Vector3.zero;
        agent.SetDestination(transform.position);
        agent.ResetPath();
        trail.enabled = true;
        agent.enabled = true;
    }
    [Button]
    public void Dead()
    {
        if (PachaGamesManager.instance != null &&
            PachaGamesManager.instance.currentGameState == PachaGamesManager.GameState.INGAME)
        {
            
            currentPlayerState = PlayerState.DEAD;
            rbd.isKinematic = true;
            ragdoll.StartRagdoll();
            PachaGamesManager.instance.Death();
        }

    }  
    [Button]
    void Revive()
    {
        currentPlayerState = PlayerState.MENU;
        transform.position = Vector3.zero;
        ragdoll.ragdollManager.blendTime = 0;
        ragdoll.ragdollManager.blendToMecanim();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("FinishLine"))
        {
            CompleteLevel();
        } 
        if (other.gameObject.CompareTag("Deadly"))
        {
            Dead();
        }
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (delayToTrigger > 0) return;

        if (other.CompareTag("DraggableObject"))
        {
            
            InteractableObject dObj = other.GetComponent<InteractableObject>();
            if(!dObj.canInteract) return;
            
            if (dObj.SquadNeeded() > 0)
            {
                if (squad
                    .Count > 0)
                {
                    var s = squad.OrderBy(x => Vector3.Distance(x.transform.position, other.transform.position))
                        .ToList()
                        .First(x => x.squadState == Squad.SquadState.activated).UnwrapOr(null);

                    if (s == null) return;
                    s.squadState = Squad.SquadState.carrying;
                    
                    s.ParentSquad(dObj.places[dObj.squad.Count],other.transform);
                    playerAnimator.playerAnimator.SetTrigger("Throw");
                    dObj.squad.Add(s);
                    delayToTrigger = 0.15f;
                }
            }
        }

        if (other.CompareTag("Brick"))
        {
            var b = other.GetComponent<Brick>();
                if(b.hasBeenPicked) return;
            var s = squad.OrderBy(x => Vector3.Distance(x.transform.position,other.transform.position)).ToList()
                .First(x => x.squadState == Squad.SquadState.activated).UnwrapOr(null);
            if (s == null) return;
            b.hasBeenPicked= true;

            s.squadState = Squad.SquadState.jumping;
            other.gameObject.tag = "Default";
            s.GetBrick(other.transform);
            playerAnimator.playerAnimator.SetTrigger("Throw");
            delayToTrigger = 0.15f;

        }
        
        if (other.CompareTag("Bridge"))
        {
            var bridge = other.transform.parent.GetComponent<Bridge>();

            var squads = squad.OrderBy(x => Vector3.Distance(x.transform.position, other.transform.position)).ToList();
            var s = squads.First(x => x.squadState == Squad.SquadState.carryingBrick).UnwrapOr(null);
            if (s == null) return;
            
            bridge.PlaceBrick(s.transform);
            s.squadState = Squad.SquadState.activated;
            playerAnimator.playerAnimator.SetTrigger("Throw");
            delayToTrigger = 0.15f;

        }
        
        if (other.CompareTag("Enemy"))
        {
            var enemy = other.transform.GetComponent<Enemy>();
            if(enemy.life <= 0 ) return;
            var squads = squad.OrderBy(x => Vector3.Distance(x.transform.position, other.transform.position)).ToList();
            var s = squads.First(x => x.squadState == Squad.SquadState.activated).UnwrapOr(null);
            if (s == null) return;
            enemy.life--;
            s.ThrowSquadOnEnemy(enemy);
            s.squadState = Squad.SquadState.jumping;
            playerAnimator.playerAnimator.SetTrigger("Throw");
            delayToTrigger = 0.15f;

        }
       
    }

    public NavMeshAgent agent;
    public float distance = 1;
    void WalkLogic()
    {
        float h = CF2Input.GetAxis("Horizontal");
        float v = CF2Input.GetAxis("Vertical");
        var input = new Vector2(h,v).normalized;
        var desiredPosition = transform.position + new Vector3(input.x, 0, input.y) * distance;
        if (input.magnitude>0.1f)
        {
            agent.isStopped=false;

            transform.LookAt(desiredPosition);
            agent.SetDestination(desiredPosition);
        }
        else
        {
            agent.isStopped=true;
        }
    }
    void CompleteLevel()
    {        
        currentPlayerState = PlayerState.VICTORY;
        playerAnimator.playerAnimator.SetBool("Dancing", true );

        StartCoroutine(StartMove());
       
    }

    public void KillBoss()
    {
        currentPlayerState = PlayerState.VICTORY;
        playerAnimator.playerAnimator.SetBool("Dancing", true );
    }
    
    public float angularMovement = 5;
    IEnumerator StartMove()
    {
        
        while (Vector3.Distance(transform.position, FinishLine.Instance.Unwrap().finalPosition.position) > 0.5f)
        {
            agent.SetDestination(FinishLine.Instance.Unwrap().finalPosition.position);
            transform.rotation = Quaternion.RotateTowards(transform.rotation,
                FinishLine.Instance.Unwrap().finalPosition.rotation, angularMovement * Time.deltaTime);
            yield return null;
        }

        agent.enabled = false;
        FinishLine.Instance.Unwrap().CompleteLevel();
        transform.rotation = Quaternion.Euler(0,180,0);
        yield return new WaitForSeconds(0.5f);
        if(PachaGamesManager.instance != null)
            PachaGamesManager.instance.Victory();
        
    }

    private void Update()
    {
        if(currentPlayerState == PlayerState.GAME)
            WalkLogic();
        if(transform.position.y <= -15)
            Dead();

        if (currentPlayerState == PlayerState.MENU)
        {
            transform.position=Vector3.zero;
            
        }

        delayToTrigger -= Time.deltaTime;
    }
    
}
