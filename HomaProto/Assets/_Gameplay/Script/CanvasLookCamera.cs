﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasLookCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.eulerAngles = Camera.main.transform.rotation.eulerAngles + new Vector3(-15,0,0);
        
    }
}
