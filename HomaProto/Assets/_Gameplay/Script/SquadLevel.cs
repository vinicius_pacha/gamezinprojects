using System.Collections.Generic;
using BitStrap;
using UnityEngine;

[System.Serializable]
public class SquadLevel : ScriptableObject
{
    public List<LevelRoom> rooms;
    
    [Button]
    void BuildLevel()
    {
        LevelBuilder.Instance.Unwrap().LoadLevel(this);
    }
    [Button]
    void ClearLevel()
    {
        LevelBuilder.Instance.Unwrap().ClearRoom();
    }
}