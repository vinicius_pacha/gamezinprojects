﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Monetization;

public class LevelBuilder : BitStrap.Singleton<LevelBuilder>
{
    public SquadLevel[] levels;
    public SquadLevel[] bossLevel;
    public List<LevelRoom> levelRooms = new List<LevelRoom>();
    public List<GameObject> instantiatedObjects = new List<GameObject>();
    private Vector3 lastRoomPosition;
    void Start()
    {
        MenuWindow.instance.onShow.Register(LoadLevel);
    }
    [Button]    

    public void LoadLevel()
    {
        var lvRest = (LevelManager.instance.level - 1)%5;
        var lvIndex = (LevelManager.instance.level - 1)/5;
        var currentSquadLevel = lvIndex * 4 + lvRest;
        if (currentSquadLevel >= levels.Length)
            currentSquadLevel = Random.Range(2, levels.Length);
        var currentLevel = levels[currentSquadLevel];
        
        if (lvRest == 4)
        {
            currentLevel = bossLevel[lvIndex % bossLevel.Length];
        }
        LoadLevel(currentLevel);
    }
    [Button]    
    public void LoadLevel(SquadLevel currentLevel)
    {
        ClearRoom();
        Vector3 roomDelta =Vector3.forward*4;
        lastRoomPosition = Vector3.forward*2;
        for (int i = 0; i < currentLevel.rooms.Count; i++)
        {
            LevelRoom room;
            room = Instantiate(currentLevel.rooms[i],lastRoomPosition,Quaternion.identity);
            levelRooms.Add(room);
            roomDelta = lastRoomPosition - room.initial.position;
            room.transform.position += roomDelta;
            if(room.final != null)
                lastRoomPosition = room.final.position + Vector3.forward*4;
            else
            {
                lastRoomPosition = new Vector3(500,500,500);
            }
            FinishLine.Instance.Unwrap().transform.position = lastRoomPosition;
            
        }

    }
    
    [Button]
    public void ClearRoom()
    {
        foreach (var lr in levelRooms)
        {
            lr.Clear();
            if (Application.isPlaying)
            {
                if(lr!=null)
                    Destroy(lr.gameObject);
            }
            else
            {
                if(lr!=null)
                    DestroyImmediate(lr.gameObject);
            }
        }

        levelRooms.Clear();
    }
    
}
