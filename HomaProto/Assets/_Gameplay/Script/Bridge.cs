﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;
using UnityEngine.AI;

public class Bridge : MonoBehaviour
{
    public int brickIndex = 0;

    public AnimationCurve yCurve;
    public float yValue;
    public List<GameObject> bricks = new List<GameObject>();
    public GameObject prefab;
    public NavMeshObstacle[] navMeshObstacles;
    void Update()
    {

    }
    [Button]
    public void PlaceBrick(Transform fromPosition)
    {
        GameObject go = Instantiate(prefab);
        bricks.Add(go);
        go.transform.position = fromPosition.position;
        StartCoroutine(RepositionBrick(go.transform));
    }

    public float yBridge = 0.85f;
    Vector3 GetBdrickIndexPosition(Transform brick)
    {
        int length = (int)transform.localScale.z;
        var line = brickIndex / 2;
        navMeshObstacles[line].enabled = false;
        var collumn = brickIndex % 2;
        var pos = Vector3.zero;
        pos.y = yBridge;
        pos.z = line - length / 2 + 0.5f;
        if (line % 2 == 0)
        {
            if (collumn == 0)
            {
                brick.localScale = new Vector3(1.9f,0.3f,0.9f);
                pos.x = -0.5f;
            }
            else
            {
                pos.x = 1;

                brick.localScale = new Vector3(0.9f,0.3f,0.9f);

            }
        }
        else
        {
            if (collumn == 0)
            {
                pos.x = -1;
                brick.localScale = new Vector3(0.9f,0.3f,0.9f);
            }
            else
            {
                pos.x = 0.5f;
                brick.localScale = new Vector3(1.9f,0.3f,0.9f);
            }
        }

        return transform.position + pos;
    }
    
    IEnumerator RepositionBrick(Transform brick)
    {
        float t = 0;
        Vector3 initialPosition = brick.transform.position;
        Vector3 finalPosition = GetBdrickIndexPosition(brick);
        brickIndex++;
        while (t < 1)
        {
            t+=Time.deltaTime*1.5f;
            brick.position = Vector3.Lerp(initialPosition,finalPosition,t);
            var tempPosition = brick.position;
            tempPosition.y = tempPosition.y + yCurve.Evaluate(t) * yValue;
            brick.position = tempPosition;
            yield return null;
        }

    }

    public void Clear()
    {
        foreach (var b in bricks)
        {
            if(b!=null)
                Destroy(b.gameObject);
            
        }
    }

}
