﻿Shader "Custom/Mobile/Diffuse X SelfIlumin Unlit"
{
    Properties
    {
        [HDR]_Color("Color",COLOR)=(0.5,0.5,0.5,1.0)
        [HDR]_ColorIlumin("Ilumin",COLOR)=(0.5,0.5,0.5,1.0)
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _IluminTex ("Base (RGB)", 2D) = "white" {}
		[HDR] _RimColor ("Rim Color", Color) = (0.6, 0.6, 0.6, 1.0)
        _RimPower ("Rim Power", Range(0.5,8.0)) = 1.0
    }
 
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 150
        CGPROGRAM
        #pragma surface surf Lambert noforwardadd
 
        sampler2D _MainTex;
        sampler2D _IluminTex;
        fixed4 _Color;
        fixed4 _ColorIlumin;
		
		float4 _RimColor;
        float _RimPower;

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_IluminTex;
			float3 viewDir;
        };
		
		void surf (Input IN, inout SurfaceOutput o) 
        {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			fixed4 i = tex2D(_IluminTex, IN.uv_IluminTex);
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));

            o.Alpha = c.a;
			o.Emission =  c.rgb + _ColorIlumin.rgb * i.rgb + _RimColor.rgb * pow(rim, _RimPower);
        }
        ENDCG
    }
    Fallback "Mobile/VertexLit"
}