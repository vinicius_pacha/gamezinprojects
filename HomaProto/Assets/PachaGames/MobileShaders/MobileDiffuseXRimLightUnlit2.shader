﻿Shader "Custom/Mobile/DiffuseX RimLight Unlit WAVEEE"
{
    Properties
    {
        [HDR]_Color("Color",COLOR)=(0.5,0.5,0.5,1.0)
        _MainTex ("Base (RGB)", 2D) = "white" {}
       [HDR] _RimColor ("Rim Color", Color) = (0.6, 0.6, 0.6, 1.0)
        _RimPower ("Rim Power", Range(0.5,8.0)) = 1.0 
		_Frequency ("Frequency", Range(0, 1000)) = 10
		_Amplitude ("Amplitude", Range(0, 5)) = 1
	    _WaveSpeed ("Wave Speed", Range(-100, 100)) = 1

		_WaveFalloff ("Wave Falloff", Range(1, 8)) = 4
		_MaxWaveDistortion ("Max Wave Distortion", Range(0.1, 10.0)) = 1

    }
 
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 150
        CGPROGRAM
        #pragma surface surf Lambert noforwardadd vertex:vert
 
        sampler2D _MainTex;
        fixed4 _Color;
		float4 _RimColor;
        float _RimPower;
        float _ControlTime;
        float _Frequency;
        float _Amplitude;

        float _WaveFalloff;
        float _WaveSpeed;
        float _MaxWaveDistortion;

		void vert (inout appdata_base v) {
			float4 origin = float4(0.0, 0.0, 0.0, 0.0);
			float4 world_space_vertex = mul(unity_ObjectToWorld, v.vertex);

			float dist = distance(v.vertex, origin);

			//Adjust our distance to be non-linear.
			dist = pow(dist, _WaveFalloff);

			//Set the max amount a wave can be distorted based on distance.
			dist = max(dist, _MaxWaveDistortion);
			
			v.vertex.xyz += v.normal * sin(v.vertex.y * _Frequency + _Time.y * _WaveSpeed) * _Amplitude * (1 / dist);		
		}

        struct Input
        {
            float2 uv_MainTex;
			float3 viewDir;
        };
 
        void surf (Input IN, inout SurfaceOutput o)
        {
            half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));

            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Emission = c.rgb + _RimColor.rgb * pow(rim, _RimPower);
			o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }
    Fallback "Mobile/VertexLit"
}