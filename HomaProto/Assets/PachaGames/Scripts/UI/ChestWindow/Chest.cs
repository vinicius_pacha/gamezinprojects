using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
[System.Serializable]
public class Reward{
    public enum RewardType
    {
        COIN,
        CUSTOMIZATION
    }

    public RewardType type;
    public int value;
}
public class Chest : MonoBehaviour
{
    public Button chestButton;

    public GameObject closedChest;
    public Jun_TweenRuntime openChest;
    public Jun_TweenRuntime coinWidget;
    public Jun_TweenRuntime customizationWidget;
    public TMP_Text coinValue;
    public Image chestCustomization;
    private Reward reward;
    private void Start()
    {
        CloseChest();
    }

    public bool CanOpen
    {
        get { return chestButton.interactable; }   
    }

    [Button]
    public void CloseChest()
    {
        chestButton.interactable = true;
        closedChest.SetActive(true);
        openChest.gameObject.SetActive(false);
        coinWidget.gameObject.SetActive(false);
        customizationWidget.gameObject.SetActive(false);

    }
    [Button]
    public void OpenChest()
    {
        if (((ChestWindow.instance) as ChestWindow).SpendKey())
        {
            chestButton.interactable = false;
            closedChest.SetActive(false);
            openChest.gameObject.SetActive(true);
            openChest.Play();
            ((ChestWindow.instance)as ChestWindow).onOpenChest.Call();
            AppearReward(reward);
        }
     
    }

    public void SetupReward(Reward reward)
    {
        this.reward = reward;
        switch (reward.type)
        {
            case Reward.RewardType.COIN:
                coinValue.text = string.Format("+{0}", reward.value);
                break;
            case Reward.RewardType.CUSTOMIZATION:
                chestCustomization.sprite = CustomizationManager.Instance.Unwrap().customizationData
                    .FirstOrDefault(x => x.id == reward.value).customizationObjectConfig.customizationSprite;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    void AppearReward(Reward reward)
    {
        switch (reward.type)
        {
            case Reward.RewardType.COIN:
                CameraShakeUI.Instance.Unwrap().SetSmallShake();

                coinWidget.gameObject.SetActive(true);
                coinWidget.Play();
                coinWidget.AddOnFinished(EarnCoinsFromReward);
                break;
            case Reward.RewardType.CUSTOMIZATION:
                CameraShakeUI.Instance.Unwrap().SetBigShake();

                customizationWidget.gameObject.SetActive(true);
                customizationWidget.Play();
                customizationWidget.AddOnFinished(AquireReward);
                break;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void AquireReward()
    {
        ChestManager.Instance.Unwrap().AquireCurrentReward(reward.value);
    }
    void EarnCoinsFromReward()
    {
        CoinManager.Instance.Unwrap().EarnCoins(reward.value);
        CoinsFeedback.Instance.Unwrap().EarnCoinFromPosition(reward.value, transform.position);
        coinWidget.ClearOnFinished();
    }
}
