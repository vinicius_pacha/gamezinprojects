using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CustomizationWindow : UiWindow
{
    public static UiWindow instance;
    public Image[] pages;
    public CustomizationWidget customizationWidgetPrefab;
    public int totalWidgets = 1;
    public List<CustomizationWidget> customizationWidgets = new List<CustomizationWidget>();
    public Transform customizationWidgetParent;

    public List<CustomizationClass> customizationClasses;
    [Header("private")] 
    public CustomizationCategory[] categories;
    public int currentCustomizationClass = 0;
    public int currentCustomizationPage = 0;
    public Jun_TweenRuntime pageTween;

    public CustomizationData[] datas;

    public GameObject next;
    public GameObject previous;
    
    public TMP_Text title;
    public GameObject unlockButtons;
    public TMP_Text unlockValue;
    public TMP_Text unlockDescription;
    public Button unlockRandom;
   
    public Color activePageColor;
    public Color notActivePageColor;
    public TMP_Text customizationClassTitle;

    public virtual void Awake()
    {
        instance = this;
        CreateWidgets();

    }

    public void CreateWidgets()
    {
        for (int i = 0; i < totalWidgets; i++)
        {
            var customizationWidget = Instantiate(customizationWidgetPrefab, customizationWidgetParent);
            customizationWidgets.Add(customizationWidget);
        }

        customizationWidgetPrefab.gameObject.SetActive(false);
    }

    public override void Show()
    {
        base.Show();
    }

    public void SetupClass(int classIndex)
    {
        if(currentCustomizationClass != classIndex)
            currentCustomizationPage = 0;

        customizationClassTitle.text = customizationClasses[classIndex].name;
        currentCustomizationClass = classIndex;
        categories = CustomizationManager.Instance.Unwrap().GetCategoriesByClass(customizationClasses[classIndex]);
        UpdateWidgets();
        CheckNextAndPrevious();
    }

    void UpdatePageObjects()
    {
        var pageQuantity = categories.Length;
        int i = 0;
        foreach (var pageImage in pages)
        {
            if(pageQuantity>0)
                pageImage.gameObject.SetActive(true);
            else
                pageImage.gameObject.SetActive(false);
            
            if (currentCustomizationPage == i)
                pages[i].color = activePageColor;
            else
                pages[i].color = notActivePageColor;
            
            pageQuantity--;
            i++;
        }

        if (CoinManager.Instance.Unwrap().desiredCoins <= categories[currentCustomizationPage].price)
        {
            unlockRandom.interactable = false;
        }
        else
        {
            var availableWidgets = customizationWidgets.Where(x => x.owned == 0 && x.gameObject.activeSelf).ToArray();
            
            if(availableWidgets.Length > 0)
                unlockRandom.interactable = true;
            else
                unlockRandom.interactable = false;

            
        }
    }



    public void UpdateDelayed(float seconds)
    {
        CancelInvoke("UpdateWidgets");
        Invoke("UpdateWidgets", seconds);
    }

    public void UpdateWidgets()
    {
        var category = categories[currentCustomizationPage];
        title.text = category.name;
        if (category.unlockType == CustomizationCategory.UnlockType.Random)
        {
            unlockButtons.SetActive(true);
            unlockValue.text = category.price.ToString();
            unlockDescription.gameObject.SetActive(false);
        }
        else
        {
            unlockButtons.SetActive(false);
            unlockDescription.gameObject.SetActive(true);
            unlockDescription.text = category.description;
        }
        
        datas = CustomizationManager.Instance.Unwrap()
            .GetCustomizationsByCategoriesAndByClass(category,customizationClasses[currentCustomizationClass]);

        if (datas.Length > customizationWidgets.Count)
        {
            Debug.LogWarning("THERE IS A CATEGORY WITH MORE ITEMS THAN THE GRID");    
        }
        
        for (int i = 0; i < customizationWidgets.Count; i++)
        {
            if (i < datas.Length)
            {
                customizationWidgets[i].gameObject.SetActive(true);
                customizationWidgets[i].Setup(datas[i]);
            }
            else
            {
                customizationWidgets[i].gameObject.SetActive(false);
            }
        }
        UpdatePageObjects();
        
    }

    [Button]
    public void NextPage()
    {
        if (pageTween.isPlaying) return;

        var page = currentCustomizationPage;
        currentCustomizationPage = Mathf.Clamp(currentCustomizationPage + 1, 0, categories.Length - 1);
        if (currentCustomizationPage != page)
            StartCoroutine(NextPageCoroutine());
        
        CheckNextAndPrevious();

    }

    IEnumerator NextPageCoroutine()
    {
        pageTween.Play();
        yield return new WaitForSeconds(pageTween.animationTime / 2.0f);
        UpdateWidgets();
    }

    IEnumerator PreviousPageCoroutine()
    {
        pageTween.Rewind();
        yield return new WaitForSeconds(pageTween.animationTime / 2.0f);
        UpdateWidgets();
    }

    [Button]
    public void PreviousPage()
    {
        if (pageTween.isPlaying) return;
        var page = currentCustomizationPage;

        currentCustomizationPage = Mathf.Clamp(currentCustomizationPage - 1, 0, categories.Length - 1);
        if (currentCustomizationPage != page)
            StartCoroutine(PreviousPageCoroutine());
        CheckNextAndPrevious();

    }

    [Button]
    public void GetRandomCustomization()
    {
        unlockRandom.interactable = false;
        CoinManager.Instance.Unwrap().EarnCoins(-categories[currentCustomizationPage].price);
        StartCoroutine(GetRandomCustomizationCoroutine());
    }

    IEnumerator GetRandomCustomizationCoroutine()
    {
        var availableWidgets = customizationWidgets.Where(x => x.owned == 0 && x.gameObject.activeSelf).ToArray();
        for (int i = 0; i < 10; i++)
        {
            availableWidgets[Random.Range(0, availableWidgets.Length - 1)].PlayGlowAnimation();
            yield return new WaitForSeconds(availableWidgets[0].ownedTween.animationTime / 3.0f);
        }

        availableWidgets[Random.Range(0, availableWidgets.Length - 1)].AquireCustomization();
        UpdatePageObjects();
    }

    void CheckNextAndPrevious()
    {
        next.SetActive(currentCustomizationPage+1 < categories.Length);
        previous.SetActive(currentCustomizationPage > 0);
    }

    public void GetCoinsByAd(int value)
    {
        AdManager.Instance.Unwrap().ShowRewardedVideo(() => EarnCoinsFromAd(value));
    }
    
    public void EarnCoinsFromAd(int value)
    {
        CoinManager.Instance.Unwrap().EarnCoins(value);
        CoinsFeedback.Instance.Unwrap().EarnCoinFromPosition(value, unlockButtons.transform.position);
        UpdatePageObjects();
    }

}
