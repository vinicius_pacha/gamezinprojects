using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using UnityEngine;

public class RewardManager : BitStrap.Singleton<RewardManager>
{
    public int previousReward = 0;
    public int reward = 0;
    
    public int Reward
    {
        get => reward;
        set
        {
            PlayerPrefs.SetInt(REWARD, value);
            reward = value;
        }
    }
    public int RewardIndex
    {
        get => rewardIndex;
        set
        {
            PlayerPrefs.SetInt(REWARD_INDEX, value);
            rewardIndex = value;
        }
    }

    public int rewardIndex = 0;
    public int firstReward = 0;
    public SafeAction<int> onRewardChange = new SafeAction<int>();
    public SafeAction onRewardFinishIncrease = new SafeAction();
    public int[] percentagePerLevel = new []{20,15,10}; 
    public int[] percentagePerLevelByAd = new []{20,15,10};

    const string REWARD = "PERCENTAGE";
    const string REWARD_INDEX = "REWARD_INDEX";

    public bool watchedRewardVideo;
    public int RewardIncrease
    {
        get { return percentagePerLevel[Mathf.Min(percentagePerLevel.Length - 1, rewardIndex)]; } 
    }
    public int RewardIncreaseByAd
    {
        get { return percentagePerLevelByAd[Mathf.Min(percentagePerLevelByAd.Length - 1, rewardIndex)]; } 
    }
    private void Start()
    {
        if (!PlayerPrefs.HasKey(REWARD))
        {
            PlayerPrefs.SetInt(REWARD, reward);
        }
        else
        {
            reward = PlayerPrefs.GetInt(REWARD);
        }
        if (!PlayerPrefs.HasKey(REWARD_INDEX))
        {
            PlayerPrefs.SetInt(REWARD_INDEX, rewardIndex);
        }
        else
        {
            rewardIndex = PlayerPrefs.GetInt(REWARD_INDEX);
        }
    }

    public void IncreaseReward()
    {
        previousReward = reward;
        Reward = Mathf.Min(Reward + RewardIncrease,100);
    }
    public void IncreaseRewardByAd()
    {
        previousReward = reward;
        Reward += RewardIncreaseByAd;
        
        if (WindowManager.instance.currentWindow is RewardWindow)
        {
            (RewardWindow.instance as RewardWindow).AnimateWithoutDelayReward();
        }
    }

    public void LoseReward()
    {
        Reward = 0;
        RewardIndex = RewardIndex + 1;
        PachaGamesManager.instance.LoadMenuWindow();

    }
    public CustomizationClass[] GetAllClassesForReward()
    {
        var a = CustomizationManager.Instance.Unwrap().customizationData.Where(x=> x.owned<1 && x.customizationObjectConfig.category.unlockType == CustomizationCategory.UnlockType.Progress).Select(x => x.customizationObjectConfig.customizationClass).Distinct().ToList();
        CustomizationClass[] newArray = a.OrderBy((x) => x.priority).ToArray();

        return newArray;
    }
    
    public CustomizationData GetCurrentReward()
    {
        CustomizationClass[] classes = GetAllClassesForReward();
        var classIndex = (RewardIndex + firstReward) % classes.Length;
        CustomizationClass rewardClass = classes[classIndex];
        var a = CustomizationManager.Instance.Unwrap().customizationData.First(x=> x.owned<1 && 
                                                                                   x.customizationObjectConfig.category.unlockType == CustomizationCategory.UnlockType.Progress &&
                                                                                   x.customizationObjectConfig.customizationClass == rewardClass);
        
        Debug.Log("CURRENT PROGRESSION REWARD");
        Debug.Log("CLASS:" + rewardClass.name);
        Debug.Log("CATEGORY:" + a.Unwrap().customizationObjectConfig.category.name);
        return a.Unwrap();
    }
    public void TryClaimReward()
    {
        AdManager.Instance.Unwrap().ShowRewardedVideo(ClaimReward);
    }

    public void ClaimReward()
    {
        Reward = 0;
        
        CustomizationManager.Instance.Unwrap().AquireCustomization(GetCurrentReward().id,true);

        RewardIndex = RewardIndex + 1;
        
        PachaGamesManager.instance.LoadMenuWindow();
    }

    public void GetRewardIncreaseByAd()
    {
        AdManager.Instance.Unwrap().ShowRewardedVideo(IncreaseRewardByAd);
    }
}
