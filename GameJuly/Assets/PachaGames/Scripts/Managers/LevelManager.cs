﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {
    public static LevelManager instance;
    public int level = 1;
    public BitStrap.SafeAction onIncreaseLevel = new BitStrap.SafeAction();
    public int currentObjective;
    public int finalObjective;
    void Awake ()
    {
        instance = this;
    }
    private void Start()
    {
        if (!PlayerPrefs.HasKey("LevelUnlocked"))
        {
            level = 1;
            PlayerPrefs.SetInt("LevelUnlocked", level);

        }
        else
        {
            level = PlayerPrefs.GetInt("LevelUnlocked", level);
        }
    }
    
    public void NextLevel()
    {
        level++;
        onIncreaseLevel.Call();
        ResetLevelProgress();
        PlayerPrefs.SetInt("LevelUnlocked", level);
    }
    
    void ResetLevelProgress()
    {
        currentObjective = 0;
        finalObjective = 10;
    }
    public float GetLevelRatio()
    {
        return (float)currentObjective / (float)finalObjective;
    }
}
