using System;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPopup : UiPopup {

    public static UiPopup instance;
    public Image vibrationIcon;

    public Sprite vibrationOn;
    public Sprite vibrationOff;
    public virtual void Awake()
    {
        instance = this;
    }

    public override void Show()
    {
        base.Show();
        SetupVibrationIcon();
    }

    public void AddPopupToQueue()
    {
        PopupManager.instance.AddPopupToQueue(this);
    }

    void SetupVibrationIcon()
    {
        if (VibrationManager.Instance.Unwrap().useVibration)
        {
            vibrationIcon.sprite = vibrationOn;
        }
        else
        {
            vibrationIcon.sprite = vibrationOff;
        }
    }
    public void ToggleVibration()
    {
        VibrationManager.Instance.Unwrap().ToggleVibration();
        SetupVibrationIcon();
    }

}