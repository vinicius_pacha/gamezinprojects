using System.Collections;
using System.Collections.Generic;
using ControlFreak2;
using UnityEngine;

public class MenuWindow : UiWindow {

    public static UiWindow instance;
    public virtual void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        PachaGamesManager.instance.StartGame();
    }
    public override void Show()
    {
        base.Show();
    }

    public override void Update()
    {
        base.Update();

        Vector2 input = new Vector2(CF2Input.GetAxis("Horizontal"),CF2Input.GetAxis("Vertical"));
        if (input.magnitude >= 0.5f)
        {
            PachaGamesManager.instance.StartGame();
        }
    }
}
