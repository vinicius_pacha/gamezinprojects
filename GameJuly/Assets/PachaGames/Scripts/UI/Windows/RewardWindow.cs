using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RewardWindow : UiWindow {
    public static UiWindow instance;
    public TMP_Text percentage;
    public RectTransform rewardBar;
    public float minWidth;
    public float maxWidth;
    public float timeToStartBar;
    public float barTime;
    public TMP_Text watchAdPercentage;

    public GameObject continueBtn;
    public GameObject watchAdBtn;
    public GameObject noThanksBtn;

    [Header("RewardWidgetConfig")] 
    public Image rewardSprite;
    
    public virtual void Awake()
    {
        instance = this;
    }

    public override void Show()
    {
        base.Show();
        continueBtn.SetActive(false);
        noThanksBtn.SetActive(true);
        watchAdBtn.SetActive(true);
        var reward = RewardManager.Instance.Unwrap().GetCurrentReward();
        rewardSprite.sprite = reward.customizationObjectConfig.customizationSprite;
        rewardSprite.color = Color.black;
        watchAdPercentage.text = String.Format("+{0}%",RewardManager.Instance.Unwrap().RewardIncreaseByAd);
        AnimateReward();
    }

    public void AnimateReward()
    {
        StopAllCoroutines();
        StartCoroutine(RewardCoroutine(timeToStartBar));
    }
    public void AnimateWithoutDelayReward()
    {
        StopAllCoroutines();
        StartCoroutine(RewardCoroutine(0));
    }
    
    IEnumerator RewardCoroutine(float timeToStartBar)
    {
        float t = 0;
        float previousPercentage = RewardManager.Instance.Unwrap().previousReward;
        float desiredPercentage = RewardManager.Instance.Unwrap().reward;
        float currentPercentage = previousPercentage;
        var width = Mathf.Lerp(minWidth, maxWidth, previousPercentage/100.0f);
        rewardBar.sizeDelta = new Vector2(width,rewardBar.sizeDelta.y);
        percentage.text = string.Format("{0}%", Mathf.RoundToInt(previousPercentage));

        yield return new WaitForSeconds(timeToStartBar);
        
        while (t < 1)
        {
            t += Time.deltaTime/barTime;
            currentPercentage  = Mathf.Lerp(previousPercentage, desiredPercentage, t);
            yield return null;
            percentage.text = string.Format("{0}%", Mathf.RoundToInt(currentPercentage));
            width = Mathf.Lerp(minWidth, maxWidth, currentPercentage / 100.0f);
            rewardBar.sizeDelta = new Vector2(width,rewardBar.sizeDelta.y);
        }
        currentPercentage = desiredPercentage;
        percentage.text = string.Format("{0}%", Mathf.RoundToInt(currentPercentage));
        width = Mathf.Lerp(minWidth, maxWidth, currentPercentage/100.0f);

        rewardBar.sizeDelta = new Vector2(width,rewardBar.sizeDelta.y);
        if (RewardManager.Instance.Unwrap().reward >= 100)
        {
            PachaGamesManager.instance.RewardComplete();
        }
        
    }
    
    public void GetRewardIncreaseByAd()
    {
        AdManager.Instance.Unwrap().ShowRewardedVideo(RewardManager.Instance.Unwrap().IncreaseRewardByAd);
        continueBtn.SetActive(true);
        noThanksBtn.SetActive(false);
        watchAdBtn.SetActive(false);

    }
}
