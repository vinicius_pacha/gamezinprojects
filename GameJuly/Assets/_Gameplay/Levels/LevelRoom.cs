﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelRoom : MonoBehaviour
{
    public Transform initial;
    public Transform final;
    public int currentEnemyIndex = -1;
    public Transform instantiatedPrefab;
    public List<GameObject> instantiatedObjects;

    public List<RoomEnemyPattern> enemyPattern = new List<RoomEnemyPattern>();

    
    public void SetupEnemies()
    {
        StartCoroutine(SetupEnemiesCoroutine());
        
    }

    IEnumerator SetupEnemiesCoroutine()
    {
        yield return null;
        var level = LevelManager.instance.level;
        var newList = enemyPattern.Where(x => level >= x.minLevel).ToList();
        var pattern = newList[Random.Range(0, newList.Count)];

        currentEnemyIndex = enemyPattern.IndexOf(pattern);

        instantiatedPrefab = Instantiate(enemyPattern[currentEnemyIndex], transform).transform;
        instantiatedPrefab.gameObject.SetActive(true);
        int children = instantiatedPrefab.childCount;
        for (int i = 0; i < children; ++i)
        {
            var e = instantiatedPrefab.GetChild(i).gameObject;
            if (e != null)
                instantiatedObjects.Add(e);
        }
    }
    public void ReSetupEnemies()
    {
        instantiatedPrefab = Instantiate(enemyPattern[currentEnemyIndex], transform).transform;
        instantiatedPrefab.gameObject.SetActive(true);
        int children = instantiatedPrefab.childCount;
        for (int i = 0; i < children; ++i)
        {
            var e = instantiatedPrefab.GetChild(i).gameObject;
            if (e != null)
                instantiatedObjects.Add(e);
        }
    }

    public void ClearEnemies()
    {
        foreach (var e in instantiatedObjects)
        {
            if (e)
            {
                Destroy(e.gameObject);
            }
        }
        instantiatedObjects.Clear();
    }

    public void ResetRoom()
    {
        ClearEnemies();
        ReSetupEnemies();
    }
    
    
}
