﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelConnection : MonoBehaviour
{
    public Transform initial;
    public Transform final;
}
