﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using ControlFreak2;
using UnityEngine;

public class Ball : BitStrap.Singleton<Ball>
{
    public float maxSize;
    public float currentSize;
    public float increaseSizeByDistance;
    public float distance = 2;
    public float rotateSpeed = 2;
    public float launchSpeed = 2;
    private Player player;
    public Rigidbody ballPrefab;
    public GameObject instantiatedBall;
    public float startSize = 0.5f;
    private Vector3 lastPlayerPosition;

    public float creatingBallTime = 0;
    public float timeToBreakBall = 0;
    public ParticleSystem creatingFx;

    public float creatingBall;

    public float MaxSize
    {
        get
        {
            return maxSize + maxSize * 0.05f * UpgradeStatus.Instance.Unwrap().ballSizeLevel;
        }
    }
    
    public float IncreaseByDistance
    {
        get
        {
            return increaseSizeByDistance + increaseSizeByDistance * 0.05f * UpgradeStatus.Instance.Unwrap().ballLaunchLevel;
        }
    }    
    public float CreateBallTime
    {
        get
        {
            return creatingBallTime - creatingBallTime * 0.05f * UpgradeStatus.Instance.Unwrap().createBallLevel;
        }
    }

// Start is called before the first frame update
    void Start()
    {
        transform.parent = null;
        player = Player.Instance.Unwrap();
        //CreateBall();
        //MenuWindow.instance.onShow.Register(CreateBall);
    }

    public void CreateBall()
    {
        if (instantiatedBall != null) return;
        
        var ball = Instantiate(ballPrefab.gameObject, transform.position,transform.rotation);
        ball.transform.parent = transform;
        ball.transform.localPosition = Vector3.zero;
        ball.transform.localRotation = Quaternion.identity;
        currentSize = startSize;
        ball.transform.localScale = currentSize * Vector3.one;
        instantiatedBall = ball;
    }

    public void BreakSnowBall()
    {
        if (instantiatedBall != null)
        {
            currentSize = startSize;
            instantiatedBall.GetComponent<BallCollideOnEnemy>().BreakSnowBall();
        }
    }
    public void LaunchBall()
    {
        creatingBall = 0;
        instantiatedBall.GetComponent<Collider>().isTrigger = false;
        instantiatedBall.transform.parent = null;
        var ballrbd = instantiatedBall.GetComponent<Rigidbody>();
        ballrbd.useGravity = true;
        ballrbd.isKinematic = false;
        ballrbd.AddForce(player.transform.forward * launchSpeed);
        instantiatedBall = null;

    }

    // Update is called once per frame
    void Update()
    {
        if (!player.controller.IsGrounded())
        {
            timeToBreakBall += Time.deltaTime;
            if (timeToBreakBall >= 0.3f)
            {
                BreakSnowBall();
                return;

            }
        }
        else
        {
            timeToBreakBall = 0;
        }
        if (player.currentPlayerState != Player.PlayerState.GAME)
        {
            player.playerAnimator.playerAnimator.SetBool("MakingBall", false);
            player.playerAnimator.playerAnimator.SetBool("HasBall", false);
            creatingFx.Stop();
            return;
        }
        player.playerAnimator.playerAnimator.SetBool("MakingBall", creatingBall > 0);
        player.playerAnimator.playerAnimator.SetBool("HasBall", instantiatedBall != null);
        transform.position = player.transform.position + player.transform.forward * distance + player.transform.forward * ( 0.5f * currentSize) + Vector3.up * ( 0.5f* currentSize);

        if (instantiatedBall == null)
        {
            if (Input.touchCount <= 0 && !CF2Input.anyKey)
            {
                creatingBall += Time.deltaTime;
                if(!creatingFx.isPlaying)
                    creatingFx.Play();
                
                if (creatingBall >= CreateBallTime)
                {
                    lastPlayerPosition = player.transform.position;
                    creatingBall = 0;
                    CreateBall();
                }
            }
            else
            {
                creatingBall = 0;
                creatingFx.Stop();
            }
            return;
        }
      
        creatingFx.Stop();

        var deltaPos = Vector3.Distance(player.transform.position, lastPlayerPosition);
        currentSize += deltaPos * IncreaseByDistance * Time.deltaTime;
        currentSize = Mathf.Clamp(currentSize, 0, MaxSize);
        instantiatedBall.transform.localScale = Vector3.one * currentSize;
        transform.forward = player.transform.forward;
        instantiatedBall.transform.Rotate(rotateSpeed * deltaPos,0,0,Space.Self);
        lastPlayerPosition = player.transform.position;
        
        if(Input.GetKeyDown(KeyCode.Space) || CF2Input.GetButtonUp("Fire1"))
            LaunchBall();
    }
}
