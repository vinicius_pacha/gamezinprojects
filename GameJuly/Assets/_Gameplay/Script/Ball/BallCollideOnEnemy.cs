﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;


public class BallCollideOnEnemy : MonoBehaviour
{
    public float ballEnemyLength = -.1f;
    public List<Enemy> enemiesAttached = new List<Enemy>();
    public List<Rigidbody> thingsAttached = new List<Rigidbody>();
    public TrailRenderer trail;
    public ParticleSystem destroyFx;
    public float timeToBreakCounter;
    public Rigidbody rbd;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform == null)
        {
            return;
            
        }
        if(this.gameObject == null)
            return;
        
        if (transform.position.y < -10)
        {
            Destroy(this.gameObject);
            return;
            
        }

        if (transform.parent == null && rbd.velocity.magnitude <= 1.5f)
        {
            if(timeToBreakCounter >= 0.05f)
                BreakSnowBall();
            timeToBreakCounter += Time.deltaTime;
        }
        else
        {
            timeToBreakCounter = 0;
        }
        foreach (var e in enemiesAttached)
        {
            if (e)
            {
                var dir = e.transform.localPosition.normalized;
                e.transform.localPosition =
                    dir.normalized * transform.localScale.x * 0.25f + dir.normalized * ballEnemyLength;
                e.transform.localScale = Vector3.one*e.size * (1 / transform.localScale.x);
            }

        }
        foreach (var e in thingsAttached)
        {
            var dir = e.transform.localPosition.normalized;
            e.transform.localPosition =
                dir.normalized * transform.localScale.x * 0.25f + dir.normalized * ballEnemyLength;
            e.transform.localScale = Vector3.one * (1 / transform.localScale.x);
        }

        trail.widthMultiplier = transform.localScale.x;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            var e = other.gameObject.GetComponent<Enemy>();
            if (e.size > 1 && transform.localScale.x < e.size * 1)
            {
                e.BreakSnowBall();
                BreakSnowBall();
                return;
            }
            
            e.SetFrozen();
            e.transform.parent = this.transform;
            e.transform.localScale = Vector3.one;
            enemiesAttached.Add(e);
        }
        if (other.gameObject.CompareTag("Absorbable"))
        {
            var rbd = other.gameObject.GetComponent<Rigidbody>();
            if (rbd.transform.localScale.x > transform.localScale.x)
            {
                BreakSnowBall();
                return;;
            }
            var cols = rbd.GetComponents<Collider>();
           
                
            foreach (var c in cols)
            {
                c.enabled = false;
                
            }
            rbd.isKinematic = true;
            other.enabled = false;
            rbd.transform.parent = this.transform;
            //rbd.transform.localScale = Vector3.one;
            thingsAttached.Add(rbd);
        }
        if (other.gameObject.CompareTag("Breakable"))
        {
            BreakSnowBall();
        }if (other.gameObject.CompareTag("Deadly"))
        {
            BreakSnowBall();
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.parent)
        {
            if (other.transform.parent.gameObject.CompareTag("Enemy"))
            {
                var e = other.gameObject.GetComponent<Enemy>();
                e.SetFrozen();
                e.transform.parent = this.transform;
                e.transform.localScale = Vector3.one;
                enemiesAttached.Add(e);
            }
        }
    }

    public void BreakSnowBall()
    {
        destroyFx.transform.parent = null;
        destroyFx.Play();
        foreach (var e in enemiesAttached)
        {
            e.SetDead();
        }

        foreach (var rbd in thingsAttached)
        {
            rbd.isKinematic = false;
            rbd.useGravity = true;
            var cols = rbd.GetComponents<Collider>();
            foreach (var c in cols)
            {
                c.enabled = true;
            }
        }
        Destroy(this.gameObject);
    }

}
