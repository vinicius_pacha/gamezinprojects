﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrailFx : MonoBehaviour
{
    public Transform parentBall;
    // Start is called before the first frame update
    void Start()
    {
        transform.parent = null;
    }

    void Update()
    {
        if(parentBall == null)
            Destroy(this.gameObject);
        if(parentBall.parent == null) 
            Destroy(this.gameObject);
        transform.position = parentBall.transform.position - Vector3.up * 0.5f * parentBall.transform.localScale.x +
                             Vector3.up * 0.1f;
        transform.localScale = parentBall.transform.localScale;
        transform.rotation = parentBall.parent.rotation;

    }
}
