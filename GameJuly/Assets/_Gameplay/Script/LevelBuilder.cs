﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;
using UnityEngine.Monetization;

public class LevelBuilder : MonoBehaviour
{
    public List<LevelRoom> rooms = new List<LevelRoom>();
    public List<LevelConnection> connections = new List<LevelConnection>();
    public LevelRoom firstLevel;
    public LevelRoom secondLevel;
    public LevelConnection connection;

    public List<GameObject> instantiatedObjects = new List<GameObject>();
    public int currentLevel;
    private Vector3 lastRoomPosition;
    void Start()
    {
        MenuWindow.instance.onShow.Register(LoadLevel);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    [Button]    
    void LoadLevel()
    {
        if (currentLevel != LevelManager.instance.level)
        {
            currentLevel = LevelManager.instance.level;
            ClearRoom();
        }
        else
        {
            foreach (var io in instantiatedObjects)
            {
                var r = io.GetComponent<LevelRoom>();
                if (r != null)
                    r.ResetRoom();
            }

            return;
        }
        
        int roomQuantity = 2;
        
        if(LevelManager.instance.level-1 > 5)
            roomQuantity = Random.Range(2, 4);
        if(LevelManager.instance.level-1 > 10)
            roomQuantity = Random.Range(2, 5);
        if(LevelManager.instance.level-1 > 20)
            roomQuantity = Random.Range(2, 6);
        if(LevelManager.instance.level-1 > 30)
            roomQuantity = Random.Range(2, 7);
        if(LevelManager.instance.level-1 > 40)
            roomQuantity = Random.Range(2, 8);
        
        roomQuantity = Mathf.Clamp(roomQuantity, 2, 4);
        Vector3 roomDelta =Vector3.forward*4;
        lastRoomPosition = Vector3.forward*4;
        for (int i = 0; i < roomQuantity; i++)
        {
            int roomToInstantiate = 0;
            if (i == 0)
                roomToInstantiate = 1;
            LevelRoom room;
            if (LevelManager.instance.level == 1)
            {
                if(i ==0)
                    room = Instantiate(firstLevel,Vector3.forward*4,Quaternion.identity);
                else
                {
                    room = Instantiate(secondLevel,Vector3.forward*4,Quaternion.identity);
                }
            }
            else
            {
                room = Instantiate(rooms[Random.Range(roomToInstantiate,rooms.Count)],Vector3.forward*4,Quaternion.identity);

            }
            roomDelta = lastRoomPosition - room.initial.position;
            instantiatedObjects.Add(room.gameObject);

            room.transform.position += roomDelta;
            room.SetupEnemies();
            lastRoomPosition = room.transform.position;
            if (i < roomQuantity - 1)
            {
                LevelConnection c;
                if (LevelManager.instance.level <= 4)
                {
                    c = Instantiate(connection,room.transform.position,Quaternion.identity);
                }
                else
                {
                    c = Instantiate(connections[Random.Range(0,connections.Count)],room.transform.position,Quaternion.identity);

                }
                
                instantiatedObjects.Add(c.gameObject);

                var deltaPosition = room.final.position - c.initial.transform.position;
                c.transform.position += deltaPosition;
                lastRoomPosition = c.final.position;
            }
            else
            {
                FinishLine.Instance.Unwrap().transform.position = room.final.position;
            }

        }

    }
    [Button]
    public void ClearRoom()
    {
        foreach (var io in instantiatedObjects)
        {
            if (Application.isPlaying)
            {
                var r = io.GetComponent<LevelRoom>();
                if(r!= null)
                    r.ClearEnemies();
                if(io!=null)
                    Destroy(io.gameObject);
            }
            else
            {
                if(io!=null)
                    DestroyImmediate(io.gameObject);
            }
        }
        instantiatedObjects.Clear();
    }
    
}
