﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectile : MonoBehaviour
{
    public Transform target;
    public Rigidbody rbd;
    public bool bounce;
    public bool follow;
    public float guided = 0.2f;
    public void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            Destroy(this.gameObject);
            
            other.gameObject.GetComponent<Player>().Dead();
        } 
        if(other.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            
            other.gameObject.GetComponent<Enemy>().BreakSnowBall();
        }
        if(other.gameObject.CompareTag("Ball"))
        {
            other.gameObject.GetComponent<BallCollideOnEnemy>().BreakSnowBall();
            Destroy(this.gameObject);
        }
        if(!bounce)
            Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
            
            other.gameObject.GetComponent<Enemy>().BreakSnowBall();
        }
        
    }

    private void Update()
    {
        if (follow)
        {
            var dir = rbd.velocity;
            var playerDir = Player.Instance.Unwrap().transform.position - transform.position;

            var finalDir = Vector3.Lerp(playerDir.normalized, dir.normalized, 0.2f);

            rbd.velocity = finalDir.normalized * rbd.velocity.magnitude;
        }
    }
}
