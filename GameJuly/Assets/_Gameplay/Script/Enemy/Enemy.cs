﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using MLSpace;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public bool hasGun;
    public float size = 1;

    public float alertRange = 5;
    public float attackRange = 3;
    public float attackLeaveRange = 3;
    public Material[] enemyMaterials;
    public Material deadMaterial;
    public Renderer enemyRenderer;
    public NavMeshAgent agent;
    public EnemyWeapon weapon;
    public float shootForce = 1;
    private Player player;
    private Rigidbody rbd;
    public EnemyState currentState;
    public RagdollManagerHum ragdollManager;
    public Animator animator;
    public LayerMask groundLayerMask;
    private Collider[] cols;
    public enum EnemyState
    {
        idle,
        alert,
        shooting,
        frozen,
        dead
    }

    public SafeAction onAlert = new SafeAction();
    public SafeAction onShooting= new SafeAction();
    public SafeAction onDead = new SafeAction();

    public bool isPurple = false;

    public Material[] purpleMats;
    // Start is called before the first frame update
    [Button]
    void Start()
    {
        cols = transform.GetComponents<Collider>();
        rbd = transform.GetComponent<Rigidbody>();
        player = Player.Instance.Unwrap();
        transform.localScale = Vector3.one * size;
        SetMaterial();
        agent.enabled = true;

        if(LevelManager.instance.level > 10)
            agent.speed = 1.75f * 1.0f/size;
        if(LevelManager.instance.level > 20)
            agent.speed = 2f * 1.0f/size;
    }

    // Update is called once per frame
    void Update()
    {
        if (PachaGamesManager.instance != null && PachaGamesManager.instance.currentGameState != PachaGamesManager.GameState.INGAME)
        {
            if(agent.isOnNavMesh)
                agent.SetDestination(transform.position);
            return;
        }
        if (transform.position.y < -30)
        {
            SetDead(); 
        }

        CheckGround();
        switch (currentState)
        {
            case EnemyState.idle:
                Idle();
                break;
            case EnemyState.alert:
                Alert();
                break;
            case EnemyState.shooting:
                Shooting();
                break;
            case EnemyState.frozen:
                Frozen();
                break;
            case EnemyState.dead:
                Dead();
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
      
    }

    void Idle()
    {
        if(agent.isOnNavMesh)
            agent.SetDestination(transform.position);

        if (Vector3.Distance(transform.position, player.transform.position) < alertRange)
        {
            currentState = EnemyState.alert;
            onAlert.Call();
        }
    }
    void Alert()
    {
        
        if(agent.isOnNavMesh)
            agent.SetDestination(player.transform.position);
        if (Vector3.Distance(transform.position, player.transform.position) < attackRange)
        {
            currentState = EnemyState.shooting;
            onShooting.Call();
        }
        if (Vector3.Distance(transform.position, player.transform.position) > alertRange)
        {
            currentState = EnemyState.idle;
        }
    }
    void Shooting()
    {

        if(agent.isOnNavMesh)
            agent.SetDestination(transform.position);
        if (hasGun)
        {
            var dir = (Player.Instance.Unwrap().transform.position - transform.position);
            dir.y = 0;
            transform.rotation = Quaternion.LookRotation(dir);
            weapon.Logic();
            if (weapon.CanShoot)
            {
                weapon.Shoot(shootForce);
                
            }
            else
            {
                if (Vector3.Distance(transform.position, player.transform.position) > attackRange)
                {
                    currentState = EnemyState.alert;
                    return;
                }
            }
        }
        else 
        {
            if (Vector3.Distance(transform.position, player.transform.position) > attackLeaveRange)
                currentState = EnemyState.alert;
        }
    }

    public void SetDead()
    {
        if (isPurple)
        {
            BreakSnowBall();
            return;
        }
        if(currentState == EnemyState.dead)
            return;

        if (transform)
        {
            var col = GetComponent<Collider>();
            if(col) 
                col.enabled = false;
            var anim = GetComponent<Animator>();
            if(anim) 
                anim.enabled = true;

        }
        agent.enabled = false;
        
        currentState = EnemyState.frozen;
        transform.parent = null;
        transform.localScale = Vector3.one * size;
        ragdollManager.startRagdoll(null,null,Vector3.up * 2 + UnityEngine.Random.insideUnitSphere * 2);

        enemyRenderer.material = deadMaterial;
        currentState = EnemyState.dead;

    }
    void Dead()
    {
    }
    void Frozen()
    {
    }

    public void SetFrozen()
    {
        agent.enabled = false;
        ragdollManager.blendToMecanim();
        
        foreach (var c in cols)
        {
            c.enabled = false;
        }

        rbd.isKinematic = true;
        CancelInvoke();
        currentState = Enemy.EnemyState.frozen;
        GetComponent<Animator>().enabled = false;
    }
    public void BreakSnowBall()
    {
        agent.enabled = false;
        GetComponent<Animator>().enabled = true;
        currentState = EnemyState.frozen;
        transform.parent = null;
        transform.localScale = Vector3.one * size;
        ragdollManager.startRagdoll(null,null,Vector3.up * 2 + UnityEngine.Random.insideUnitSphere * 2);
        if (size > 1)
        {
            size -= 0.5f;
            size = Mathf.Clamp(size, 1, 999);

            transform.localScale = Vector3.one * size;
            SetMaterial();
            Invoke("Recover", 3);
        }
        else
        {
            if (!isPurple)
                SetDead();
            else
            {
                Invoke("Recover", 2);
            }
        }
        
    }  
    public void Recover()
    {
        ragdollManager.blendToMecanim();
        rbd.isKinematic = false;
        foreach (var c in cols)
        {
            c.enabled = true;
        }
        Invoke("RecoverFinish",0.5f);
    }

    public void RecoverFinish()
    {
        currentState = EnemyState.idle;
        agent.enabled = true;

    }

    public void CheckGround()
    {
        if (currentState != EnemyState.frozen && !agent.enabled)
        {
            if (Physics.Raycast(transform.position + Vector3.up * 0.3f, Vector3.down, out RaycastHit hit, 1f, groundLayerMask))
            {
                if(hit.transform.gameObject.isStatic)
                    agent.enabled = true;

            }
        }
    }
    private void SetMaterial()
    {
        
        var s = (size - 0.5f) / 0.5f;
        s = Mathf.Round(s);
        enemyRenderer.material = enemyMaterials[(int) s];
        if (isPurple)
            enemyRenderer.material = purpleMats[(int) s];

    }
}
