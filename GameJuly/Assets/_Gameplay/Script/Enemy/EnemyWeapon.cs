﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public class EnemyWeapon : MonoBehaviour
{
    
    public EnemyProjectile projectile;


    public float currentShootTimer;
    public float currentPrepareTimer;
    public float prepareTime ;
    public float shootDelay;
    public Transform projectileSpot;
    public Enemy enemy;
    public Animator animator;
    // Start is called before the first frame update
    public bool CanShoot
    {
        get
        {
            return currentShootTimer > shootDelay;
        }
    }

    // Update is called once per frame
    public void Logic()
    {
        currentShootTimer += Time.deltaTime;
        if (CanShoot)
            currentPrepareTimer += Time.deltaTime;
        else
            currentPrepareTimer = 0;
        animator.SetBool("Aiming",enemy.currentState == Enemy.EnemyState.shooting && CanShoot);
    }

    [Button]
    public void Shoot(float shootForce)
    {
        if (currentPrepareTimer < prepareTime) return;
        animator.SetTrigger("Shoot");

        var projectilePosition = projectileSpot.position;
        projectilePosition.y = transform.position.y + 0.75f;
        var p = Instantiate(projectile, projectilePosition, transform.rotation);
        var dir = Player.Instance.Unwrap().transform.position - transform.position;
        p.rbd.AddForce(dir.normalized * shootForce);
        currentShootTimer = 0;
    }
}
