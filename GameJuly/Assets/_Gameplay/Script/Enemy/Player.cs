﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public class Player : BitStrap.Singleton<Player>
{
    public BasicWalkerController controller;
    public Mover mover;
    public PlayerRagdoll ragdoll;
    public PlayerAnimator playerAnimator;
    public PlayerState currentPlayerState;
    public TrailRenderer trail;
    public ParticleSystem runFx;
    public Rigidbody rbd;
    private float initialSpeed;
    public enum PlayerState
    {
        MENU,
        GAME,
        VICTORY,
        DEAD
    }

    
    private void Start()
    {
        MenuWindow.instance.onShow.Register(Reset);
        InGameWindow.instance.onShow.Register(StartGame);
        Application.targetFrameRate = 60;
        initialSpeed = controller.movementSpeed;
    }
    void Reset()
    {
        controller.blockInput = false;
        transform.position = Vector3.zero;
        rbd.velocity = Vector3.zero;
        playerAnimator.playerAnimator.SetBool("Dancing", false );
        trail.Clear();
        trail.enabled = false;
        Revive();
    }   
    void StartGame()
    {
        playerAnimator.playerAnimator.SetBool("Dancing", false );
        rbd.isKinematic = false;
        Ball.Instance.Unwrap().creatingBall = Ball.Instance.Unwrap().creatingBallTime - 0.3f;
        currentPlayerState = PlayerState.GAME;
        trail.enabled = true;
    }
    // Start is called before the first frame update
    [Button]
    public void Dead()
    {
        if (PachaGamesManager.instance != null &&
            PachaGamesManager.instance.currentGameState == PachaGamesManager.GameState.INGAME)
        {
            
            currentPlayerState = PlayerState.DEAD;
            controller.enabled = false;
            rbd.isKinematic = true;
            ragdoll.StartRagdoll();
            Ball.Instance.Unwrap().BreakSnowBall();
            PachaGamesManager.instance.Death();
        }

    }  
    [Button]
    void Revive()
    {
        currentPlayerState = PlayerState.MENU;
        controller.enabled = true;
        transform.position = Vector3.zero;
        ragdoll.ragdollManager.blendTime = 0;
        ragdoll.ragdollManager.blendToMecanim();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("FinishLine"))
        {
            CompleteLevel();
        } 
        if (other.gameObject.CompareTag("Deadly"))
        {
            Dead();
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            if(other.GetComponent<Enemy>().agent.enabled)
                Dead();
        }
        
    }

    void CompleteLevel()
    {        
        controller.blockInput = true;
        Ball.Instance.Unwrap().BreakSnowBall();
        currentPlayerState = PlayerState.VICTORY;
        playerAnimator.playerAnimator.SetBool("Dancing", true );

        StartCoroutine(StartMove());
       
    }
    
    public float angularMovement = 5;
    IEnumerator StartMove()
    {
        while (Vector3.Distance(transform.position, FinishLine.Instance.Unwrap().finalPosition.position) > 0.3f)
        {
            transform.position = Vector3.MoveTowards(transform.position,
                FinishLine.Instance.Unwrap().finalPosition.position, controller.movementSpeed * 1 * Time.deltaTime);
            transform.rotation = Quaternion.RotateTowards(transform.rotation,
                FinishLine.Instance.Unwrap().finalPosition.rotation, angularMovement * Time.deltaTime);
            yield return null;
        }
        FinishLine.Instance.Unwrap().CompleteLevel();
        yield return new WaitForSeconds(0.5f);
        if(PachaGamesManager.instance != null)
            PachaGamesManager.instance.Victory();
        
    }

    private void Update()
    {
        if(transform.position.y <= -15)
            Dead();
        if (Ball.Instance.Unwrap().instantiatedBall != null)
        {
            controller.movementSpeed = initialSpeed - initialSpeed * 0.2f -
                                       (Ball.Instance.Unwrap().instantiatedBall.transform.localScale.x /
                                        Ball.Instance.Unwrap().maxSize) * initialSpeed * 0.2f;
        }
        else
        {
            controller.movementSpeed = initialSpeed;
        }

        if (currentPlayerState == PlayerState.MENU)
        {
            transform.position=Vector3.zero;
            
        }
    }
}
