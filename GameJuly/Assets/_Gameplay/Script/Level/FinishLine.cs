﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : BitStrap.Singleton<FinishLine>
{
    public ParticleSystem[] ps;

    public Transform finalPosition;
    // Start is called before the first frame update
    public void CompleteLevel()
    {
        foreach (var p in ps)
        {
            p.Play();
        }
    }

}
