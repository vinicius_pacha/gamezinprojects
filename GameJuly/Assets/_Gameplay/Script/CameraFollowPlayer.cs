﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    public Player target;
    public Vector3 victoryOffset;
    private Vector3 initialPosition;
    private Vector3 targetInitialPosition;
    public float speed = 5f;
    // Start is called before the first frame update
    void Start()
    {
        targetInitialPosition = target.transform.position;
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        var deltaPosition = target.transform.position - targetInitialPosition;
        var desiredPosition = initialPosition + deltaPosition;

        switch (target.currentPlayerState)
        {
            case Player.PlayerState.MENU:
                transform.position = Vector3.Lerp(transform.position,desiredPosition,Time.deltaTime*speed);
                break;
            case Player.PlayerState.GAME:
                transform.position = Vector3.Lerp(transform.position,desiredPosition,Time.deltaTime*speed);
                break;
            case Player.PlayerState.VICTORY:
                transform.position = Vector3.Lerp(transform.position,desiredPosition + victoryOffset,Time.deltaTime *2);
                break;
            case Player.PlayerState.DEAD:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        
        
    }
}
