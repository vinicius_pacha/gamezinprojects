﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeStatus : BitStrap.Singleton<UpgradeStatus>
{
    public int createBallLevel = 1;
    public int ballSizeLevel = 1;
    public int ballLaunchLevel = 1;
    public TMP_Text createPrice;
    public TMP_Text sizePrice;
    public TMP_Text launchPrice;    
    
    public TMP_Text createLevel;
    public TMP_Text sizeLevel;
    public TMP_Text launchLevel;
    public Button createButton;
    public Button sizeButton;
    public Button launchButton;
    public const string CREATE_BALL_LEVEL = "CREATE_BALL_LEVEL";
    public const string BALL_SIZE_LEVEL = "BALL_SIZE_LEVEL";
    public const string BALL_LAUNCH_LEVEL = "BALL_LAUNCH_LEVEL";
    private void Start()
    {
        if (PlayerPrefs.HasKey(CREATE_BALL_LEVEL))
        {
            createBallLevel = PlayerPrefs.GetInt(CREATE_BALL_LEVEL);
        }
        if (PlayerPrefs.HasKey(BALL_SIZE_LEVEL))
        {
            ballSizeLevel = PlayerPrefs.GetInt(BALL_SIZE_LEVEL);

        }
        if (PlayerPrefs.HasKey(BALL_LAUNCH_LEVEL))
        {
            ballLaunchLevel = PlayerPrefs.GetInt(BALL_LAUNCH_LEVEL);
        }
    }

    public void UpgradeCreate()
    {
        var price = GetPriceValue(createBallLevel);
        if(CoinManager.Instance.Unwrap().SpendCoins(price))
        {
            createBallLevel++;
            PlayerPrefs.SetInt(CREATE_BALL_LEVEL,createBallLevel);
        }
    }
    public void UpgradeSize()
    {
        var price = GetPriceValue(ballSizeLevel);
        if(CoinManager.Instance.Unwrap().SpendCoins(price))
        {
            ballSizeLevel++;
            PlayerPrefs.SetInt(BALL_SIZE_LEVEL,ballSizeLevel);
        }
    }
    public void UpgradeLaunch()
    {
        var price = GetPriceValue(ballLaunchLevel);
        if(CoinManager.Instance.Unwrap().SpendCoins(price))
        {
            ballLaunchLevel++;
            PlayerPrefs.SetInt(BALL_LAUNCH_LEVEL,ballLaunchLevel);
        }
    }

    int GetPriceValue(int level)
    {
        return 50 + (level-1) * 10;
    }

    private void Update()
    {
        launchPrice.text = GetPriceValue(ballLaunchLevel).ToString();
        createPrice.text = GetPriceValue(createBallLevel).ToString();
        sizePrice.text = GetPriceValue(ballSizeLevel).ToString();
        createLevel.text = string.Format("LV{0}", createBallLevel);
        launchLevel.text = string.Format("LV{0}", ballLaunchLevel);
        sizeLevel.text = string.Format("LV{0}", ballSizeLevel);
        
        launchButton.gameObject.SetActive(LevelManager.instance.level>3);
        sizeButton.gameObject.SetActive(LevelManager.instance.level>3);
        createButton.gameObject.SetActive(LevelManager.instance.level>3);
        
        launchButton.interactable = CoinManager.Instance.Unwrap().desiredCoins >= GetPriceValue(ballLaunchLevel);
        sizeButton.interactable = CoinManager.Instance.Unwrap().desiredCoins >= GetPriceValue(ballSizeLevel);
        createButton.interactable = CoinManager.Instance.Unwrap().desiredCoins >= GetPriceValue(createBallLevel);
        
        

    }
}
