﻿using System.Collections;
using System.Collections.Generic;
using MLSpace;
using UnityEngine;

public class PlayerRagdoll : MonoBehaviour
{
    public RagdollManagerHum ragdollManager;
   

    // Start is called before the first frame update
    void Start()
    {

    }

    [BitStrap.Button]
    public void StartRagdoll()
    {
        ragdollManager.startRagdoll(null,null,Vector3.up);
    }

// Update is called once per frame
    void Update()
    {
        
    }
}
