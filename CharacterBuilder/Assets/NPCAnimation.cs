﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCAnimation : MonoBehaviour
{
    public Animator[] animators;

    public NavMeshAgent agent;
    // Start is called before the first frame update
    void Update()
    {
        foreach (var animator in animators)
        {
            if(animator.gameObject.activeSelf)
                animator.SetFloat("Velocity",agent.velocity.magnitude);
        }
    }

}
