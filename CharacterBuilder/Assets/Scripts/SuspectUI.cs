﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public class SuspectUI : BitStrap.Singleton<SuspectUI>
{
    public SuspectWidgetUI[] suspectWidgets;
    [Button]
    public void Setup()
    {
        var suspect = LevelCreator.Instance.Unwrap().currentSuspect;
        foreach (var sw in suspectWidgets)
        {
            sw.gameObject.SetActive(false);
        }
        int i = 0;
        
        foreach (var id in suspect.indentifiers)
        {
            suspectWidgets[i].Setup(id);
            i++;
        }
        
    }
}
