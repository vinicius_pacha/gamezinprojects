﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;
using Random = UnityEngine.Random;

public class Player : MonoBehaviour
{
    public BodyType[] bodyTypes;

    public int currentBodyTypeIndex = 0;
    public BodyType currentBodyType;
    public Color torsoColor;
    public Color skinColor;
    public Color legsColor;
    public Color shoesColor;
    public Clothes clothes;
    [Header("AssetRefs")]
    [InlineScriptableObject]
    public ClothesColor skinAsset;
    [InlineScriptableObject]
    public ClothesColor torsoAsset;
    [InlineScriptableObject]
    public ClothesColor legsAsset;
    [InlineScriptableObject]
    public ClothesColor shoesAsset;

    public void Setup()
    {
        Randomize();
        GenerateFinalTexture();
    }

    void Randomize()
    {
        currentBodyTypeIndex = Random.Range(0, bodyTypes.Length);
        currentBodyType = bodyTypes[currentBodyTypeIndex];
        SetupBodyType();
        skinColor = skinAsset.GetRandomColor();
        torsoColor = torsoAsset.GetRandomColor();
        legsColor = legsAsset.GetRandomColor();
        shoesColor = shoesAsset.GetRandomColor();
    }
    public void SetupAndVerify()
    {
        Randomize();

        var currentSuspect = LevelCreator.Instance.Unwrap().currentSuspect;
        bool isValid = LevelCreator.Instance.Unwrap().CompareValidPlayer(currentSuspect, this);

        if (!isValid)
        {
            Suspect.Description descriptionToModify;
            descriptionToModify = currentSuspect.indentifiers[Random.Range(0, currentSuspect.indentifiers.Count)];
            switch (descriptionToModify)
            {
                case  Suspect.Description.Torso:
                    torsoColor = torsoAsset.GetAnotherColor(torsoColor);
                    break;
                case  Suspect.Description.Legs:
                    legsColor = torsoAsset.GetAnotherColor(legsColor);
                    break;
                case  Suspect.Description.Shoes:
                    shoesColor = torsoAsset.GetAnotherColor(shoesColor);
                    break;
                case  Suspect.Description.BodyType:
                    currentBodyTypeIndex = (currentBodyTypeIndex + 1) % bodyTypes.Length; 
                    currentBodyType = bodyTypes[currentBodyTypeIndex];
                    SetupBodyType();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        GenerateFinalTexture();
    }

    void GenerateFinalTexture()
    {
        clothes.Setup(torsoColor,legsColor,shoesColor,skinColor);
    }
    void SetupBodyType()
    {
        foreach (var body in bodyTypes)
        {
            body.gameObject.SetActive(body == currentBodyType);
        }
    }
}
