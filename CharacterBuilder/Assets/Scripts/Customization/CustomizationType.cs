﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizationType : ScriptableObject
{
    public string name;

    public enum CustomizationCategory
    {
        Hat,
        Hair,
        Eyes,
        Torso,
        Legs,
        Shoes
    }
    public GameObject[] prefabs;
    
}
