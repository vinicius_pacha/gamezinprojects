﻿using System.Collections.Generic;
using BitStrap;
using UnityEngine;


[System.Serializable]
public class Suspect
{
    
    public enum Description
    {
        Torso,
        Legs,
        Shoes,
        BodyType
    }
    
    public Player player;
    public List<Description> indentifiers = new List<Description>();
}


public class LevelCreator : BitStrap.Singleton<LevelCreator>
{
    public Vector3 minPosition;
    public Vector3 maxPosition;
    public Player playerPrefab;
    public Suspect currentSuspect;
    
    public List<Player> playerPool = new List<Player>();

    Vector3 GetRandomSpawnPosition()
    {
        var x = Random.Range(minPosition.x, maxPosition.x);
        var y = Random.Range(minPosition.y, maxPosition.y);
        var z = Random.Range(minPosition.z, maxPosition.z);
        return new Vector3(x, y, z);
    }
    [Button]
    void CreateLevel()
    {
        foreach (var p in playerPool)
        {
            Destroy(p.gameObject);
            
        }
        playerPool.Clear();
        GenerateSuspect();
        GeneratePlayers();
        GeneratePlayers();
        GeneratePlayers();
        SuspectUI.Instance.Unwrap().Setup();
    }

    Player InstantiatePlayer()
    {
        var p = Instantiate(playerPrefab,GetRandomSpawnPosition(),Quaternion.identity);
        playerPool.Add(p);
        return p;
    }
    public void GenerateSuspect()
    {
        var suspect = new Suspect();
        suspect.player = InstantiatePlayer();

        int descriptionQuantity = Random.Range(1, 4);

        for (int i = 0; i < descriptionQuantity; i++)
        {
            Suspect.Description description = (Suspect.Description)Random.Range(0, 4);
            if (suspect.indentifiers.Contains(description))
            {
                description = (Suspect.Description) (((int)description + 1) % 4);
            }
            suspect.indentifiers.Add(description);
        }
     
        
        currentSuspect = suspect;
        
        suspect.player.Setup();
    } 
    public void GeneratePlayers()
    {
        var p = InstantiatePlayer();
        
        p.SetupAndVerify();
    }

    public bool CompareValidPlayer(Suspect s, Player p)
    {
        int valid = s.indentifiers.Count;
        foreach (var identifiers in s.indentifiers)
        {
            switch (identifiers)
            {
                case Suspect.Description.Torso:
                    if (p.torsoColor == s.player.torsoColor)
                        valid--;
                    break;
                case Suspect.Description.Legs:
                    if (p.legsColor == s.player.legsColor)
                        valid--;
                    break;
                case Suspect.Description.Shoes:
                    if (p.shoesColor == s.player.shoesColor)
                        valid--;
                    break;
                case Suspect.Description.BodyType:
                    if (p.currentBodyTypeIndex == s.player.currentBodyTypeIndex)
                        valid--;
                    break;
                default:
                    break;
            }
        }

        return valid > 0;

    }
}
