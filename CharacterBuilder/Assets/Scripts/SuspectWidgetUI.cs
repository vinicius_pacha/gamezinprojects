﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuspectWidgetUI : MonoBehaviour
{
    public Image image;
    
    public Sprite[] spriteBodyRefs;
    public Sprite torso;
    public Sprite pants;
    public Sprite shoes;
    public Image icon;
    
    public void Setup(Suspect.Description description)
    {
        switch (description)
        {
            case Suspect.Description.Torso:
                image.sprite = torso;
                image.color = LevelCreator.Instance.Unwrap().currentSuspect.player.torsoColor;
                break;
            case Suspect.Description.Legs:
                image.sprite = pants;
                image.color = LevelCreator.Instance.Unwrap().currentSuspect.player.legsColor;
                break;
            case Suspect.Description.Shoes:
                image.sprite = shoes;
                image.color = LevelCreator.Instance.Unwrap().currentSuspect.player.shoesColor;
                break;
            case Suspect.Description.BodyType:
                image.sprite = spriteBodyRefs[LevelCreator.Instance.Unwrap().currentSuspect.player.currentBodyTypeIndex];
                var c = Color.black;
                c.a = 0.3f;
                image.color = c;
                
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(description), description, null);
        }
        gameObject.SetActive(true);
    }
}
