﻿using UnityEngine;

public class BodyType : MonoBehaviour
{
    public enum Body
    {
        male,
        female,
        strong,
        fat
    }

    public Body type;
}