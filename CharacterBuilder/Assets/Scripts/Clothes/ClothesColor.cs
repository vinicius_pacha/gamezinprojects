﻿using System.Linq;
using UnityEngine;

public class ClothesColor : ScriptableObject
{
    public Color[] colors;

    public Color GetRandomColor()
    {
        return colors[Random.Range(0, colors.Length)];
    }
    public Color GetAnotherColor(Color currentColor)
    {
        var newColors = colors.ToList();
        newColors.Remove(currentColor);
        return newColors[Random.Range(0, newColors.Count)];
    }
}