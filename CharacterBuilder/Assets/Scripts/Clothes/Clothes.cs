﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;
using Random = UnityEngine.Random;

public class Clothes : MonoBehaviour
{
    public Renderer[] playerRenderer;
    public TorsoType upper;
    public LegsType lower;
    
    public enum LegsType
    {
        Pants,
        Shorts
    }

    public enum TorsoType
    {
        Tank,
        TShirt,
        Sleeve,
    }

    [BitStrap.Button]
    public void Setup(Color shirt, Color pants, Color shoes, Color skinColor)
    {
        TextureManager.Instance.Unwrap().face = skinColor; 
        TextureManager.Instance.Unwrap().neck = skinColor;
        TextureManager.Instance.Unwrap().hands = skinColor;
        upper = (TorsoType)Random.Range(0, 3);

        TextureManager.Instance.Unwrap().shirt = shirt;
        TextureManager.Instance.Unwrap().armsLow = shirt;
        TextureManager.Instance.Unwrap().armsUp = shirt;
        TextureManager.Instance.Unwrap().belly = shirt;
        TextureManager.Instance.Unwrap().shoes = shoes;

        switch (upper)
        {
            case TorsoType.Tank:
                TextureManager.Instance.Unwrap().armsLow = skinColor;
                TextureManager.Instance.Unwrap().armsUp = skinColor;
                break;
            case TorsoType.TShirt:
                TextureManager.Instance.Unwrap().armsLow = skinColor;
                break;
            case TorsoType.Sleeve:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
      
        TextureManager.Instance.Unwrap().details = null;

        lower = (LegsType)Random.Range(0, 2);

        TextureManager.Instance.Unwrap().underwear = pants;
        TextureManager.Instance.Unwrap().legsUp = pants;
        TextureManager.Instance.Unwrap().legsLow = pants;
        switch (lower)
        {
            case LegsType.Pants:
                break;
            case LegsType.Shorts:
                TextureManager.Instance.Unwrap().legsLow = skinColor;
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        foreach (var r in playerRenderer)
        {
            r.material.mainTexture = TextureManager.Instance.Unwrap().CreateTextureButton();
        }
    }
}
