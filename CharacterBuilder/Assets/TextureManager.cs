﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using BitStrap;
using UnityEngine;
using UnityEngine.UIElements;

public class TextureManager : BitStrap.Singleton<TextureManager>
{
    public Texture2D tempTexture;
    public Texture2D baseTexture;
    public Texture2D details;
    public Color face;
    public Color neck;
    public Color shirt;
    public Color armsUp;
    public Color armsLow;
    public Color hands;
    public Color underwear;
    public Color legsUp;
    public Color legsLow;
    public Color shoes;
    public Color belly;
    
    void Start()
    {
        //CreateTextureButton();
    }
    [Button]
    public Texture2D CreateTextureButton()
    {
        var t = Instantiate(tempTexture);
        
        for (int i = 0; i < baseTexture.width; i++)
        {
            for (int j = 0; j < baseTexture.height; j++)
            {
                Color c = new Color(0,0,0,0);

                var currentPixel = baseTexture.GetPixel(i, j);
                var currentRed = currentPixel.r * 255;
                switch (currentRed)
                {
                    case 10:
                        currentPixel = Color.white * face;
                        break;
                    case 20:
                        currentPixel = Color.white * neck;
                        break;
                    case 30:
                        currentPixel = Color.white * shirt;
                        break;
                    case 40:
                        currentPixel = Color.white * armsUp;
                        break;
                    case 50:
                        currentPixel = Color.white * armsLow;
                        break;
                    case 60:
                        currentPixel = Color.white * hands;
                        break;
                    case 70:
                        currentPixel = Color.white * underwear;
                        break;
                    case 80:
                        currentPixel = Color.white * legsUp;
                        break;
                    case 90:
                        currentPixel = Color.white * legsLow;
                        break;
                    case 100:
                        currentPixel = Color.white * shoes;
                        break; 
                    case 110:
                        currentPixel = Color.white * belly;
                        break;
                }

                if (details != null)
                {
                    var detailsPixel = details.GetPixel(i, j);
                    currentPixel = Color.Lerp(currentPixel, detailsPixel, detailsPixel.a);
                    
                }
                
                t.SetPixel(i,j,currentPixel);
            }
        } 
        t.Apply();
        return t;
    }
    
}
