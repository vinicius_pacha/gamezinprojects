using UnityEngine;
using UnityEditor;

public static class PrefabHelper
{
    #if UNITY_EDITOR
    public static  void CreatePrefab(GameObject go, string tempFolderName)
    {
        string localPath = string.Format("Assets/#delete/{0}{1}.prefab", tempFolderName, go.name);

        // Make sure the file name is unique, in case an existing Prefab has the same name.
        localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);
        PrefabUtility.UnpackPrefabInstance(go, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
        // Create the new Prefab.
        PrefabUtility.SaveAsPrefabAssetAndConnect(go, localPath, InteractionMode.UserAction);
        
    }

    // Disable the menu item if no selection is in place.
    static bool ValidateCreatePrefab()
    {
        return Selection.activeGameObject != null && !EditorUtility.IsPersistent(Selection.activeGameObject);
    }
    #endif
}