﻿using System.Collections.Generic;
using System.IO;
using BitStrap;
using UnityEditor;
using UnityEngine;

public sealed class CreateAllPrefabs : MonoBehaviour
{
#if UNITY_EDITOR
    public string path;
    public string tempFolderName;
    public List<GameObject> objs = new List<GameObject>();
    
    
    [Button]
    void CreatePrefabs()
    {
        foreach (var ob in objs)
        {
            PrefabHelper.CreatePrefab(ob,tempFolderName);
        }
    }
#endif
}