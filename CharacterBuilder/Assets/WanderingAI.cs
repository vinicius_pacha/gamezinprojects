﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
 
public class WanderingAI : MonoBehaviour {
 
    public float wanderRadius;
    public float wanderTimer;
 
    private Transform target;
    private NavMeshAgent agent;
    private float timer;
 
    // Use this for initialization
    void OnEnable () {
        agent = GetComponent<NavMeshAgent> ();
        timer = 0;
    }
 
    // Update is called once per frame
    void Update () {
        timer -= Time.deltaTime;
 
        if (timer <= 0) {
            Vector3 newPos = RandomNavSphere(transform.position, Random.Range(wanderRadius,wanderRadius*2), -1);
            agent.SetDestination(newPos);
            timer = Random.Range(wanderTimer, wanderTimer*2);
        }
    }
 
    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask) {
        Vector3 randDirection = Random.insideUnitSphere * dist;
 
        randDirection += origin;
 
        NavMeshHit navHit;
 
        NavMesh.SamplePosition (randDirection, out navHit, dist, layermask);
 
        return navHit.position;
    }
}