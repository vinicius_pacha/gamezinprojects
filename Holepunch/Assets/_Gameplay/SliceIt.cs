using BitStrap;
using EzySlice;
using UnityEngine;

public sealed class SliceIt : MonoBehaviour
{
    public Transform plane;
    public GameObject obj;
    [Button]
    void Slice()
    {
        var objs = obj.SliceInstantiate(plane.transform.position, plane.up);

        foreach (var VARIABLE in objs)
        {
            
        }
    }
}
