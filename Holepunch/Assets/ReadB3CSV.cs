using System;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using UnityEngine;

[System.Serializable]
public class B3Line
{
    public string COD;
    public string date;
    public int buy;
    public int sell;
    public float priceBuy;
    public float priceSell;
    public string qntLeft;
    public string position;

    public B3Line(B3Line b3l = null)
    {
        if (b3l == null) return;
        this.COD = b3l.COD;
        this.date = b3l.date;
        this.buy = b3l.buy;
        this.sell = b3l.sell;
        this.priceBuy = b3l.priceBuy;
        this.priceSell = b3l.priceSell;
        this.qntLeft = b3l.qntLeft;
        this.position = b3l.position;
    }

}

public sealed class ReadB3CSV : MonoBehaviour
{
    public string[] test;
    private TextAsset _textAsset;
    public List<B3Line> b3Lines = new List<B3Line>();
    public List<B3Line> dayTrade = new List<B3Line>();
    public List<B3Line> swingTrade = new List<B3Line>();
    public List<B3Line> swingTradeGrouped = new List<B3Line>();
    public string[] filterMonth;

    public float dayTradeProfit = 0;
    public float swingTradeProfit = 0;
    [Button]
    void Convert()
    {
        string line;
        System.IO.StreamReader
            file = new System.IO.StreamReader(Application.dataPath + "/Resources/b3Consolidated.csv"); //load text file with data
        b3Lines.Clear();
        while ((line = file.ReadLine()) != null)
        {
            char[] delimiterChar = {','}; //variable separation
            string[] split = line.Split(delimiterChar, System.StringSplitOptions.None);
            test = split;
            var b3Line = new B3Line();
            b3Line.COD = split[0];
            var dates = split[7].Split('/');
            string monthYear = string.Format("{0}/{1}", dates[1], dates[2]);
                
            b3Line.date = monthYear;
            b3Line.buy = int.Parse(split[14]);
            b3Line.sell = int.Parse(split[20]);
            b3Line.priceBuy = float.Parse(split[28]);
            b3Line.priceSell = float.Parse(split[36]);
            b3Line.qntLeft = split[42];
            b3Line.position = split[46];
            
            if(filterMonth.Contains(b3Line.date) )
                b3Lines.Add(b3Line); //add to database
        }

        file.Close();
    }

    [Button]
    void SeparateDayFromSwing()
    {
        swingTrade.Clear();
        dayTrade.Clear();
        foreach (var b3l in b3Lines)
        {
            if (b3l.buy > 0 && b3l.sell > 0)
            {
                var dayTradeValue = Mathf.Min(b3l.buy, b3l.sell);
                var newDayTradeLine = new B3Line(b3l);
                newDayTradeLine.buy = dayTradeValue;
                newDayTradeLine.sell = dayTradeValue;

                b3l.buy = b3l.buy - dayTradeValue;
                b3l.sell = b3l.sell - dayTradeValue;

                dayTrade.Add(newDayTradeLine);
                if (b3l.buy > 0 || b3l.sell > 0)
                {
                    swingTrade.Add(b3l);
                }
            }
            else
            {
                swingTrade.Add(b3l);
            }
        }
    }

    [Button]
    void GroupAllSwingTrades()
    {
        swingTradeGrouped.Clear();
        List<string> allCodes = new List<string>();
        foreach (var st in swingTrade)
        {
            if (!allCodes.Contains(st.COD))
            {
                allCodes.Add(st.COD);
            }
        }

        foreach (var code in allCodes)
        {
            int buy = 0;
            int sell = 0;

            float avgBuyPrice = 0;
            float avgSellPrice = 0;
            foreach (var st in swingTrade)
            {
                if (st.COD == code)
                {
                    if (st.buy > 0)
                        avgBuyPrice = (buy * avgBuyPrice + st.buy * st.priceBuy) / (buy + st.buy);
                    if (st.sell > 0)
                        avgSellPrice = (sell * avgSellPrice + st.sell * st.priceSell) / (sell + st.sell);
                    buy += st.buy;
                    sell += st.sell;
                }
            }

            var groupedSwing = new B3Line();

            groupedSwing.COD = code;
            groupedSwing.buy = buy;
            groupedSwing.sell = sell;
            groupedSwing.priceBuy = avgBuyPrice;
            groupedSwing.priceSell = avgSellPrice;
            swingTradeGrouped.Add(groupedSwing);
        }
    }

    [Button]
    public void CalculateCustody()
    {
        swingTradeProfit = 0;
        dayTradeProfit = 0;
        foreach (var stg in swingTradeGrouped)
        {
            if (stg.sell > 0)
            {
                var val = stg.sell * stg.priceSell - stg.sell * stg.priceBuy;

                swingTradeProfit += val - stg.sell * stg.priceSell*(0.005f/100.0f);
            }
        }  
        foreach (var stg in dayTrade)
        {
            if (stg.sell > 0)
            {
                var val = stg.sell * stg.priceSell - stg.sell * stg.priceBuy;
                dayTradeProfit += val;
            }
        }
    }


}
