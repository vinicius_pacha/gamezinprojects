using BitStrap;
using UnityEngine;

public sealed class EnviromentSetup : ScriptableObject
{
    public Color grass;
    public Color grassBottom;
    public Color wall;
    public Color road;

    public Color fogTop;
    public Color fog;

    [Button]
    public void Setup()
    {
        EnvironmentManager.Instance.Unwrap().Setup(this);
    }

}
