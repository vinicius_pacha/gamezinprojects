using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public class CreateEnvironment : MonoBehaviour
{
    public GameObject environmentPrefab;
    public Transform environmentParent;
    public List<GameObject> instantiatedEnvironment = new List<GameObject>();
    public float zMultiplier;

    public Vector3 fromScale;
    public Vector3 toScale;
    public float y = -10;
    public float lateralDistanceMin = 5;
    public float lateralDistanceMax = 20;
    public Vector3 GetRandomVector(Vector3 v1, Vector3 v2)
    {
        return new Vector3(
            Random.Range(v1.x, v2.x),
            Random.Range(v1.y, v2.y),
            Random.Range(v1.z, v2.z)
        );
    }
    [Button]
    void CreateEnvironmentCubes()
    {
        for (int i = 0; i < 100; i++)
        {

            float leftRandom = Random.Range(lateralDistanceMin, lateralDistanceMax);
            float rightRandom = Random.Range(-lateralDistanceMin, -lateralDistanceMax);
            var go = Instantiate(environmentPrefab, new Vector3(leftRandom, y, i*Random.Range(zMultiplier,zMultiplier*2)),Quaternion.identity);
            var go2 = Instantiate(environmentPrefab, new Vector3(rightRandom, y, i *Random.Range(zMultiplier,zMultiplier*2)),
                Quaternion.identity);
            
            instantiatedEnvironment.Add(go);
            instantiatedEnvironment.Add(go2);
            go.transform.parent = environmentParent;
            go2.transform.parent = environmentParent;
            go.transform.localScale = GetRandomVector(fromScale, toScale);
            go2.transform.localScale = GetRandomVector(fromScale, toScale);
        }
    }

    [Button]
    void DestroyAll()
    {
        for (int i = 0; i < instantiatedEnvironment.Count; i++)
        {
            DestroyImmediate(instantiatedEnvironment[i].gameObject);
        }
        
        instantiatedEnvironment.Clear();
    }
}
