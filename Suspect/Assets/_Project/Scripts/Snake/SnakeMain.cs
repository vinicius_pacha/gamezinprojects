using UnityEngine;

public sealed class SnakeMain : BitStrap.Singleton<SnakeMain>
{
    public SnakeControl snakeControl;
    public SnakeEffect snakeEffect;
    
    public ColorManager.ColorType ColorType
    {
        get
        {
            return snakeEffect.colorType;
        }
    }

    public void IncreaseSize()
    {
        snakeEffect.Grow();
    }
    public void Remove()
    {
        snakeEffect.Remove();
    }
    

}
