using System;
using System.Collections;
using ControlFreak2;
using UnityEngine;

public sealed class SnakeControl : MonoBehaviour
{
    public float minX = -3;
    public float speed = 5;
    public float initialForwardSpeed = 5;
    public float forwardSpeed = 5;
    public float moveTowardsSpeed = 5;
    public float maxX = 3;
    public float distance = 0.5f;
    public LayerMask environmentMask;
    public LayerMask obstacleMask;
    public float rotationSpeed = 1;
    private float desiredX;
    public int increaseSpeedSize = 5;

    public Vector3 desiredPosition;
    
    private Vector3 currentMousePosition;
    private Vector3 lastPosition;
    private Vector3 initialPosition;
    public Vector3 deltaPosition;

    public bool run;
    private void Start()
    {
        initialForwardSpeed = forwardSpeed;
        initialPosition = Vector3.one * 10000;
        lastPosition = initialPosition;
        run = true;
        MenuWindow.instance.onShow.Register(Reset);
    }

    void Reset()
    {
        run = true;
        var tempPosition = transform.position;
        tempPosition.x = 0;
        desiredX = 0;
        transform.position = tempPosition;
        playEffectOnFinishLine = false;
        SnakeEffect.Instance.Unwrap().ResetSnakeSize();
    }
    [HideInInspector]
    public  Vector3 dir;
    private void Update()
    {
        if (!run) return;
        InputLogic();
        desiredPosition = transform.position;
        desiredPosition.x = desiredX;
        
        float velocityZ = 0;
        if (true)
            velocityZ = forwardSpeed * Time.deltaTime;

        desiredPosition += Vector3.forward * velocityZ;
        
        dir = desiredPosition - transform.position;
        dir.y = 0;
        dir = dir.normalized;
        if(Physics.Raycast(transform.position+Vector3.up*0.5f,dir,out var hit,distance, obstacleMask))
        {
            var coloredObstacle = hit.transform.GetComponent < ColoredObstacle>();
            
            if (coloredObstacle)
            {
                if(coloredObstacle.currentColor != SnakeMain.Instance.Unwrap().ColorType)
                {
                  
                    //SnakeMain.Instance.Unwrap().Remove();
                    //transform.position -= dir * Time.deltaTime;
                    //return;
                }
            }
        }
        transform.position = Vector3.MoveTowards(transform.position,desiredPosition,moveTowardsSpeed*Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(dir),Time.deltaTime*rotationSpeed);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position+Vector3.up*0.5f,transform.position+Vector3.up*0.5f+dir.normalized * distance);
    }

    void InputLogic()
    {
        currentMousePosition = Input.mousePosition;
        if (Input.anyKeyDown)
        {
            lastPosition = currentMousePosition;
        }
        if (Input.anyKey && lastPosition != initialPosition)
        {
            deltaPosition.x = Mathf.Lerp(deltaPosition.x, currentMousePosition.x-lastPosition.x, Time.deltaTime*12);
            if (Mathf.Abs(deltaPosition.x) > 0)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position+Vector3.up*0.5f , transform.right * Mathf.Sign(deltaPosition.x),out hit, distance,environmentMask))
                {
                    Debug.Log(hit.transform.gameObject.name);
                    //deltaPosition.x = 0;
                } 
                else if (Physics.Raycast(transform.position+Vector3.up*0.5f+transform.forward * 0.4f , transform.right * Mathf.Sign(deltaPosition.x),out hit, distance,environmentMask))
                {
                   // deltaPosition.x = 0;
                }
                else if (Physics.Raycast(transform.position+Vector3.up*0.5f-transform.forward * 0.4f , transform.right * Mathf.Sign(deltaPosition.x),out hit, distance,environmentMask))
                {
                   // deltaPosition.x = 0;
                }
            }

            
        }
        desiredX += deltaPosition.x * Time.deltaTime * speed;
        desiredX = Mathf.Clamp(desiredX, minX, maxX);
        lastPosition = currentMousePosition;
    }

    public void FinishLine(float seconds,Vector3 position)
    {
        StartCoroutine(FinishLineCoroutine(seconds));
        StartCoroutine(PlayEffectOnFinishLine(position));
    }

    public bool playEffectOnFinishLine = false;
    IEnumerator PlayEffectOnFinishLine(Vector3 position)
    {
        playEffectOnFinishLine = true;
        SnakeEffect snakeEffect = SnakeEffect.Instance.Unwrap();
        for (int i = 0; i < snakeEffect.snakeSize; i++)
        {
            snakeEffect.PlayDeathEffectAtPosition(position+Vector3.up,snakeEffect.colorType);
            if(snakeEffect.snakeSize - i > 3)
                snakeEffect.transform.position -= snakeEffect.transform.forward*Time.deltaTime*snakeEffect.snakeIncreasePerSize*SnakeMain.Instance.Unwrap().snakeControl.forwardSpeed;
            yield return new WaitForSeconds(snakeEffect.snakeIncreasePerSize);
        }
    }
    IEnumerator FinishLineCoroutine(float seconds)
    {
        SnakeEffect snakeEffect = SnakeEffect.Instance.Unwrap();
        yield return new WaitForSeconds(snakeEffect.snakeIncreasePerSize);
        snakeEffect.UpdateSnakeSizeToFinish();
        yield return new WaitForSeconds(seconds);
        snakeEffect.StopEmit();
        run = false;
        if (Physics.Raycast(transform.position + Vector3.up * 0.5f, Vector3.down, out var hit, 2, environmentMask))
        {
            Debug.Log(hit.transform.gameObject.name);
            var finalLine = hit.transform.gameObject.GetComponent<FinalLineMultiplier>();
            if (finalLine)
            {
                finalLine.Animate();
                yield return new WaitForSeconds(0.1f);
                CoinsFeedback.Instance.Unwrap().EarnCoinFromPosition(finalLine.multiplierValue,SnakeMain.Instance.Unwrap().transform.position+Vector3.up,true);
            }
        }

        snakeEffect.snakeSize = 3;
        playEffectOnFinishLine = false;

        yield return new WaitForSeconds(2f);
        PachaGamesManager.instance.Victory();
    }
    
    
    
    
}
