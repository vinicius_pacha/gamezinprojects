using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using UnityEngine.UIElements;

public class SnakeHead : MonoBehaviour
{
    public float time = 0.1f;
    public float distanceToEat = 1f;
    public AnimationCurve scaleCurve;
    public AnimationCurve yPositionCurve;
    private SnakeControl snakeControl;
    public LayerMask obstacleMask;
    private void Start()
    {
        snakeControl = SnakeMain.Instance.Unwrap().snakeControl;
    }

    private void OnTriggerEnter(Collider other)
    {
        var co = other.GetComponent<ColoredObstacle>();
        if(co)
            HandleObstacleFromTrigger(co);
        else if(other.gameObject.CompareTag("Coin"))
        {
            var coin = other.transform.GetComponent<Coin>();
            if (coin)
                coin.GetCoin();
            
        }
    }
    void HandleObstacle(Vector3 point, ColoredObstacle co)
    {
        if (co != null)
        {
            if (co.gameObject.layer == LayerMask.NameToLayer("NotDestroyable"))
            {
                return;
            }
                
            if (co.currentColor == SnakeMain.Instance.Unwrap().ColorType)
            {
                co.col.enabled = false;
                StartCoroutine(ScaleTransform(point, co.transform,co.currentColor,true));
            }
        }
    }    
    void HandleObstacleFromTrigger(ColoredObstacle co)
    {
        if (co != null)
        {
            if (co.gameObject.layer == LayerMask.NameToLayer("NotDestroyable"))
            {
                return;
            }
                
            if (co.currentColor == SnakeMain.Instance.Unwrap().ColorType)
            {
                co.col.enabled = false;
                StartCoroutine(ScaleTransform(co.transform.position, co.transform,co.currentColor,true));
            }
            else if(co.currentColor != SnakeMain.Instance.Unwrap().ColorType)
            {
                StartCoroutine(ScaleTransform(co.transform.position, co.transform, co.currentColor, false));
            }
        }
    }

    IEnumerator ScaleTransform(Vector3 hitPoint, Transform tr, ColorManager.ColorType currentColor, bool increase)
    {
        float t = 0;
        Vector3 initialScale = tr.transform.localScale;
        Vector3 desiredScale = Vector3.zero;
        Vector3 initialPosition = tr.transform.position;
        Vector3 desiredPosition = SnakeMain.Instance.Unwrap().transform.position;
        //SnakeMain.Instance.Unwrap().IncreaseSize();
        while (t < 1 && Vector3.Distance(hitPoint,SnakeMain.Instance.Unwrap().transform.position + Vector3.up*0.5f) > 0.75f)
        {
            if (tr)
            {
                tr.transform.localScale = Vector3.Lerp(initialScale,desiredScale,scaleCurve.Evaluate(t));
                tr.transform.position = Vector3.Lerp(tr.transform.position,SnakeMain.Instance.Unwrap().transform.position+Vector3.up*0.5f,t);
                Vector3 tempPosition = tr.transform.position;
                tempPosition.y = Mathf.Lerp(0,1f,yPositionCurve.Evaluate(t));
                tr.transform.position = tempPosition;
            }
            t += Time.deltaTime/time;
            desiredPosition = SnakeMain.Instance.Unwrap().transform.position;

            yield return null;
        }

        if (increase)
        {   
            SnakeEffect.Instance.Unwrap().PlayEatEffectAtPosition(desiredPosition+Vector3.up,currentColor);
            SnakeMain.Instance.Unwrap().IncreaseSize();
        }
        
        else
        {
            SnakeMain.Instance.Unwrap().Remove();
            SnakeEffect.Instance.Unwrap().PlayDeathEffectAtPosition(desiredPosition+Vector3.up,currentColor);

            (InGameWindow.instance as InGameWindow).Down();
        }
        if(tr)
            Destroy(tr.gameObject);
    }

    private void FixedUpdate()
    {
        if (Physics.Raycast(transform.position + Vector3.up * 0.5f, snakeControl.dir, out var hit, distanceToEat, obstacleMask))
        {
            var co = hit.transform.GetComponent<ColoredObstacle>();
            if(co)
                HandleObstacle(hit.point,co);          
        }
    }
}