using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;

public sealed class SnakeEffect : BitStrap.Singleton<SnakeEffect>
{
    public Renderer snakeHeadRenderer;
    public TrailRenderer trail;
    public ColorManager.ColorType colorType;
    public ParticleSystem deathEffect;
    public ParticleSystem eatEffect;
    public ParticleSystem snakeParticle;
    public ParticleSystem speedUpParticle;
    public float snakeIncreasePerSize = 0.1f;
    public int snakeSize = 1;
    public float snakeRemoveDelay = 1;
    public float currentSnakeRemoveDelay = 1;
    public float startSize, finalSize, timeToIncreaseSnake;
    private Vector3 lastPosition;
    public TMP_Text sizeText;
    public Jun_TweenRuntime sizeTextTween;

    private float snakeIncreasePerSizeInitial;
    private float t;
    public Jun_TweenRuntime tween;

    private void Start()
    {
        snakeIncreasePerSizeInitial = snakeIncreasePerSize;
    }

    [Button]
    public void TestTween()
    {
        tween.Play();
    }

    Color Vec4ToColor(Vector4 vec)
    {
        return new Color(vec.x, vec.y, vec.z, vec.w);
    }

    [BitStrap.Button]
    public void Remove()
    {
        if (snakeSize == 0) return;
        if (currentSnakeRemoveDelay > 0)
        {
            return;
        }

        ParticleSystem.MainModule main = snakeParticle.main;
        main.startSize = finalSize;
        snakeParticle.Emit(1);
        main.startSize = startSize;
        PlayDeathEffectAtPosition(transform.position + Vector3.up * 0.5f, colorType);
        snakeSize--;
        currentSnakeRemoveDelay = snakeRemoveDelay;
        UpdateSnakeSize();
        IncreaseSpeed();
        if (snakeSize <= 0)
        {
            PachaGamesManager.instance.Death();
            SnakeMain.Instance.Unwrap().snakeControl.run = false;
        }

    }

    IEnumerator Flash()
    {
        var r = snakeParticle.GetComponent<ParticleSystemRenderer>();
        r.sharedMaterial.SetColor("_AmbientColor",Color.white);
     
        t = 0;
        while (t < 0.3f)
        {
            t += Time.deltaTime;
            r.sharedMaterial.SetFloat("_AmbientPower",0.4f- t);
            yield return null;
        }
        t = 0;
        r.sharedMaterial.SetFloat("_AmbientPower",0.1f);

    }

    private void Update()
    {
        currentSnakeRemoveDelay -= Time.deltaTime;
        if (PachaGamesManager.instance.currentGameState != PachaGamesManager.GameState.INGAME)
            sizeText.text = "";
        else
        {
            sizeText.text = snakeSize.ToString();
        }
    }

    public void UpdateSnakeSize()
    {
        ParticleSystem.MainModule main = snakeParticle.main;
        main.startLifetime = snakeSize * snakeIncreasePerSize;
        IncreaseSpeed();
    }
    public void ResetSnakeSize()
    {
        snakeParticle.Stop();
        snakeParticle.Simulate(0);
        snakeParticle.Play();
        snakeSize = 3;
        snakeIncreasePerSize = snakeIncreasePerSizeInitial;
        StartEmit();
        UpdateSnakeSize();
        IncreaseSpeed();
    }

    public void StopEmit()
    {
        var emission = snakeParticle.emission;
        emission.enabled = false;
    }
    public void StartEmit()
    {
        var emission = snakeParticle.emission;
        emission.enabled = true;
    }
    public void UpdateSnakeSizeToFinish()
    {
        ParticleSystem.MainModule main = snakeParticle.main;
        main.startLifetime = Mathf.Infinity;
    }
    
    [Button]
    public void Grow()
    {
        snakeSize++;
        StopAllCoroutines();
        StartCoroutine(Flash());
        sizeTextTween.Play();
        UpdateSnakeSize();
        ParticleSystem.MainModule main = snakeParticle.main;
        main.startSize = finalSize;
        snakeParticle.Emit(1);
        main.startSize = startSize;
    }

    void IncreaseSpeed()
    {
        SnakeControl sc = SnakeMain.Instance.Unwrap().snakeControl;
        ParticleSystem.MainModule main = snakeParticle.main;
        float initialSpeed = sc.forwardSpeed;
        sc.forwardSpeed = sc.initialForwardSpeed + snakeSize / sc.increaseSpeedSize;
        float finalSpeed = sc.forwardSpeed;

        snakeIncreasePerSize = snakeIncreasePerSizeInitial / (sc.forwardSpeed / sc.initialForwardSpeed);
        main.startLifetime = snakeSize * snakeIncreasePerSize;

        if (finalSpeed > initialSpeed)
        {
            (InGameWindow.instance as InGameWindow).SpeedUp();
            ParticleSystem.MainModule main2 = speedUpParticle.main;
            var c = ColorManager.Instance.Unwrap().GetColor(colorType);
            c.a = 1;
            main2.startColor = c;
            speedUpParticle.Play();


        }
        else if (finalSpeed < initialSpeed)
        {
            (InGameWindow.instance as InGameWindow).SpeedDown();
        }

    }
    
    [Button]
    public void ChangeColor(ColorManager.ColorType colorType)
    {
        snakeHeadRenderer.material = ColorManager.Instance.Unwrap().GetMaterial(colorType);
        var c = ColorManager.Instance.Unwrap().GetColor(colorType);
        c.a = 0.3f;
        trail.startColor = c;
        trail.endColor = c;
        this.colorType = colorType;
        ParticleSystem.MainModule main = snakeParticle.main;
        main.startColor =ColorManager.Instance.Unwrap().GetColor(colorType);
    }

    public void PlayDeathEffectAtPosition(Vector3 position, ColorManager.ColorType color)
    {
        CameraShake.Instance.Unwrap().SetShake();
        ParticleSystem.MainModule main = deathEffect.main;
        main.startColor = ColorManager.Instance.Unwrap().GetColor(color) + new Color(0.1f,0.1f,0.1f,1);
        deathEffect.transform.position = position;
        deathEffect.Play();

    }public void PlayEatEffectAtPosition(Vector3 position, ColorManager.ColorType color)
    {
        CameraShake.Instance.Unwrap().SetShake();
        ParticleSystem.MainModule main = eatEffect.main;
        main.startColor = ColorManager.Instance.Unwrap().GetColor(color) + new Color(0.1f,0.1f,0.1f,1);
        eatEffect.transform.position = position;
        eatEffect.Play();

    }
    
}
