using System;
using System.Collections.Generic;
using System.Runtime;
using BitStrap;
using UnityEngine;

public sealed class ColoredObstacle : MonoBehaviour
{
    public List<Renderer> renderers;
    public List<SpriteRenderer> sprites;
    public List<TrailRenderer> trails;

    public ColorManager.ColorType startColor;
    public ColorManager.ColorType currentColor;

    public Collider col;

    private void Awake()
    {
        col = GetComponent<Collider>();
    }

    public void Setup(ColorManager.ColorType c)
    {
        currentColor = c;
        Setup();
    }

    [Button]
    void Setup()
    {
        foreach (var r in renderers)
        {
            r.material = ColorManager.Instance.Unwrap().GetMaterial(currentColor);
        }

        foreach (var s in sprites)
        {
            float a = s.color.a;
            Color c = ColorManager.Instance.Unwrap().GetColor(currentColor);
            c.a = a;
            s.color = c;
        }
        foreach (var t in trails)
        {
            Color c = ColorManager.Instance.Unwrap().GetColor(currentColor);
            c.a = 1;
            t.startColor = c;
            c.a = 0;
            t.endColor = c;
            
        }
    }
    
    [Button]
    void SetupInitial()
    {
        Setup(startColor);
    }

    private void Start()
    {
        Setup();
    }
}
