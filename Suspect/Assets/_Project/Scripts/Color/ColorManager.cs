using UnityEngine;

public sealed class ColorManager : BitStrap.Singleton<ColorManager>
{
    public ColorConfig config;
    
    public enum ColorType
    {
        CYAN,
        BLUE,
        PURPLE,
        PINK,
        RED,
        ORANGE,
        YELLOW,
        GREEN
    }

    public Material GetMaterial(ColorType c)
    {
        return config.GetMaterial(c);
    }
    public Color GetColor(ColorType c)
    {
        return config.GetColor(c);
    }
}
