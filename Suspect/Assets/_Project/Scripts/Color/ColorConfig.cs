using UnityEngine;

[System.Serializable]

public class ColorConfig : ScriptableObject
{
    public Material[] colorMaterials;

    public Color GetColor(ColorManager.ColorType colorType)
    {
        return colorMaterials[(int)colorType].GetColor("_Color");
    } 
    public Material GetMaterial(ColorManager.ColorType colorType)
    {
        return colorMaterials[(int)colorType];
    }

}
