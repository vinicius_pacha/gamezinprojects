using UnityEngine;

public sealed class EnvironmentStep : MonoBehaviour
{
    public GameObject[] prefab;

    public void ActivatePrefabByIndex(int index)
    {
        for (int i = 0; i < prefab.Length; i++)
        {
            prefab[i].SetActive(false);
            if (i == index)
                prefab[i].SetActive(true);
        }
    }
}
