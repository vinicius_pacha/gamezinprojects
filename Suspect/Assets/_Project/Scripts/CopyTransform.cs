using System;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public sealed class CopyTransform : MonoBehaviour
{
    public Vector3 offset;
    public Transform target;
    public bool unparentOnPlay;
    public float speed = 5;
    private void Start()
    {
        if (unparentOnPlay)
            transform.parent = null;
    }

    private void Update()
    {
        transform.position = Vector3.Slerp(transform.position, target.position + offset, Time.deltaTime * speed);
    }
}
