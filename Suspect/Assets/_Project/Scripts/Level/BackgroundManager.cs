using System;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public sealed class BackgroundManager : BitStrap.Singleton<BackgroundManager>
{
    public Camera camera;
    public Renderer fog;
    public Renderer building;
    public SpriteRenderer fogTop;
    public BackgroundConfig config;

    public BackgroundConfig[] configs;

    private void Start()
    {
        MenuWindow.instance.onShow.Register(SetupConfigOnCurrentLevel);
    }

    void SetupConfig()
    {
        SetupConfig(config);
    }
    public void SetupConfig(BackgroundConfig config)
    {
        fog.sharedMaterial.SetColor("_Color",config.fogColor);
        building.sharedMaterial.SetColor("_LightTint",config.buildingColor);
        fogTop.color = config.fogTop;
        camera.backgroundColor = config.skyColor;
        RenderSettings.fogColor = config.skyColor;
    } 
    
   

    void SetupConfigOnCurrentLevel()
    {
        int levelIndex = LevelManager.instance.level % configs.Length;
        SetupConfig(configs[levelIndex]);
    }
    
    
}
