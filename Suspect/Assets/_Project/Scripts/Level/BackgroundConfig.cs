﻿using BitStrap;
using UnityEngine;

public class BackgroundConfig:ScriptableObject
{
    public Color skyColor;
    public Color fogColor;
    public Color buildingColor;
    public Color fogTop;
    [Button]
    public void Setup()
    {
        BackgroundManager.Instance.Unwrap().SetupConfig(this);
    }
}