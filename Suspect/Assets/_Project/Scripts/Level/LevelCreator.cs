using System;
using System.Collections.Generic;
using System.Reflection;
using BitStrap;
using UnityEngine;
using Random = UnityEngine.Random;


public class LevelStepClass
{
    public ColorManager.ColorType currentColor;
    public List<ColoredObstaclePrefab> obstaclesPrefab = new List<ColoredObstaclePrefab>();
    
    [Button]
    public void ShiftObjstaclesColor()
    {
        foreach (var o in obstaclesPrefab)
        {
            foreach (var co in o.coloredObstacles)
            {
                var desiredColor = (int) co.startColor + (int)currentColor;
                desiredColor = desiredColor % 8;
            
                co.Setup((ColorManager.ColorType) desiredColor);
            }
          
        }
        
    }
}
public sealed class LevelCreator : BitStrap.Singleton<LevelCreator>
{
    public ColoredObstaclePrefab[] prefabs;
    public ColoredObstaclePrefab colorChanger;
    public List<GameObject> objectsCreated = new List<GameObject>();
    public ColorManager.ColorType color;
    public ColoredObstaclePrefab finishLine;
    public int steps = 1;
    public float distanceBetween = 10;
    public bool testingSteps = false;

    public List<int> indexes = new List<int>();
    private Vector3 initialPosition;
    [Button]
    public void CreateNewLevel()
    {
        var snakePosition = SnakeMain.Instance.Unwrap().transform.position;
        snakePosition.x = 0;
        foreach (var o in objectsCreated)
        {
            Destroy(o.gameObject);
            
        }
        objectsCreated.Clear();
        initialPosition = snakePosition + Vector3.forward * distanceBetween;
        if (testingSteps)
        {
            CreateAllLevels(initialPosition,  (ColorManager.ColorType.CYAN));
        }
        else if(LevelManager.instance.level == 1)
        {
            CreateFirstLevels(initialPosition,  (ColorManager.ColorType.CYAN));
        }
        else if (LevelManager.instance.level == 2)
        {
            indexes.Clear();
            CreateLevelStep(initialPosition,  (ColorManager.ColorType.CYAN) );
            CreateLevelStep(finishLine.transform.position, (ColorManager.ColorType)Random.Range(0,8));
        }
        else
        {
            indexes.Clear();
            CreateLevelStep(initialPosition,  (ColorManager.ColorType)Random.Range(0,8));
            CreateLevelStep(finishLine.transform.position, (ColorManager.ColorType)Random.Range(0,8));
            CreateLevelStep(finishLine.transform.position,  (ColorManager.ColorType)Random.Range(0,8));
        }
    }

    private void Start()
    {
        MenuWindow.instance.onShow.Register(HideFinishLine);
       
    }

    void HideFinishLine()
    {
        finishLine.transform.position = new Vector3(-100,-100,-100); 
        foreach (var o in objectsCreated)
        {
            Destroy(o.gameObject);
            
        }
        objectsCreated.Clear();
    }

    void CreateLevelStep(Vector3 initialPosition, ColorManager.ColorType colorType)
    {
        LevelStepClass ls = new LevelStepClass();
        ls.currentColor = colorType;
        ls.obstaclesPrefab.Add(finishLine);
        var currentPosition = initialPosition + Vector3.forward * distanceBetween;
        var cc = Instantiate(colorChanger, currentPosition, Quaternion.identity);
       
        objectsCreated.Add(cc.gameObject);
        ls.obstaclesPrefab.Add(cc);
        currentPosition += Vector3.forward * distanceBetween*2; 
        
        float distance = 0;
        while (distance < 50)
        {
            var levelPrefab = GetRandomPrefab();
            currentPosition += transform.forward * (levelPrefab.distance*0.5f);

            var co = Instantiate(levelPrefab, currentPosition, Quaternion.identity);
            if (Random.Range(0, 100) < 50)
            {
                co.transform.Rotate(0,180,0);
            }
            objectsCreated.Add(co.gameObject);
            ls.obstaclesPrefab.Add(co);
            currentPosition += Vector3.forward*co.distance*0.5f;
            currentPosition += Vector3.forward * distanceBetween;
            distance += co.distance;
            distance += distanceBetween;
        }
        ls.ShiftObjstaclesColor();
        finishLine.gameObject.SetActive(true);
        finishLine.transform.position = currentPosition;
    }
    ColoredObstaclePrefab GetRandomPrefab()
    {
        int maxPrefab = LevelManager.instance.level + 4;
        maxPrefab = Mathf.Min(maxPrefab, prefabs.Length);
        int index = Random.Range(0, maxPrefab);

        while (indexes.Contains(index))
        {
            index = Random.Range(0, maxPrefab);
        }
        
        indexes.Add(index);
        return prefabs[index];
    }

    public float GetLevelProgress()
    {
        float initialZ = initialPosition.z;
        float finalZ = finishLine.transform.position.z;
        float currentZ = SnakeMain.Instance.Unwrap().transform.position.z;

        return Mathf.InverseLerp(initialZ, finalZ, currentZ);
    }
    
    void CreateAllLevels(Vector3 initialPosition, ColorManager.ColorType colorType)
    {
        LevelStep ls = new LevelStep();
        ls.currentColor = colorType;
        ls.obstaclesPrefab.Add(finishLine);
        var currentPosition = initialPosition + Vector3.forward * distanceBetween;
        var cc = Instantiate(colorChanger, currentPosition, Quaternion.identity);
        objectsCreated.Add(cc.gameObject);
        ls.obstaclesPrefab.Add(cc);
        currentPosition += Vector3.forward * distanceBetween*2; 
        
        float distance = 0;
        for(int i = 0; i < prefabs.Length; i++)
        {
            var levelPrefab = prefabs[i];
            currentPosition += transform.forward * (levelPrefab.distance*0.5f);
            var co = Instantiate(levelPrefab, currentPosition, Quaternion.identity);
            objectsCreated.Add(co.gameObject);
            ls.obstaclesPrefab.Add(co);
            currentPosition += Vector3.forward*(co.distance*0.5f);
            currentPosition += Vector3.forward * distanceBetween;
            distance += co.distance;
            distance += distanceBetween;
        }
        ls.ShiftObjstaclesColor();
        finishLine.gameObject.SetActive(true);
        finishLine.transform.position = currentPosition;
    }
    
    void CreateFirstLevels(Vector3 initialPosition, ColorManager.ColorType colorType)
    {
        LevelStep ls = new LevelStep();
        ls.currentColor = colorType;
        ls.obstaclesPrefab.Add(finishLine);
        var currentPosition = initialPosition + Vector3.forward * distanceBetween;
        
        var cc = Instantiate(colorChanger, currentPosition, Quaternion.identity);
        objectsCreated.Add(cc.gameObject);
        ls.obstaclesPrefab.Add(cc);
        currentPosition += Vector3.forward * distanceBetween*2; 
        
        float distance = 0;
        for(int i = 0; i < 4; i++)
        {
            var levelPrefab = prefabs[i];
            currentPosition += transform.forward * (levelPrefab.distance*0.5f);
            var co = Instantiate(levelPrefab, currentPosition, Quaternion.identity);

            objectsCreated.Add(co.gameObject);
            ls.obstaclesPrefab.Add(co);
            currentPosition += Vector3.forward*co.distance*0.5f;
            currentPosition += Vector3.forward * distanceBetween;
            distance += co.distance;
            distance += distanceBetween;
        }
        ls.ShiftObjstaclesColor();
        finishLine.gameObject.SetActive(true);
        finishLine.transform.position = currentPosition;
    }
}
