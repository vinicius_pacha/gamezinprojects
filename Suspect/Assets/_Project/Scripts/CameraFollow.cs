using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public SnakeMain snake;
    public Transform inGameTransform;
    public Transform menuTransform;
    public Transform finalScreen;
    public Transform endScreenTransform;

    public float speed;
    public float rotationSpeed;

    private Vector3 desiredPosition;

    private Transform desiredTransform;

    // Start is called before the first frame update
    void Start()
    {
        snake = SnakeMain.Instance.Unwrap();
    }
    
    // Update is called once per frame
    void LateUpdate()
    {
        switch (PachaGamesManager.instance.currentGameState)
        {
            case PachaGamesManager.GameState.MENU:
                desiredTransform = menuTransform;
                break; 
            case PachaGamesManager.GameState.VICTORY:
                desiredTransform = inGameTransform;
                break;
            case PachaGamesManager.GameState.INGAME:
                desiredTransform = inGameTransform;
                break;
            default:
                desiredTransform = inGameTransform;
                break;
        }
        int snakeSize = snake.snakeEffect.snakeSize;
        desiredPosition = snake.transform.position;
        desiredPosition.x = 0;
        var snakeSizeModifier = transform.forward * snakeSize * 0.5f;

        if (snake.snakeControl.playEffectOnFinishLine)
        {
            Transform finishLine = FinishLine.Instance.Unwrap().transform;
            transform.position = Vector3.Lerp(transform.position, finishLine.transform.position + finalScreen.transform.position, Time.deltaTime * speed * 0.3f);
            transform.rotation = Quaternion.Slerp(transform.rotation, finalScreen.transform.rotation, Time.deltaTime * rotationSpeed * 0.3f);
        }
        else
        {
            transform.position = Vector3.Lerp(transform.position, desiredPosition + desiredTransform.position - snakeSizeModifier, Time.deltaTime * speed);
            transform.rotation = Quaternion.Slerp(transform.rotation, desiredTransform.rotation, Time.deltaTime * rotationSpeed);
        }
    }
}
