using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishLine : BitStrap.Singleton<FinishLine>
{
    private void OnTriggerEnter(Collider other)
    {
        var control = other.transform.parent.GetComponent<SnakeControl>();
        if (control)
        {
            control.FinishLine(SnakeEffect.Instance.Unwrap().snakeSize * SnakeEffect.Instance.Unwrap().snakeIncreasePerSize,SnakeMain.Instance.Unwrap().transform.position);
            float totalTime = 0.3f + SnakeEffect.Instance.Unwrap().snakeSize * SnakeEffect.Instance.Unwrap().snakeIncreasePerSize;
            (InGameWindow.instance as InGameWindow).SetupFinalLevelProgress(totalTime);
        }
    }
}
