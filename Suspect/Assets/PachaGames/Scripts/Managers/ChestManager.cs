using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using UnityEngine;

public class ChestManager : BitStrap.Singleton<ChestManager>
{
    public int chestIndex = 0;
    
    const string CHEST_INDEX = "CHEST_INDEX";
    
    public int ChestIndex
    {
        get => chestIndex;
        set
        {
            PlayerPrefs.SetInt(CHEST_INDEX, value);
            chestIndex = value;
        }
    }

    public int firstChestClass = 0;
    public int[] coinValues; 

    private void Start()
    {
        if (!PlayerPrefs.HasKey(CHEST_INDEX))
        {
            PlayerPrefs.SetInt(CHEST_INDEX, chestIndex);
        }
        else
        {
            chestIndex = PlayerPrefs.GetInt(CHEST_INDEX);
        }
    }

    public void IncreaseChestIndexReward()
    {
        ChestIndex++;
    }

    public CustomizationClass[] GetAllClassesForChest()
    {
        var a = CustomizationManager.Instance.Unwrap().customizationData.Where(x=> x.owned<1 && x.customizationObjectConfig.category.unlockType == CustomizationCategory.UnlockType.Chest).Select(x => x.customizationObjectConfig.customizationClass).Distinct().ToList();
        CustomizationClass[] newArray = a.OrderBy((x) => x.priority).ToArray();

        return newArray;
    }
    
    public CustomizationData GetCurrentChestReward()
    {
        CustomizationClass[] classes = GetAllClassesForChest();
            
        var classIndex = (ChestIndex + firstChestClass) % classes.Length;
        CustomizationClass chestClass = classes[classIndex];
        var a = CustomizationManager.Instance.Unwrap().customizationData.First(x=> x.owned<1 && 
                                                                                   x.customizationObjectConfig.category.unlockType == CustomizationCategory.UnlockType.Chest &&
                                                                                   x.customizationObjectConfig.customizationClass == chestClass);
        
        Debug.Log("CURRENT CHEST REWARD");
        Debug.Log("CLASS:" + chestClass.name);
        Debug.Log("CATEGORY:" + a.Unwrap().customizationObjectConfig.category.name);
        return a.Unwrap();
    }

    public void AquireCurrentReward(int rewardId)
    {
        CustomizationManager.Instance.Unwrap().AquireCustomization(rewardId, true);
        IncreaseChestIndexReward();
    }
}
