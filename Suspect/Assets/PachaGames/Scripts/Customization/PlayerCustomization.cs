using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  PlayerCustomization : CustomizationObject
{
    public GameObject[] hats;
    public Material customizationMaterial;

    public override void EquipCustomization()
    {
        base.EquipCustomization();
    }
}
