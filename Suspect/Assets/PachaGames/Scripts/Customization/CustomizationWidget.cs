using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CustomizationWidget : MonoBehaviour
{
    public Image sprite;
    public int owned;
    CustomizationData _currentCustomizationData;
    public Jun_TweenRuntime glow;
    public Jun_TweenRuntime ownedTween;
    
    public GameObject ownedObject;
    public GameObject notOwned;
    
    public void Setup(CustomizationData customizationData)
    {
        //name.text = string.Format("id{0}_{1}", customizationData.id, customizationData.customizationObjectConfig.name);
        this._currentCustomizationData = customizationData;
        sprite.sprite = _currentCustomizationData.customizationObjectConfig.customizationSprite;
        owned = customizationData.owned;
        
        ownedObject.SetActive(owned == 1);
        notOwned.SetActive(owned == 0);
    }
    public void EquipCustomization()
    {
        CustomizationManager.Instance.Unwrap().EquipCustomization(_currentCustomizationData);
      
        (CustomizationWindow.instance as CustomizationWindow).UpdateWidgets();
    }

    public void AquireCustomization()
    {
        ownedTween.Play();
        owned = 1;
        ownedObject.SetActive(owned == 1);
        notOwned.SetActive(owned == 0);
        CustomizationManager.Instance.Unwrap().AquireCustomization(_currentCustomizationData.id,true);
    }
    public void PlayGlowAnimation()
    {
        glow.Play();
    }
}
