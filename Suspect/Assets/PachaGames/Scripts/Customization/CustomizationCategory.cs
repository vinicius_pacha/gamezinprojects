using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class  CustomizationCategory : ScriptableObject
{
    public string name;
    public enum UnlockType
    {
        Random,
        Chest,
        Progress
    }

    public int priority;
    public UnlockType unlockType;
    public int price = 100;
    public string description;
}
