using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;

public class CoinPlus : BitStrap.Singleton<CoinPlus>
{
    public Jun_TweenRuntime coinTween;
    public TMP_Text coinText;
    public Color positiveColor;
    public Color negativeColor;

    public void CoinFeedback(int value)
    {
        if (value == 1) return;
        coinText.text = string.Format("+{0}", value);
        if (value > 0)
        {
            coinText.color = positiveColor;
        }
        else
        {
            coinText.text = string.Format("{0}", value);

            coinText.color = negativeColor;
        }
        coinTween.PlayAtTime(0);
        coinTween.Play();
    }
}
