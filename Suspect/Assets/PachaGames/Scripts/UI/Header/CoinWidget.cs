using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public class CoinWidget : MonoBehaviour
{
    public Jun_TweenRuntime coinTween;
    public TMP_Text coinText;
    public float timeBetweenPlay;

    private float lastCoins;
    private float lastTimePlayed;

    private void Start()
    {
        coinText.text = CoinManager.Instance.Unwrap().Coins.ToString();
    }

    void Update()
    {
        lastTimePlayed += Time.deltaTime;
        if (lastCoins != CoinManager.Instance.Unwrap().Coins)
        {
            if (lastTimePlayed > timeBetweenPlay)
            {
                coinTween.PlayAtTime(0);
                coinTween.Play();
                lastTimePlayed = 0;
            }
            
            coinText.text = CoinManager.Instance.Unwrap().Coins.ToString();
        }

        lastCoins = CoinManager.Instance.Unwrap().Coins;
    }
}
