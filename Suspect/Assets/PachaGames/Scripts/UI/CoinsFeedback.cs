using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using UnityEngine;

public class CoinsFeedback : BitStrap.Singleton<CoinsFeedback>
{
    public CoinsFeedbackEntry[] entries;
    public Transform finalLocation;
    public float randomForce = 1;
    public RectTransform canvasRect;
    public Camera mainCamera;
    public float breakSpeed = 0.7f;
    private int thisQuantity = 0;
    CoinsFeedbackEntry finalFeedbackEntry;

    [Button]
    void GetEntries()
    {
        entries = GetComponentsInChildren<CoinsFeedbackEntry>(true);
    }

    Vector3 GetCanvasPosition(Vector3 worldPosition)
    {
        Vector2 viewportPosition= mainCamera.WorldToViewportPoint(worldPosition);
        Vector2 worldObjectScreenPosition=new Vector2(
            ((viewportPosition.x*canvasRect.sizeDelta.x)-(canvasRect.sizeDelta.x*0.5f)),
            ((viewportPosition.y*canvasRect.sizeDelta.y)-(canvasRect.sizeDelta.y*0.5f)));

        return worldObjectScreenPosition;
    }
   
    [Button]
    public void EarnCoinFromPosition(int quantity, Vector3 position, bool isWorldPosition=false)
    {
        StartCoroutine(EarnCoinFromPositionCoroutine(quantity, position, isWorldPosition));
    }

    IEnumerator EarnCoinFromPositionCoroutine(int quantity, Vector3 position, bool isWorld = false)
    {
        thisQuantity = quantity;
        CoinManager.Instance.Unwrap().enabled = false;
        CoinManager.Instance.Unwrap().EarnCoins(thisQuantity);

        var force = randomForce;
        if (isWorld)
        {
            position = GetCanvasPosition(position);
        }
        if(quantity == 1)
            force = 0;

        if (quantity > entries.Length) quantity = entries.Length;
        for (int i =1; i <= quantity; i++)
        {
            finalFeedbackEntry = entries.FirstOrDefault( x=> !x.gameObject.activeSelf);
            finalFeedbackEntry.StartFeedback(position,finalLocation.position, force,isWorld);
            if (i < quantity)
            {
                yield return  new WaitForSeconds(0.02f);
            }
            else
            {
                finalFeedbackEntry.onCoinAnimationEnded.Register(EnableCoin);
            }
        }

    }

    void EnableCoin()
    {
        CoinPlus.Instance.Unwrap().CoinFeedback(thisQuantity);
        CoinManager.Instance.Unwrap().enabled = true;
        finalFeedbackEntry.onCoinAnimationEnded.UnregisterAll();
    }
    void AnimateCoinFromPosition(int quantity, Vector3 position)
    {
        entries.FirstOrDefault( x=> !x.gameObject.activeSelf).StartFeedback(position,finalLocation.position, 0,true);
    }

    [Button]
    void AnimateFromUiPosition()
    {
        entries.FirstOrDefault( x=> !x.gameObject.activeSelf).StartFeedback(Vector3.zero,finalLocation.position, randomForce,false);
    }
    [Button]
    void AnimateFromSnakePositionPosition()
    {
        entries.FirstOrDefault( x=> !x.gameObject.activeSelf).StartFeedback(SnakeMain.Instance.Unwrap().transform.position,finalLocation.position, 0,true);
    }

}
