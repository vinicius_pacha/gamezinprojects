using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class NewItemPopup : UiPopup {

    public static UiPopup instance;
    public Image itemSprite;
    public float timeToShake ;
    public virtual void Awake()
    {
        instance = this;
    }

    public override void Show()
    {
        base.Show();
        StartCoroutine(Shake());
    }

    IEnumerator Shake()
    {
        yield return new WaitForSeconds(timeToShake);
        CameraShakeUI.Instance.Unwrap().SetBigShake();
    }

    public void Setup(int customizationIndex)
    {
        CustomizationData data = CustomizationManager.Instance.Unwrap().customizationData[customizationIndex];
        itemSprite.sprite = data.customizationObjectConfig.customizationSprite;
    }
}