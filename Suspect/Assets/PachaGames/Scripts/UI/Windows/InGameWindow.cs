using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InGameWindow : UiWindow {
    public static UiWindow instance;
    public Image levelProgress;
    public TMP_Text level;
    public GameObject finalLevelProgressParent;
    public Image finalLevelProgress;
    public float currentFinalTime;
    public Jun_TweenRuntime speedUp;
    public Jun_TweenRuntime down;
    public Jun_TweenRuntime speedDown;
    private float totalTime;
    public virtual void Awake()
    {
        instance = this;
    }

    public void Down()
    {
        down.Play();
    }
    public override void Update()
    {
        base.Update();
        levelProgress.fillAmount = LevelCreator.Instance.Unwrap().GetLevelProgress();

        currentFinalTime -= Time.deltaTime;
        
        finalLevelProgressParent.gameObject.SetActive(currentFinalTime > 0);
        finalLevelProgress.fillAmount = currentFinalTime / totalTime;
    }

    public void SetupFinalLevelProgress(float seconds)
    {
        currentFinalTime = seconds;
        totalTime = seconds;
    }

    public override void Show()
    {
        base.Show();
        level.text = string.Format("Level {0}", LevelManager.instance.level);
    }

    public void SpeedUp()
    {
        speedUp.Play();
    }
    public void SpeedDown()
    {
        speedDown.Play();
    }
}
