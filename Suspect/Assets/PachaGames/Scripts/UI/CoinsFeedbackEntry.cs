using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;
using Random = UnityEngine.Random;

public class CoinsFeedbackEntry : MonoBehaviour
{
    public Vector3 toPosition;
    public Vector3 fromPosition;
    public SafeAction onCoinAnimationEnded = new SafeAction();
    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void StartFeedback(Vector3 from, Vector3 to, float randomForce, bool isLocal=false)
    {
        if (isLocal)
        {
            transform.localPosition = from;
        }
        else
        {
            transform.position = from;
        }
        fromPosition = transform.position;
        toPosition = to;
        gameObject.SetActive(true);
        StartCoroutine(FeedbackCoroutine(randomForce));
    }

    IEnumerator FeedbackCoroutine(float speed)
    {
        var thisSpeed = Random.Range(speed*0.5f,speed*1.5f);
        var randomDirection = Random.insideUnitCircle;
        while (thisSpeed*Time.deltaTime > 5f)
        {
            transform.position += Time.deltaTime * thisSpeed * new Vector3(randomDirection.x, randomDirection.y);
            yield return null;
            thisSpeed *= CoinsFeedback.Instance.Unwrap().breakSpeed;
        }

        fromPosition = transform.position;
        float t = 0;
        while(t < 1)
        {
            t += Time.deltaTime * 1.5f;
            
            transform.position = Vector3.Lerp(fromPosition, toPosition, t);
            yield return null;
        }
        onCoinAnimationEnded.Call();
        CoinManager.Instance.Unwrap().enabled = true;
        gameObject.SetActive(false);
    }

}
