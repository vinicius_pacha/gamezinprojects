﻿Shader "Custom/Mobile/DiffuseX RimLight Unlit"
{
    Properties
    {
        [HDR]_Color("Color",COLOR)=(0.5,0.5,0.5,1.0)
        _MainTex ("Base (RGB)", 2D) = "white" {}
       [HDR] _RimColor ("Rim Color", Color) = (0.6, 0.6, 0.6, 1.0)
        _RimPower ("Rim Power", Range(0.5,8.0)) = 1.0

    }
 
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 150
        CGPROGRAM
        #pragma surface surf Lambert noforwardadd
 
        sampler2D _MainTex;
        fixed4 _Color;
		float4 _RimColor;
        float _RimPower;

        struct Input
        {
            float2 uv_MainTex;
			float3 viewDir;
        };
 
        void surf (Input IN, inout SurfaceOutput o)
        {
            half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));

            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            o.Emission = c.rgb + _RimColor.rgb * pow(rim, _RimPower);
            o.Alpha = c.a;
        }
        ENDCG
    }
    Fallback "Mobile/VertexLit"
}