using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelProgressWidget : MonoBehaviour
{
    public Color negativeColor = Color.black;
    public Color positiveColor = Color.yellow;

    public Image currentLevel;
    public Image nextLevel;
    public Image progressBar;
    public Image progressBarBackground;
    public TMP_Text currentLevelNumber;
    public TMP_Text nextLevelNumber;
    [Range(0,1.0f)]
    public float progress;

    public void Setup(int currentLevel)
    {
        currentLevelNumber.text = currentLevel.ToString();
        nextLevelNumber.text = (currentLevel+1).ToString();
        Setup();
    }
    [BitStrap.Button]
    void Setup()
    {
        progressBarBackground.color = negativeColor;
        progressBar.color = positiveColor;
        currentLevel.color = positiveColor;
        nextLevel.color = negativeColor;
        progress = 0;
    }
    void Update()
    {
        currentLevelNumber.text = LevelManager.instance.level.ToString();
        nextLevelNumber.text = (LevelManager.instance.level+1).ToString();
        progressBar.fillAmount = progress;
    
        if (progress >= 0.99f)
        {
            nextLevel.color = positiveColor;
        }
        else
        {
            nextLevel.color = negativeColor;
        }
    }
}
