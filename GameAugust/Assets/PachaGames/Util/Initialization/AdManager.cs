using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdManager : BitStrap.Singleton<AdManager>
{
    public float adTime = 60;
    public float loadingRewardedVideo = 3;
    public bool noVideosOnEditor;
    float currentAdTime = 60;
    bool canShowAd;
    
    void Start()
    {
        currentAdTime = adTime;

    }

    public void InitializeRewardedAds()
    {

        
    }

    public bool HasRewardedVideo
    {
        get
        {
            if (Application.isEditor)
            {
                return loadingRewardedVideo <= 0;
            }
            else
            {
                return false;
            }
        }
    }

    private void LoadRewardedAd()
    {
    }

    private void OnRewardedAdLoadedEvent(string adUnitId)
    {
        // Rewarded ad is ready to be shown. MaxSdk.IsRewardedAdReady(rewardedAdUnitId) will now return 'true'
    }

    private void OnRewardedAdFailedEvent(string adUnitId, int errorCode)
    {
        // Rewarded ad failed to load. We recommend re-trying in 3 seconds.
        Invoke("LoadRewardedAd", 3);
    }

    private void OnRewardedAdFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        // Rewarded ad failed to display. We recommend loading the next ad
        LoadRewardedAd();
    }

    private void OnRewardedAdDisplayedEvent(string adUnitId) { }

    private void OnRewardedAdClickedEvent(string adUnitId) { }

    private void OnRewardedAdDismissedEvent(string adUnitId)
    {
        // Rewarded ad is hidden. Pre-load the next ad
        LoadRewardedAd();
    }

    public void GetReward()
    {
        loadingRewardedVideo = 3;
        RewardAction.Call();
        RewardAction.UnregisterAll();
    }
    
    public void Fail()
    {
        RewardAction.UnregisterAll();
    }
    
    public BitStrap.SafeAction RewardAction = new BitStrap.SafeAction();
    
    public void ShowRewardedVideo(System.Action a)
    {
        Debug.Log("ShowRewardedVideo");
        if(Application.isEditor)
        {
            Debug.Log("TestingPopup with function");
            Debug.Log(a.ToString());

            RewardAction.Register(a);
            PopupManager.instance.AddPopupToQueue(RewardedVideoPopup.instance);
        }
        
    }

    public void InitializeBannerAds()
    {
        
    }

    public void InitializeInterstitialAds()
    {
      
        LoadInterstitial();
    }

    private void LoadInterstitial()
    {
        canShowAd = false;
        currentAdTime = adTime;
    }

    private void OnInterstitialLoadedEvent(string adUnitId)
    {
        // Interstitial ad is ready to be shown. MaxSdk.IsInterstitialReady(interstitialAdUnitId) will now return 'true'
    }

    private void OnInterstitialFailedEvent(string adUnitId, int errorCode)
    {
        // Interstitial ad failed to load. We recommend re-trying in 3 seconds.
        Invoke("LoadInterstitial", 3);
    }

    private void InterstitialFailedToDisplayEvent(string adUnitId, int errorCode)
    {
        // Interstitial ad failed to display. We recommend loading the next ad
        LoadInterstitial();
    }

    private void OnInterstitialDismissedEvent(string adUnitId)
    {
        // Interstitial ad is hidden. Pre-load the next ad
        LoadInterstitial();
    }


    // Update is called once per frame
    void Update()
    {
        loadingRewardedVideo -= Time.deltaTime;
        currentAdTime = 1;//Time.deltaTime;
        if (currentAdTime <= 0)
        {
            canShowAd = true;
        }
    }

    public void TryShowInterstitial()
    {
       
    }
}
