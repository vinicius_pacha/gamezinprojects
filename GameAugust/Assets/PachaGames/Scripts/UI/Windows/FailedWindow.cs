using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FailedWindow : UiWindow {

    public static UiWindow instance;

    public virtual void Awake()
    {
        instance = this;
    }

}
