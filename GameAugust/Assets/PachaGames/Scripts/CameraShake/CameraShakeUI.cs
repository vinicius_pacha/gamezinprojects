using UnityEngine;
using System.Collections;

public class CameraShakeUI : BitStrap.Singleton<CameraShakeUI>
{
    // Transform of the camera to shake. Grabs the gameObject's transform
    // if null.
    public Transform camTransform;

    // How long the object should shake for.
    private float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    public float bigShakeAmount = 0.7f;
    public float bigShakeDuration = 0f;
    public float smallShakeAmount = 0.7f;
    public float smallShakeDuration = 0f;
    
    private float shakeAmount = 0.7f;
    private float decreaseFactor = 1.0f;
    
    Vector3 originalPos;

    void Awake()
    {
        if (camTransform == null)
        {
            camTransform = GetComponent(typeof(Transform)) as Transform;
        }
    }

    void OnEnable()
    {
        originalPos = camTransform.localPosition;
    }

    void Update()
    {
        if (shakeDuration > 0)
        {
            camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
        }
        else
        {
            shakeDuration = 0f;
            camTransform.localPosition = originalPos;
        }
    }
    [BitStrap.Button]
    public void SetBigShake()
    {
        shakeDuration = bigShakeDuration;
        
        shakeAmount = bigShakeAmount;
        decreaseFactor = bigShakeAmount/5.0f;
    }     
    [BitStrap.Button]

    public void SetSmallShake()
    {
        shakeDuration = smallShakeDuration;
        shakeAmount = smallShakeAmount;
        decreaseFactor = smallShakeAmount/5.0f;
    }
}
