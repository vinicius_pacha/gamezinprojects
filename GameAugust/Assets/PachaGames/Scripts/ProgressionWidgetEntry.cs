using UnityEngine;
using UnityEngine.UI;

public class ProgressionWidgetEntry : MonoBehaviour
{
    public Image sprite;
    public ProgressionWidget.ProgressionState state;
}