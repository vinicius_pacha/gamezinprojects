﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelRoom : MonoBehaviour
{
    public Transform initial;
    public Transform final;
    public List<GameObject> enemiesOrSquads;
    public Bridge bridge;

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(0.1f);
        SetupSquads();
        bridge = GetComponentInChildren<Bridge>();
    }

    public void SetupSquads()
    {
        var childrenSquads = GetComponentsInChildren<Squad>();
        foreach (var s in childrenSquads)
        {
            enemiesOrSquads.Add(s.gameObject);
        }
    }

    public void Clear()
    {
        if(bridge!=null)
            bridge.Clear();
        foreach (var io in enemiesOrSquads)
        {
            if (Application.isPlaying)
            {
                if(io!=null)
                    Destroy(io.gameObject);
            }
            else
            {
                if(io!=null)
                    DestroyImmediate(io.gameObject);
            }
        }
        enemiesOrSquads.Clear();
    }
    
    
}
