﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public class Zoning : MonoBehaviour
{
    public Camera cam;
    public Color[] colors;
    public Renderer fogMat;// Start is called before the first frame update
    void Start()
    {
        MenuWindow.instance.onShow.Register(CheckZone);
;    }

    [Button]
    void CheckZone()
    {
        int level = (LevelManager.instance.level-1) / 5;
        SetZone(level);
    }

    void SetZone(int level)
    {
        RenderSettings.fogColor = colors[level%colors.Length];
        fogMat.sharedMaterial.SetColor("_Color", colors[level%colors.Length]);
        cam.backgroundColor = colors[level%colors.Length];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
