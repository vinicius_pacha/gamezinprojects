﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    public Transform spawn;
    public Transform finishLine;
    public GameObject levelBase;
    public void HideSpawnAndFinishLineTransforms()
    {
        spawn.gameObject.SetActive(false);
        finishLine.gameObject.SetActive(false);
        levelBase.SetActive(false);
    }

    private void Awake()
    {
        spawn.gameObject.SetActive(false);
        if(PachaGamesManager.instance != null)
            HideSpawnAndFinishLineTransforms();
    }
}
