﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;

#if UNITY_EDITOR
    using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
#if UNITY_EDITOR
    public SceneAsset[] sceneAssets;
#endif
    public List<string> sceneAssetStrings = new List<string>();
    public Level level;
    public bool gameStarted = false;
    private Scene sc;

    public int currentLevel = 0;
    // Start is called before the first frame update
    void Start()
    {
        sc = new Scene();
        MenuWindow.instance.onShow.Register(LoadLevel);
        InGameWindow.instance.onShow.Register(SetGameStarted);
        //LoadLevel();
    }
#if UNITY_EDITOR
    [Button]
    void GetSceneStrings()
    {
        sceneAssetStrings.Clear();
        foreach (var s in sceneAssets)
        {
            sceneAssetStrings.Add(s.name);
        }
    }
#endif
  
    
    // Update is called once per frame
    void Update()
    {
        
    }

    public int LevelIndex
    {
        get
        {
            Debug.Log(LevelManager.instance.level % sceneAssetStrings.Count);
            return LevelManager.instance.level % sceneAssetStrings.Count;
        } 
    }
    private void SetGameStarted()
    {
        gameStarted = true;
    }
    
    [Button]
    public void LoadLevel()
    {
        //if (currentLevel == LevelIndex && !gameStarted) return;
        Debug.Log("Etapa 1");
        if (sc.name != null)
            SceneManager.UnloadSceneAsync(sc);
        currentLevel = LevelIndex;
        Debug.Log("Etapa 2");

        StartCoroutine(LoadLevelCoroutine());
        gameStarted = false;
    }

    IEnumerator LoadLevelCoroutine()
    {
        var parameters = new LoadSceneParameters(LoadSceneMode.Additive);
        Debug.Log("Etapa 3");

        sc = SceneManager.LoadScene(sceneAssetStrings[currentLevel], parameters);
        Debug.Log("Etapa 4");

        yield return null;
        Debug.Log("Etapa 5");

        var root = sc.GetRootGameObjects();
        Debug.Log("Etapa 6");

        Debug.Log("root lentgroot" + root.Length);
        
        foreach (var go in root)
        {
            Debug.Log(go.name);
            var l = go.GetComponent<Level>();
            if (l != null)
            {
                Debug.Log("FindLevel");
                level = l;
                break;
            }
        }
        Debug.Log("Etapa 7");
        Debug.Log(level);
        Debug.Log(level.spawn);
        Debug.Log(Player.Instance.Unwrap());
        Player.Instance.Unwrap().transform.position = level.spawn.transform.position;
        Player.Instance.Unwrap().transform.rotation = level.spawn.transform.rotation;
        Debug.Log("Etapa 8");

        FinishLine.Instance.Unwrap().transform.position = level.finishLine.transform.position;
        FinishLine.Instance.Unwrap().transform.rotation = level.finishLine.transform.rotation;
        Debug.Log("Etapa 9");

    }
}
