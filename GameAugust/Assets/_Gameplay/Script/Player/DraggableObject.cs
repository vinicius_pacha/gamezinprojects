﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class DraggableObject : InteractableObject
{
    public NavMeshAgent agent;
    public Transform finalDestination;
    public LineRenderer lr;
    public ParticleSystem endParticle;
    public override void Start()
    {
        base.Start();
        finalDestination.gameObject.GetComponent<Renderer>().enabled = false;
        finalDestination.transform.parent = null;
        Invoke("EnableAgent", 0.5f);
    }

    private void EnableAgent()
    {
        agent.enabled = true;
    }

    public override void Logic()
    {
        base.Logic();
        
        if (!canInteract) return;
        
        if (SquadNeeded() <= 0)
        {
            for(int i =0; i<squad.Count; i++)
            {
                squad[i].squadState = Squad.SquadState.carrying;
            }
            NavMeshPath path = new NavMeshPath();
            agent.CalculatePath(finalDestination.position, path);
        
            lr.positionCount = path.corners.Length;
            int j  = 0;
            foreach (var p in path.corners)
            {
                lr.SetPosition(j,p + Vector3.up*0.5f);
                j++;
            }
            lr.SetPosition(j,finalDestination.position + Vector3.up*0.5f);
            
            agent.SetDestination(finalDestination.transform.position);
            if (Vector3.Distance(transform.position, finalDestination.transform.position) <= 1 && agent.velocity.magnitude <= 0.1f )
            {
                canInteract = false;
                agent.enabled = false;
                UnparentSquads();
            }

            endParticle.transform.position = finalDestination.position + Vector3.up * 0.5f;
            
            if(!endParticle.isPlaying)
                endParticle.Play();
            lr.enabled = true;

        }
        else
        {
            lr.enabled = false;
            endParticle.Stop();
            
            agent.SetDestination(transform.position);
        }

        

    }

    public void DragginObject()
    {
        
    }
}
