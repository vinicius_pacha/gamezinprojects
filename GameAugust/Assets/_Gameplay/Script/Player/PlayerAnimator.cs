﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{

    public BasicWalkerController basicWalkerController;

    public Animator playerAnimator;

    private Vector3[] lastPosition = new Vector3[2];
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var delta = transform.position - lastPosition[0];
        playerAnimator.SetFloat("Velocity",(delta/Time.deltaTime).magnitude);
        lastPosition[0] = lastPosition[1];
        lastPosition[1] = transform.position;
    }
}
