﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public NavMeshAgent agent;
    public float radiusDistance = 5;
    public int life = 1;
    public ParticleSystem ps;
    private Animator animator;
    IEnumerator Start()
    {
        animator = GetComponent<Animator>();
        yield return new WaitForSeconds(0.5f);
        agent.enabled = true;
    }
    private void Update()
    {
        animator.SetFloat("Velocity",agent.velocity.magnitude);
        if (Vector3.Distance(Player.Instance.Unwrap().transform.position, transform.position) < radiusDistance)
        {
            agent.SetDestination(Player.Instance.Unwrap().transform.position);
            
        }
    }

    public void DestroyEnemy()
    {
        if (life <= 0)
        {
            ps.transform.parent = null;
            ps.Play();
            Destroy(this.gameObject);
        }
      
    }
}
