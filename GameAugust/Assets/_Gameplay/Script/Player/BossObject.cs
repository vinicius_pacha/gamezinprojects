﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Runtime.Remoting.Lifetime;
using BitStrap;
using TMPro;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BossObject : InteractableObject
{
    public NavMeshAgent agent;
    public PlayerRagdoll ragdoll;
    public float life = 2;
    public Animator bossAnimator;
    public BossState currentState;
    public float delayToAttack = 1.5f;
    private float currentDelayToAttack = 1.5f;
    public Image lifeImage;
    private float initialLife;
    public int maxMinionsOnBoss;

    public enum BossState
    {
        idle,
        following,
        attacking,
        dead
    }

    private void Awake()
    {
        initialLife = life;
        Invoke(" ActivateBoss",0.5f);

    }

    public void ActivateBoss()
    {
        agent.enabled = true;
        canInteract = true;
    }
    public override void Start()
    {
        base.Start();
        int i = 0;
        while (places.Count > maxMinionsOnBoss && i < 100)
        {
            places.RemoveAt(Random.Range(0,places.Count));
            i++;
        }
    
    }

    void SetLifeImage()
    {
        if (canInteract)
        {
            
            float lifeValue = life / initialLife;
            lifeImage.fillAmount = lifeValue;
        
            lifeImage.transform.parent.gameObject.SetActive(lifeValue < 0.99f);
        }
    }
    IEnumerator KillMinion()
    {
        bossAnimator.SetTrigger("Jump");
        yield return new WaitForSeconds(0.6f);
        var s = squad[Random.Range(0, squad.Count)];
        s.UnparentSquad();
        s.enabled = false;
        s.agent.enabled = false;
        Player.Instance.Unwrap().squad.Remove(s);
        squad.Remove(s);
        s.ragdoll.StartRagdoll((Vector3.up*5  + Random.insideUnitSphere*Random.Range(3,6)));
        yield return new WaitForSeconds(0.2f);
        s.squadRenderer.material = s.deadMaterial;

    }
    [Button]
    void KillMinionTest()
    {
        StartCoroutine(KillMinion());
    }
    void DamageLogic()
    {
        if (squad.Count > 0)
        {
            float count = squad.Count;
            float length = places.Count;
            var damageVelocity = count / length;
            life -= Time.deltaTime * damageVelocity;
            
            for(int i =0; i<squad.Count; i++)
            {
                squad[i].squadState = Squad.SquadState.attacking;
            }
            
            if (life < 0 && canInteract)
            {
               
                Death();
            }
        }
    }
    public override void Logic()
    {
        base.Logic();
        bossAnimator.SetFloat("Velocity",agent.velocity.magnitude);
        DamageLogic();
        SetLifeImage();
        switch (currentState)
        {
            case BossState.idle:
                currentDelayToAttack = 2;
                if (Vector3.Distance(transform.position, Player.Instance.Unwrap().transform.position) < 7)
                {
                    currentState = BossState.following;
                }
                break;
            case BossState.following:
                agent.SetDestination(Player.Instance.Unwrap().transform.position);

                if (squad.Count > 0)
                {
                    currentState = BossState.attacking;
                } 
                break;
            case BossState.attacking:
                agent.SetDestination(transform.position);
                currentDelayToAttack -= Time.deltaTime;

                if (currentDelayToAttack < 0)
                {
                    currentDelayToAttack = delayToAttack;
                    StartCoroutine(KillMinion());
                }
                
                if (squad.Count <= 0)
                {
                    currentState = BossState.following;
                }
                break;
            case BossState.dead:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }


    IEnumerator Complete()
    {
        FinishLine.Instance.Unwrap().CompleteLevel();
        yield return new WaitForSeconds(0.5f);
        if(PachaGamesManager.instance != null)
            PachaGamesManager.instance.Victory();
    }
    public void Death()
    {
        Player.Instance.Unwrap().KillBoss();
        StartCoroutine(Complete());
        textValue.gameObject.SetActive(false);
        agent.enabled = false;
        UnparentSquads();
        canInteract = false;
        currentState = BossState.dead;
        lifeImage.transform.parent.gameObject.SetActive(false);
        ragdoll.StartRagdoll();
    }
}
