﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Squad : MonoBehaviour
{
    public float distance;
    public Renderer squadRenderer;
    public ParticleSystem ps;
    public ParticleSystem deathEffect;
    public float activationTime;
    public NavMeshAgent agent;
    public SquadState squadState;
    public AnimationCurve yCurve;
    public TrailRenderer trail;
    public float yValue;
    public GameObject brick;
    private Animator animator;
    public PlayerRagdoll ragdoll;
    public Material deadMaterial;
    public float delayToActivate;
    public enum SquadState
    {
        idle,
        activating,
        activated,
        jumping,
        carryingBrick,
        carrying,
        attacking
    }
    public bool activated = false;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        animator = GetComponent<Animator>();
        trail.enabled = false;
        Player.Instance.Unwrap().squadCounter++;
        agent.avoidancePriority = Player.Instance.Unwrap().squadCounter;
        ragdoll = GetComponent<PlayerRagdoll>();
        yield return new WaitForSeconds(0.2f);
        agent.enabled = true;
    }

    public void SaveSquadFromPrison()
    {
        enabled = true;
        transform.parent = null;
        delayToActivate = 1.5f;
        
    }
    // Update is called once per frame
    void Update()
    {
        delayToActivate -= Time.deltaTime;
        if (delayToActivate > 0) return;
        if (Vector3.Distance(transform.position, Player.Instance.Unwrap().transform.position) < distance && !activated)
        {
            Activate();
        }
        
        animator.SetFloat("Velocity",agent.velocity.magnitude);
        animator.SetBool("Attacking",squadState == SquadState.attacking);
        animator.SetBool("Carrying",squadState == SquadState.carrying);
        animator.SetBool("Dancing",Player.Instance.Unwrap().currentPlayerState == Player.PlayerState.VICTORY);

        brick.SetActive(squadState == SquadState.carryingBrick);
        
        switch (squadState)
        {
            case SquadState.idle:
                agent.SetDestination(transform.position + Random.insideUnitSphere);
                break;
            case SquadState.activating:
                agent.SetDestination(transform.position);
                break; 
            case SquadState.jumping:
                break;
            case SquadState.activated:
                agent.SetDestination(Player.Instance.Unwrap().transform.position);
                break;
            case SquadState.carrying:
                break;  
            case SquadState.carryingBrick:
                agent.SetDestination(Player.Instance.Unwrap().transform.position);
                break;
            case SquadState.attacking:
                break;
                
            
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    void Activate()
    {
        activated = true;
        squadState = SquadState.activating;
        squadRenderer.material = Player.Instance.Unwrap().playerMaterial;

        StartCoroutine(ActivateCoroutine());
    }

    IEnumerator ActivateCoroutine()
    {
        animator.SetTrigger("Rescued");
        ps.Play();
        yield return new WaitForSeconds(activationTime);
        Player.Instance.Unwrap().squad.Add(this);
        squadState = SquadState.activated;
        
    }

    public void ParentSquad(Transform t,Transform centerOfObject)
    {
        squadRenderer.material = Player.Instance.Unwrap().playerUsedMaterial;

        StartCoroutine(ParentSquadCoroutine(t,centerOfObject));
    }

    public void UnparentSquad()
    {
        transform.parent = null;
        squadState = Squad.SquadState.activated;
        trail.enabled = false;
        agent.enabled = true;
        squadRenderer.material = Player.Instance.Unwrap().playerMaterial;
        StopAllCoroutines();

    }
    IEnumerator ParentSquadCoroutine(Transform finalTransform, Transform centerOfObject)
    {
        agent.SetDestination(this.transform.position);
        agent.enabled = false;
        trail.Clear();
        trail.enabled = true;
        float t = 0;
        Vector3 initialPosition = transform.position;
        animator.SetTrigger("Flip");
        while (t < 1)
        {
            t+=Time.deltaTime*2;
            transform.position = Vector3.Lerp(initialPosition,finalTransform.transform.position,t);
            var tempPosition = transform.position;
            tempPosition.y = tempPosition.y + yCurve.Evaluate(t) * yValue;
            transform.position = tempPosition;
            yield return null;
        }

        var position = centerOfObject.position;
        position.y = transform.position.y;
        transform.LookAt(position); 
        transform.parent = finalTransform;

    }

    public void GetBrick(Transform finalTransform)
    {
        StartCoroutine(GetBrickCoroutine(finalTransform));
    }
    IEnumerator GetBrickCoroutine(Transform finalTransform)
    {
        agent.SetDestination(this.transform.position);
        agent.enabled = false;
        trail.Clear();
        trail.enabled = true;
        float t = 0;
        Vector3 initialPosition = transform.position;
        animator.SetTrigger("Flip");
        while (t < 1)
        {
            t+=Time.deltaTime*2;
            transform.position = Vector3.Lerp(initialPosition,finalTransform.transform.position,t);
            var tempPosition = transform.position;
            tempPosition.y = tempPosition.y + yCurve.Evaluate(t) * yValue;
            transform.position = tempPosition;
            yield return null;
        }
        trail.Clear();
        trail.enabled = false;
        yield return new WaitForSeconds(0.1f);
        finalTransform.gameObject.SetActive(false);
        agent.enabled = true;
        squadState = SquadState.carryingBrick;
    }
    public void ThrowSquadOnEnemy(Enemy enemyTransform)
    {
        StartCoroutine(ThrowSquadOnEnemyCoroutine(enemyTransform));
    }
    IEnumerator ThrowSquadOnEnemyCoroutine(Enemy enemyTransform)
    {
        animator.speed = 2;
        agent.SetDestination(this.transform.position);
        agent.enabled = false;
        trail.Clear();
        trail.enabled = true;
        float t = 0;
        Vector3 initialPosition = transform.position;
        animator.SetTrigger("Flip");
        while (t < 1)
        {
            t+=Time.deltaTime*3;
            transform.position = Vector3.Lerp(initialPosition,enemyTransform.transform.position,t);
            var tempPosition = transform.position;
            tempPosition.y = tempPosition.y + yCurve.Evaluate(t) * yValue;
            transform.position = tempPosition;
            yield return null;
        }
        trail.Clear();
        trail.enabled = false;
        DestroySquad();
        enemyTransform.DestroyEnemy();
    }
    public void DestroySquad()
    {
        Player.Instance.Unwrap().squad.Remove(this);
        deathEffect.transform.parent = null;
        deathEffect.Play();
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        Player.Instance.Unwrap().squad.Remove(this);
    }
}
