﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using TMPro;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    public bool canInteract = true;
    public Transform placesParent;
    
    public List<Transform> places = new List<Transform>();
    public TMP_Text textValue;
    public List<Squad> squad = new List<Squad>();
    public Collider interactableCollider;
    public TextMeshPro requiredMinionsText;
    public int requiredMinions=5;
    public int maxMinions =5;
    public virtual void Start()
    {
        if (requiredMinionsText != null)
        {
            requiredMinionsText.text = requiredMinions.ToString();
        }
        
        if (placesParent != null)
        {
            places = placesParent.gameObject.GetComponentsInChildren<Transform>().ToList();
            places.Remove(placesParent.transform);
            if (places.Count == 0)
            {
                var v = -Vector3.forward;
                for (int i = 0; i < maxMinions; i++)
                {
                    var t = new GameObject("place_"+i);
                    t.transform.parent = placesParent.transform;
            
                    t.transform.localPosition = v.normalized * ((transform.localScale.x/ (2 * transform.localScale.x)) + 0.1f);
                    v = Quaternion.Euler(0, 360 / maxMinions, 0)*v;
                    places.Add(t.transform);
                }
            }
        }
    }
    public virtual int SquadNeeded()
    {
        return maxMinions - squad.Count;
    }
    
    public void UnparentSquads()
    {
        for(int i =0; i<squad.Count; i++)
        {
            squad[i].UnparentSquad();
        }
            
        squad.Clear();
    }

    [Button]
    public void Reset()
    {
        textValue = GetComponentInChildren<TMP_Text>();
        var c = gameObject.GetComponents<Collider>();
        placesParent = transform.Find("Places");
        interactableCollider = Enumerable.First(c, x => x.isTrigger);
    }

    public void Update()
    {
        if (squad.Count <= 0)
        {
            textValue.text = "";
           
        }
        else
        {
            textValue.text = string.Format("<color=#ff6666><u>{0}</u><size=24>\n{1}", squad.Count, places.Count);
            if (squad.Count == places.Count)
            {
                textValue.text = string.Format("<color=#FFFFaa><u>{0}</u><size=24>\n{1}", squad.Count,places.Count);
            }
            
        }
        
        if (Vector3.Distance(Player.Instance.Unwrap().transform.position, transform.position) > 
            (interactableCollider as SphereCollider).radius*transform.localScale.x + 0.2f)
        {
            UnparentSquads();
        }

        Logic();
    }

    public virtual void Logic()
    {
        
    }
}
