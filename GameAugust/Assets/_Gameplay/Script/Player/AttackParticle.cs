﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackParticle : MonoBehaviour
{
    // Start is called before the first frame update
    public ParticleSystem ps;

    public void PlayHitFx()
    {
        ps.Play();
    }
}
