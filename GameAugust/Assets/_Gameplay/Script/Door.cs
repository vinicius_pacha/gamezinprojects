﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool open = false;
    public Jun_TweenRuntime tweenRuntime;
    private float value = 0;
    public bool wasBool = false;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (open)
        {
            value += Time.deltaTime*3;
        }
        else
        {
            value -= Time.deltaTime*3;
        }
        
        value = Mathf.Clamp01(value);
        tweenRuntime.previewValue = value;

    }

    public void Open()
    {
        tweenRuntime.Play();

    }

    public void Close()
    {
        tweenRuntime.Play();
    }
}
