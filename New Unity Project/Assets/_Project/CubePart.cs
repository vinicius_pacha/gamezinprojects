﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubePart : MonoBehaviour
{
   public bool available;
   private void OnTriggerEnter(Collider other)
   {
      if (available)
      {
         if (other.CompareTag("Obstacle"))
         {
            available = false;
            ShiftCube.Instance.Unwrap().DecreaseArea(this);
         }
      }
         
   }
   
}
