﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float speed = 5;
    private Vector3 initialPosition;
    private float initialXPosition;
    private void Start()
    {
        initialPosition = transform.position;
    }

    private void FixedUpdate()
    {
        var desiredPosition = initialPosition + Vector3.right * target.transform.position.x + Vector3.up * target.transform.position.y;
        transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * speed);
    }
}
