﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShiftCube : BitStrap.Singleton<ShiftCube>
{
    public float maxSize = 1;
    public float minSize = 1;
    public float speed = 1;
    public Rigidbody rbd;
    public float currentSize = 1;
    public float maxArea = 1;
    public LayerMask mask;
    public float delay;
    public bool shouldResize;
    public float velocitySpeed;
    public CubePart[] cubes;
    public int partsToRemove = 0;
    public void DecreaseArea(CubePart cubePart)
    {
        partsToRemove++;
        delay = 0.2f;

        shouldResize = true;
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            var tempSize = currentSize + Time.deltaTime * speed;
        
            currentSize += Time.deltaTime*speed;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            currentSize -= Time.deltaTime*speed;
        }

        currentSize = Mathf.Clamp(currentSize, minSize, maxSize);
        SetCube();
        delay -= Time.deltaTime;
        if (shouldResize && delay < 0)
        {
            shouldResize = false;
            Resize();
        }

        transform.position += Vector3.right * velocitySpeed * Time.deltaTime;

    }

    void Resize()
    {
        var areaToRemove = partsToRemove * 0.25f * 0.25f;
        
        maxArea -= areaToRemove;
        partsToRemove = 0;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        float xScale = currentSize;
        float yScale = maxArea / xScale;
        transform.localScale = new Vector3(xScale,yScale,1);
    }

    void SetCube()
    {
        float xScale = currentSize;
        float yScale = maxArea / xScale;


        int cubeIndex = 0;
        for (int i = 0; i < xScale*4; i++)
        {
            for (int j = 0; j < yScale*4; j++)
            {
                cubes[cubeIndex].gameObject.SetActive(true);

                cubes[cubeIndex].transform.position = transform.position + new Vector3(0,j*0.25f - yScale*0.5f,i*0.25f - xScale*0.5f);
                cubeIndex++;
            }
        }

        for(int i = cubeIndex; i < cubes.Length; i++)
        {
            cubes[cubeIndex].gameObject.SetActive(false);
        }
        
    }
    
}

