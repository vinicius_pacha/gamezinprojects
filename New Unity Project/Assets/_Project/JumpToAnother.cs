﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpToAnother : MonoBehaviour
{
    public float initialZ;
    public float finalZ;
    public float initialX;
    public float finalX;
    public float finalY;
    public float initialY;
    public Animator animator;
    public AnimationCurve jumpAnimationCurve;
    public float animationSpeed;
    public ParticleSystem changeFx;
    public ParticleSystem spinFx;
    public ParticleSystem runFx;
    public float distance;
    public float speed;
    public bool isGrounded;
    
    public Renderer playerRenderer;
    public Material blueMat;
    public Material redMat;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isGrounded && !runFx.isPlaying)
        {
            runFx.Play();
        }
        
        if(!isGrounded)
        {
            runFx.Stop();    
        }

   
        var pos = transform.parent.position;
        pos.x += Time.deltaTime * speed;
        transform.parent.position = pos;
        
    }
    [BitStrap.Button]

    void Jump()
    {
        animator.SetTrigger("JumpPlatform");
        animator.SetTrigger("Spin");
        //changeFx.Play();
        StartCoroutine(JumpCoroutine());
    }
    IEnumerator JumpCoroutine()
    {
        var t = 0.0f;
        Vector3 position = transform.localPosition;
        while (t < 0.5f)
        {
            t += Time.deltaTime / animationSpeed;
            position.z = Mathf.Lerp(initialZ, finalZ, t);
            position.y = Mathf.Lerp(initialY, finalY, jumpAnimationCurve.Evaluate(t));
            position.x = Mathf.Lerp(initialX, finalX, t);
            yield return null;

            transform.localPosition = position;
        }

        Spin(redMat);
        while (t < 1)
        {
            t += Time.deltaTime / animationSpeed;
            position.z = Mathf.Lerp(initialZ, finalZ, t);
            position.y = Mathf.Lerp(initialY, finalY, jumpAnimationCurve.Evaluate(t));
            position.x = Mathf.Lerp(initialX, finalX, t);
            yield return null;

            transform.localPosition = position;
        }
        yield return null;
    }
    [BitStrap.Button]
    void JumpBack()
    {
        animator.SetTrigger("JumpPlatform");
        animator.SetTrigger("Spin");
        //changeFx.Play();
        StartCoroutine(JumpBackCoroutine());
    }

    void Spin(Material mat)
    {
        playerRenderer.material = mat;
        var main = changeFx.main;
        var c = mat.GetColor("_TopColor");
        c.a = 1;
        main.startColor = c;
        main = spinFx.main;
        main.startColor = c;
        changeFx.Play();
        spinFx.Play();
        changeFx.Play();
        var run = runFx.main;
        run.startColor = c;
    }
    IEnumerator JumpBackCoroutine()
    {
        var t = 0.0f;
        Vector3 position = transform.localPosition;
        while (t < 0.5f)
        {
            t += Time.deltaTime / animationSpeed;
            position.z = Mathf.Lerp(finalZ, initialZ, t);
            position.y = Mathf.Lerp(initialY, finalY, jumpAnimationCurve.Evaluate(t));
            position.x = Mathf.Lerp(finalX, initialX, t);
            yield return null;
            transform.localPosition = position;
        }
        Spin(blueMat);

        while (t < 1f)
        {
            t += Time.deltaTime / animationSpeed;
            position.z = Mathf.Lerp(finalZ, initialZ, t);
            position.y = Mathf.Lerp(initialY, finalY, jumpAnimationCurve.Evaluate(t));
            position.x = Mathf.Lerp(finalX, initialX, t);
            yield return null;
            transform.localPosition = position;
        }
        yield return null;
    }

    public void FixedUpdate()
    {
        if (Physics.Raycast(transform.position, Vector3.down, distance))
        {
            isGrounded = true;
            
        }
        else
        {
            isGrounded = false;
            
        }
    }
    

}
