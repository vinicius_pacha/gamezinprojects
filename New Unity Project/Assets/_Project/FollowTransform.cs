﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransform : MonoBehaviour
{
    public Animator speed; 
    public Transform target;
    public Vector3 offset;
    public float scaleFactor;

    public  BallSpeed ball;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = target.position + offset * target.localScale.x;
        transform.localScale = target.localScale * scaleFactor;
        float s = ball.speed;
        s = Mathf.Clamp(s, 1, 15
        );
        speed.SetFloat("SPEED", s);
        speed.SetBool("Ground", ball.isGrounded);
        
    }
}
