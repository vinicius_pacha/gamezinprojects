﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizeBall : MonoBehaviour
{
    public float maxSize = 1;
    public float minSize = 1;
    public float speed = 1;
    public Rigidbody rbd;
    public float currentSize = 1;
    public LayerMask mask;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            var tempSize = currentSize + Time.deltaTime * speed;
            
            currentSize += Time.deltaTime*speed;
            
            
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            currentSize -= Time.deltaTime*speed;
            
        }

        currentSize = Mathf.Clamp(currentSize, minSize, maxSize);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var t = rbd.velocity;
        transform.localScale = Vector3.one * currentSize;
        //rbd.mass = currentSize;
        rbd.velocity = t;
    }
}
