﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BallSpeed : MonoBehaviour
{
    public Rigidbody rbd;
    public float maxVelocity;
    public float acceleration;
    public float speed;
    public bool isGrounded;
    public TMP_Text text;
    

    public float groundDistance;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down,out hit, transform.localScale.y + groundDistance))
        {
            Vector3 dir = new Vector3(hit.normal.y,-hit.normal.x,hit.normal.z);
            isGrounded = true;
            if (rbd.velocity.x < maxVelocity)
            {
                rbd.AddForce(acceleration * dir.normalized,ForceMode.Acceleration);
            }
           
        }
        else
        {
            isGrounded = false;
        }

        speed = rbd.velocity.x;
        text.text = ((int)(rbd.velocity.x)).ToString();
    }
}
