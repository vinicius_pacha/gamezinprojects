using BitStrap;
using MLSpace;
using UnityEngine;

public sealed class RagdollTester : BitStrap.Singleton<RagdollTester>
{
    public RagdollManagerHum ragdoll;

    [Button]
    void StartRagdoll()
    {
        ragdoll.StartRagdoll();
    }
    [Button]
    void ResetRagdoll()
    {
        ragdoll.BlendToMecanim();
        
    }
    
}
