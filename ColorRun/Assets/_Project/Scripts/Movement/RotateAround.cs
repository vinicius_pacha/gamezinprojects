using System;
using UnityEngine;

public sealed class RotateAround : MonoBehaviour
{
    public Vector3 axis;
    public float rotateSpeed;

    private void Update()
    {
        transform.Rotate(axis,rotateSpeed * Time.deltaTime);
    }
}
