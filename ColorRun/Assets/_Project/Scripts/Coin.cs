using System;
using BitStrap;
using UnityEngine;
using UnityEngine.UIElements;

public sealed class Coin : MonoBehaviour
{
    private void Start()
    {
        transform.parent = null;
        MenuWindow.instance.onShow.Register(DestroyThisGameObject);
    }

    private void OnDestroy()
    {
        MenuWindow.instance.onShow.Unregister(DestroyThisGameObject);
    }

    void DestroyThisGameObject()
    {
        Destroy(gameObject);
    }
    public void GetCoin()
    {
        VibrationManager.Instance.Unwrap().SmallVibration();
        CoinsFeedback.Instance.Unwrap().EarnCoinFromPosition(1,transform.position,true);
        EffectsManager.Instance.Unwrap().PlayEffectOnPosition(EffectsManager.Instance.Unwrap().coinEffect,transform.position);
        Destroy(gameObject);
    }
}
