using System;
using System.Collections.Generic;
using System.Linq;
using MessengerExtensions;
using UnityEngine;

public sealed class EnvironmentManager : BitStrap.Singleton<EnvironmentManager>
{
    public EnvironmentStep prefab;
    public List<EnvironmentStep> environmentGrounds = new List<EnvironmentStep>() ;
    public float distance = 50;
    public float snakeOffset = 50;
    private int currentIndex = 0;
    private int currentDistance = 0;
    private SnakeMain snakePlayer;
    private int currentEnvironment = -1;
    public EnviromentSetup[] setups;
    public ColorConfig[] colorConfigs;
    public ColoredObstaclePrefab coPrefab;
    public void Start()
    {
        currentDistance = 0;
        snakePlayer = SnakeMain.Instance.Unwrap();
        for (int i = 0; i < 4; i++)
        {
            var g = Instantiate(prefab);
            environmentGrounds.Add(g);
        }
        
        foreach (var t in environmentGrounds)
        {
            t.transform.position = new Vector3(0,0,-distance*2 + distance*currentDistance);
            currentDistance++;
        }

        prefab.gameObject.SetActive(false);
        GetCurrentEnvironmentSetup();
        MenuWindow.instance.onShow.Register(ResetEnvironmentBG);
        InGameWindow.instance.onShow.Register(ResetEnvironmentBG);
    }

    public Transform environmentBG;

    public void ResetEnvironmentBG()
    {
        var position = environmentBG.transform.position;
        position.z = snakePlayer.transform.position.z;
        environmentBG.transform.position=position;
        GetCurrentEnvironmentSetup();
    }

    public void GetCurrentEnvironmentSetup()
    {
        var index = ((LevelManager.instance.level-1) / 3) % setups.Length;
        if (currentEnvironment != index)
        {
            currentEnvironment = index;
            Setup(setups[currentEnvironment]);
            foreach (var eg in environmentGrounds)
            {
                eg.ActivatePrefabByIndex(currentEnvironment);
            }
        }
    }
    private void Update()
    {
        if (snakePlayer.transform.position.z >= environmentGrounds[currentIndex].transform.position.z + snakeOffset)
        {
            environmentGrounds[currentIndex].transform.position =  new Vector3(0,0,-distance*2 + distance*currentDistance);
            currentDistance++;
            currentIndex = (currentIndex+1)%environmentGrounds.Count;
        }
    }

    [Header("EnvironmentRefs")]
    public Camera camera;
    public Renderer borderRenderer;
    public Renderer road;
    public SpriteRenderer fogTop;

    
    public void Setup(EnviromentSetup env)
    {
        int index = setups.ToList().IndexOf(env);
        ColorManager.Instance.Unwrap().config = colorConfigs[index];
        foreach (var co in coPrefab.coloredObstacles)
        {
            co.Setup(co.startColor);
        }
        SnakeMain.Instance.Unwrap().snakeEffect.ChangeColor(SnakeMain.Instance.Unwrap().snakeEffect.colorType);
        
        borderRenderer.sharedMaterials[0].SetColor("_LightTint", env.grass);
        borderRenderer.sharedMaterials[0].SetColor("_LeftBottomColor", env.grassBottom);
        borderRenderer.sharedMaterials[0].SetColor("_RightBottomColor", env.grassBottom);
        
            
        borderRenderer.sharedMaterials[1].SetColor("_LightTint", env.wall);
        
        
        road.sharedMaterial.SetColor("_LightTint", env.road);
        fogTop.color = env.fogTop;
        camera.backgroundColor = env.fog;
        RenderSettings.fogColor = env.fog;
    }
}
