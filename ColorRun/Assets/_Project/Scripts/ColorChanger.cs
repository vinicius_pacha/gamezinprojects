using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    public ColoredObstacle co;
    private void OnTriggerEnter(Collider other)
    {
        var se = other.transform.parent.GetComponent<SnakeEffect>();
        if (se)
        {
            se.ChangeColor(co.currentColor);
            StartCoroutine(PlayEffectOnFinishLine(other.transform.position-Vector3.up*0.5f));
        }
    }
    
    IEnumerator PlayEffectOnFinishLine(Vector3 position)
    {
        SnakeEffect snakeEffect = SnakeEffect.Instance.Unwrap();
        for (int i = 0; i < snakeEffect.snakeSize; i++)
        {
            snakeEffect.PlayDeathEffectAtPosition(position+Vector3.up,snakeEffect.colorType);
            if(snakeEffect.snakeSize - i > 3)
                snakeEffect.transform.position -= snakeEffect.transform.forward*Time.deltaTime*snakeEffect.snakeIncreasePerSize*SnakeMain.Instance.Unwrap().snakeControl.forwardSpeed;
            yield return new WaitForSeconds(snakeEffect.snakeIncreasePerSize);
        }
    }
}
