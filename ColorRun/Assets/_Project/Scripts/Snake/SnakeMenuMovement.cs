using System;
using UnityEngine;

public sealed class SnakeMenuMovement : MonoBehaviour
{
    public Animator snakeLocalAnimation;

    private void Update()
    {
        snakeLocalAnimation.SetBool("Menu",PachaGamesManager.instance.currentGameState == PachaGamesManager.GameState.MENU);
        snakeLocalAnimation.SetBool("InGame",PachaGamesManager.instance.currentGameState == PachaGamesManager.GameState.INGAME);
        snakeLocalAnimation.SetBool("Default",PachaGamesManager.instance.currentGameState != PachaGamesManager.GameState.INGAME &&
                                              PachaGamesManager.instance.currentGameState != PachaGamesManager.GameState.MENU);
    }
}