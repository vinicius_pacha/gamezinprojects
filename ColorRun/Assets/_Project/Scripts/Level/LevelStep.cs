using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public sealed class LevelStep : MonoBehaviour
{
    public ColorManager.ColorType currentColor;
    public List<ColoredObstaclePrefab> obstaclesPrefab = new List<ColoredObstaclePrefab>();

    [Button]
    public void ShiftObjstaclesColor()
    {
        foreach (var o in obstaclesPrefab)
        {
            foreach (var co in o.coloredObstacles)
            {
                var desiredColor = (int) co.startColor + (int)currentColor;
                desiredColor = desiredColor % 8;
            
                co.Setup((ColorManager.ColorType) desiredColor);
            }
          
        }
        
    }
}
