using System;
using BitStrap;
using UnityEngine;

public sealed class FinalLineMultiplier : MonoBehaviour
{
    public TweenShader tween;
    public ParticleSystem ps1;
    public ParticleSystem ps2;
    public int multiplierValue;
    public void Animate()
    {
        tween.PlayForward();
        ps1.Play();
        ps2.Play();
        CameraShake.Instance.Unwrap().SetShake();
        VibrationManager.Instance.Unwrap().StrongVibration();
    }

    private void Start()
    {
        InGameWindow.instance.onShow.Register(Reset);
    }

    void Reset()
    {
        tween.Clear();
    }
    
}
