using UnityEngine;

public sealed class EffectsManager : BitStrap.Singleton<EffectsManager>
{
    public ParticleSystem coinEffect;
    public void PlayEffectOnPosition(ParticleSystem effect, Vector3 position)
    {
        effect.transform.position = position;
        effect.Play();
    }
}
