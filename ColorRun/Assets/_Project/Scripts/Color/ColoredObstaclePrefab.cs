﻿using System;
using BitStrap;
using UnityEditor;
using UnityEngine;

public class ColoredObstaclePrefab : MonoBehaviour
{
    public ColoredObstacle[] coloredObstacles;
    public float distance;
    
    [Button]
    public void GetAllColoredObstacles()
    {
        coloredObstacles = GetComponentsInChildren<ColoredObstacle>();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color= Color.magenta;
        Gizmos.DrawLine(transform.position - transform.forward*distance/2 + Vector3.up*0.5f,transform.position + transform.forward*distance/2 + Vector3.up*0.5f);
    }
}