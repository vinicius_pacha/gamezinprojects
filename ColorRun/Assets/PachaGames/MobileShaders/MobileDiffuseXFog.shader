﻿Shader "Custom/Mobile/DiffuseX Height Fog"
{
    Properties
    {
        [HDR]_Color("Color",COLOR)=(0.5,0.5,0.5,1.0)
        [HDR]_ColorIlumin("Ilumin",COLOR)=(0.5,0.5,0.5,1.0)
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _IluminTex ("Self Ilumin (RGB)", 2D) = "white" {}

		_FogColor("Fog Color", Color) = (0.3, 0.4, 0.7, 1.0)
		_FogStart("Fog Start", float) = 0
		_FogEnd("Fog End", float) = 0

    }
 
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 150
        CGPROGRAM
        #pragma surface surf Lambert noforwardadd vertex:myvert

	    struct Input
        {
            float2 uv_MainTex;
			float2 uv_IluminTex;
			half fog;
        };
		
        sampler2D _MainTex;
        sampler2D _IluminTex;
        fixed4 _Color;
        fixed4 _ColorIlumin;

		fixed4 _FogColor;
		half _FogStart;
		half _FogEnd;

		void myvert(inout appdata_full v, out Input data)
		{
			UNITY_INITIALIZE_OUTPUT(Input,data);
			float4 pos = mul(unity_ObjectToWorld, v.vertex).xyzw;
			data.fog = saturate((_FogStart - pos.y) / (_FogStart - _FogEnd));
		}
  
		void surf (Input IN, inout SurfaceOutput o) 
        {
            fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			fixed4 i = tex2D(_IluminTex, IN.uv_IluminTex);

			fixed3 fogColor = _FogColor.rgb;

            o.Albedo = lerp(c.rgb, fogColor, IN.fog);

            o.Alpha = c.a;
			o.Emission = _ColorIlumin.rgb * i;
        }
        ENDCG
    }
    Fallback "Mobile/Diffuse"
}