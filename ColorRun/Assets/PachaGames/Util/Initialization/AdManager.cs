using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;

public class AdManager : BitStrap.Singleton<AdManager>
{
    public float adTime = 60;
    public float loadingRewardedVideo = 3;
    public bool noVideosOnEditor;
    float currentAdTime = 60;
    bool canShowAd;
    public BitStrap.SafeAction RewardAction = new BitStrap.SafeAction();

    void Start()
    {
        
    }


    public bool HasRewardedVideo
    {
        get
        {
            if (Application.isEditor)
            {
                return loadingRewardedVideo <= 0;
            }
            else
            {
                return false;
            }
        }
    }

    public void GetReward()
    {
        loadingRewardedVideo = 3;
        RewardAction.Call();
        RewardAction.UnregisterAll();
    }
    
    public void Fail()
    {
        RewardAction.UnregisterAll();
    }
    
    
    public void ShowRewardedVideo(System.Action a)
    {
        Debug.Log("ShowRewardedVideo");
        if(Application.isEditor)
        {
            Debug.Log("TestingPopup with function");
            Debug.Log(a.ToString());

            RewardAction.Register(a);
            PopupManager.instance.AddPopupToQueue(RewardedVideoPopup.instance);
        }
        else
        {
            //TODO
        }
    }
    


    // Update is called once per frame
    void Update()
    {
        loadingRewardedVideo -= Time.deltaTime;
        currentAdTime -= Time.deltaTime;
        if (currentAdTime <= 0)
        {
            canShowAd = true;
        }
    }

    public void TryShowInterstitial()
    {
       //todo
    }
}
