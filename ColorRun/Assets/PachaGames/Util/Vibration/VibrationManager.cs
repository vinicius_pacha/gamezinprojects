using System.Collections;
using System.Collections.Generic;
using MoreMountains.NiceVibrations;
using UnityEngine;
using UnityEngine.UI;

public class VibrationManager : BitStrap.Singleton<VibrationManager>
{
    
    public bool useVibration = false;

    public float delayBetweenVibration = 0.1f;

    private float currentVibrationTime = 0;
    // Start is called before the first frame update
    void Update()
    {
        currentVibrationTime -= Time.deltaTime;
    }

    // Update is called once per frame
    public void SmallVibration()
    {
        if (currentVibrationTime < 0)
        {
            currentVibrationTime = delayBetweenVibration;
            if(useVibration)
                MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }
    }    
    public void MediumVibration()
    {
        if (currentVibrationTime < 0)
        {
            currentVibrationTime = delayBetweenVibration;
            if(useVibration)
                MMVibrationManager.Haptic(HapticTypes.MediumImpact);
            
        }
    }
    public void StrongVibration()
    {
        currentVibrationTime = delayBetweenVibration;
        if(useVibration)
            MMVibrationManager.Haptic(HapticTypes.Success);
    }

    public void ToggleVibration()
    {
        useVibration = !useVibration;
    }
    

}
