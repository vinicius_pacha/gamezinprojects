﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;

public class CoinManager : BitStrap.Singleton<CoinManager>
{
    public float coins = 0;
    public SafeAction<int> onCoinChange = new SafeAction<int>();
    public int Coins
    {
        get { return Mathf.RoundToInt(coins); }
        
    } 
    public float desiredCoins = 0;
    public float coinIncreaseSpeed = 10;
    private float currentCoinSpeed = 10;
    public bool earnedCoins = false;
    public SafeAction onFinishedIncreasing = new SafeAction();
    const string COINS = "COINS";
    public int levelCoins = 0;
    
    void ResetLevelCoins()
    {
        levelCoins = 0;
    }
    private void Start()
    {
        if (!PlayerPrefs.HasKey(COINS))
        {
            PlayerPrefs.SetInt(COINS, (int)coins);
        }
        else
        {
            coins = PlayerPrefs.GetInt(COINS);
            desiredCoins = coins;
        }
        MenuWindow.instance.onShow.Register(ResetLevelCoins);
    }
    
    
    public void EarnCoins(int coinIncrease, System.Action onFinishedIncrease = null)
    {
        levelCoins += coinIncrease;
        earnedCoins = true;
        onFinishedIncreasing.Register(onFinishedIncrease);
        desiredCoins = desiredCoins + coinIncrease;
        currentCoinSpeed = Mathf.Abs(desiredCoins - coins)*Time.deltaTime*2;
        PlayerPrefs.SetInt(COINS, (int)desiredCoins);
        if (coinIncrease == 1)
            currentCoinSpeed = 1;
        onCoinChange.Call(coinIncrease);
    }

    public void EarnCoinsInstant(int coinIncrease, System.Action onFinishedIncrease = null)
    {
        levelCoins += coinIncrease;
        earnedCoins = true;
        onFinishedIncreasing.Register(onFinishedIncrease);
        desiredCoins = desiredCoins + coinIncrease;
        currentCoinSpeed = 10000;
        PlayerPrefs.SetInt(COINS, (int)desiredCoins);
        onCoinChange.Call(coinIncrease);
    }


    public bool SpendCoins(int coinDecrease)
    {
        if(desiredCoins-coinDecrease>=0)
        {
            desiredCoins -= coinDecrease;
            PlayerPrefs.SetInt(COINS, (int)desiredCoins);
            CoinPlus.Instance.Unwrap().CoinFeedback(-coinDecrease);
            return true;
        }
        return false;
    }
    [BitStrap.Button]
    public void EarnCoins()
    {
        EarnCoins(500);
    }
    [BitStrap.Button]
    public void SpendCoins()
    {
        SpendCoins(500);
    }
    
    private void Update()
    {
        coins = Mathf.MoveTowards(coins, desiredCoins,currentCoinSpeed);
        
        if (earnedCoins && coins == desiredCoins)
        {
            onFinishedIncreasing.Call();
            onFinishedIncreasing.UnregisterAll();
            earnedCoins = false;
        }
    }

    [Button]
    public void GetCoinsByAd100()
    {
        GetCoinsByAd(100);
    }
    public void GetCoinsByAd(int value)
    {
        AdManager.Instance.Unwrap().ShowRewardedVideo(() => EarnCoins(value));
    }
}