﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PachaGamesManager : MonoBehaviour {
    public static PachaGamesManager instance;
    public GameObject player;
    public enum GameState
    {
        MENU,
        INGAME,
        FAILED,
        VICTORY,
        REWARD,
        REWARD_COMPLETE,
        CUSTOMIZE,
        CHEST
    }
    public GameState currentGameState;

    private void Awake()
    {
        instance = this;
        Application.targetFrameRate = 60;
    }
    [BitStrap.Button]
    public void StartGame()
    {
        currentGameState = GameState.INGAME;
        LevelCreator.Instance.Unwrap().CreateNewLevel();
        WindowManager.instance.ShowWindow(InGameWindow.instance);
    }
    [BitStrap.Button]
    public void Death()
    {
        currentGameState = GameState.FAILED;
        WindowManager.instance.ShowWindow(FailedWindow.instance);

    }
    [BitStrap.Button]
    public void Victory()
    {
        LevelManager.instance.NextLevel();
        currentGameState = GameState.VICTORY;
        WindowManager.instance.ShowWindow(VictoryWindow.instance);
    }
    
    [BitStrap.Button]
    public void Reward()
    {
        currentGameState = GameState.REWARD;
        RewardManager.Instance.Unwrap().IncreaseReward();
        WindowManager.instance.ShowWindow(RewardWindow.instance);
    }    
    [BitStrap.Button]
    public void RewardComplete()
    {
        currentGameState = GameState.REWARD_COMPLETE;
        WindowManager.instance.ShowWindow(RewardCompleteWindow.instance);
    }    
    
    [BitStrap.Button]
    public void LoadCustomization(int customizationClass)
    {
        currentGameState = GameState.CUSTOMIZE;
        WindowManager.instance.ShowWindow(CustomizationWindow.instance);
        ((CustomizationWindow.instance) as CustomizationWindow).SetupClass(customizationClass);
    }

    [BitStrap.Button]
    public void LoadMenuWindow()
    {
        currentGameState = GameState.MENU;
        WindowManager.instance.ShowWindow(MenuWindow.instance);
    }   
    [BitStrap.Button]
    public void LoadChestWindow()
    {
        currentGameState = GameState.CHEST;
        WindowManager.instance.ShowWindow(ChestWindow.instance);
    }   
    [BitStrap.Button]
    public void DeletePrefs()
    {
        PlayerPrefs.DeleteAll();
    }



}
