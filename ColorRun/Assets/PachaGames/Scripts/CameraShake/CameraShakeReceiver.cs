﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeReceiver : MonoBehaviour {


    public void DoCameraShake()
    {
        CameraShake.Instance.Unwrap().SetShake();
    }
}
