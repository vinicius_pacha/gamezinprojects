using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MenuWindow : UiWindow {

    public static UiWindow instance;
    public TMP_Text level;
    public virtual void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        PachaGamesManager.instance.StartGame();
    }
    public override void Show()
    {
        base.Show();
        level.text = string.Format("Level {0}", LevelManager.instance.level);

    }

    public override void Update()
    {
        base.Update();
        if (PopupManager.instance.currentPopup == SettingsPopup.instance) return;
        
        if (Input.anyKeyDown && Input.mousePosition.y < Screen.height * 0.8f)
        {
            StartGame();
        }
    }
}
