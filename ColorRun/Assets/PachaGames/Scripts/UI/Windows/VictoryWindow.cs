using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class VictoryWindow : UiWindow {
    public static UiWindow instance;
    public GameObject watchAdBtn;
    public GameObject noThanksBtn;
    public GameObject coinWidget;
    public TMP_Text rewardText;

    public virtual void Awake()
    {
        instance = this;
    }

    public override void Show()
    {
        base.Show();
        //watchAdBtn.SetActive(true);
        noThanksBtn.SetActive(true);
        coinWidget.SetActive(true);
        rewardText.text = string.Format("+{0}",CoinManager.Instance.Unwrap().levelCoins);

    }

    public void TryGet80Coins()
    {
        AdManager.Instance.Unwrap().ShowRewardedVideo(() =>
        {
            //watchAdBtn.SetActive(false);
            noThanksBtn.SetActive(false);
            coinWidget.SetActive(false);
            CoinManager.Instance.Unwrap().EarnCoins(80,NoThanks);
            CoinsFeedback.Instance.Unwrap().EarnCoinFromPosition(80, coinWidget.transform.position);

        });
    }

    public void EarnCoinsFromVictory(int value)
    {
        CoinsFeedback.Instance.Unwrap().EarnCoinFromPosition(value, coinWidget.transform.position);
    }
    public void NoThanks()
    {
        PachaGamesManager.instance.LoadMenuWindow();
    }
}
