﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipCustomization : MonoBehaviour
{
    int currentCustomization = 0;
    public Renderer playerRenderer;
    public Transform hatLocator;
    public List<GameObject> currentHats = new List<GameObject>();
    private Material initialMaterial;
    public Animator animator;
    public PlayerCustomization pc;
    void Start()
    {
        initialMaterial = playerRenderer.material;
    }
    [BitStrap.Button]
    public void EquipPlayerCustomization()
    {
        animator.SetTrigger("ChangeCustomization");
        for (int i = 0; i < currentHats.Count; i++)
        {
            Destroy(currentHats[i].gameObject);
        }
        currentHats.Clear();

        foreach (GameObject go in pc.hats)
        {
            GameObject gameObj = Instantiate(go, hatLocator);
            currentHats.Add(gameObj);
        }
        playerRenderer.material = pc.customizationMaterial;

    }

    [BitStrap.Button]
    public void EquipPlayerCustomization(CustomizationData customizationData)
    {
        var customizationObjectConfig = (customizationData.customizationObjectConfig) as PlayerCustomization;
        animator.SetTrigger("ChangeCustomization");
        for (int i = 0; i < currentHats.Count; i++)
        {
            Destroy(currentHats[i].gameObject);
        }
        currentHats.Clear();

        foreach (GameObject go in customizationObjectConfig.hats)
        {
            GameObject gameObj = Instantiate(go, hatLocator);
            currentHats.Add(gameObj);
        }
        playerRenderer.material = customizationObjectConfig.customizationMaterial;

    }
    
    void ResetCustomization()
    {
        for (int i = 0; i < currentHats.Count; i++)
        {
            currentHats.Remove(currentHats[i].gameObject);
            Destroy(currentHats[i].gameObject);
        }
        playerRenderer.material = initialMaterial;
    }

}
