﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
#if UNITY_EDITOR
    using UnityEditor;
#endif
using UnityEngine;

public class CustomizationObject : ScriptableObject
{
    public string name;
    public Sprite customizationSprite;
   
    public CustomizationCategory category;
    public string guid;
    public CustomizationClass customizationClass;
    public string CustomizationString
    {
        get { return guid.ToString(); }
    }

    public virtual void EquipCustomization()
    {
        
    }

    [Button]
    public void GetCode()
    {
        long instanceID;
#if UNITY_EDITOR
        AssetDatabase.TryGetGUIDAndLocalFileIdentifier(this,out guid,out instanceID);
#endif
    }

    private void Reset()
    {
        long instanceID;
#if UNITY_EDITOR
        AssetDatabase.TryGetGUIDAndLocalFileIdentifier(this,out guid,out instanceID);
#endif
    }

    private void OnValidate()
    {
        long instanceID;
#if UNITY_EDITOR
        AssetDatabase.TryGetGUIDAndLocalFileIdentifier(this,out guid,out instanceID);
#endif
    }
}
