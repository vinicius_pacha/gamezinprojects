﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ColorPattern : ScriptableObject
{
    public Color mainColor;
    public Color playerColor;
    public Color groundColor;
    public Sprite color;
    public void ComparePlayerColor( Tile t, Color color)
    {

        if (color == mainColor)
        {
            t.desiredColor = this;
            BasicEnvironment.Instance.Unwrap().CreatePlayerOnTile(t, this);
        }

    }
    
    public void CompareDesiredColor( Tile t, Color color)
    {
        if (color == mainColor)
        {
            t.desiredColor = this;
        }
    }

}
