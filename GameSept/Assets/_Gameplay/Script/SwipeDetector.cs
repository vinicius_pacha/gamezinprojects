﻿using System;
using System.Collections;
using BitStrap;
using ControlFreak2;
using UnityEngine;

public class SwipeDetector : BitStrap.Singleton<SwipeDetector>
{

    public enum SwipeDirection
    {
        Up,
        Down,
        Right,
        Left
    }

    public SafeAction<SwipeDirection> Swipe = new SafeAction<SwipeDirection>();
    public float minDistanceForSwipe = 20;
    private Vector2 fingerUp;
    private Vector2 fingerDown;
    public bool detectSwipeAfterRelease;

    void Update()
    {
        minDistanceForSwipe = Screen.width * 0.15f;
        //mobile
        if (Application.isMobilePlatform)
        {
            if (Input.touchCount == 0)
                return;
            var touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                fingerUp = touch.position;
                fingerDown = touch.position;
            }

            if (!detectSwipeAfterRelease && touch.phase == TouchPhase.Moved)
            {
                fingerDown = touch.position;
                DetectSwipe();
            }

            if (touch.phase == TouchPhase.Ended)
            {
                fingerDown = touch.position;
                DetectSwipe();
            }
        }
        
        else
        {
            if (Input.anyKeyDown)
            {
                fingerUp = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
                fingerDown = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
            }

            if (!detectSwipeAfterRelease && Input.anyKey)
            {
                fingerDown = new Vector2(Input.mousePosition.x,Input.mousePosition.y);;
                DetectSwipe();
            }

            if (Input.GetMouseButtonUp(0))
            {
                fingerDown = new Vector2(Input.mousePosition.x,Input.mousePosition.y);;
                DetectSwipe();
            }
        }
    }

    bool SwipeDistanceCheckMet()
    {
        return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
    }    
    bool IsVerticalSwipe()
    {
        return VerticalMovementDistance() > HorizontalMovementDistance();
        return true;
    }
    void DetectSwipe()
    {
        if (SwipeDistanceCheckMet())
        {
            if (IsVerticalSwipe())
            {
                var direction = fingerDown.y - fingerUp.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;
                Swipe.Call(direction);
            }
            else
            {
                var direction = fingerDown.x - fingerUp.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
                Swipe.Call(direction);
            }

            fingerUp = fingerDown;
        } 
    }

    float VerticalMovementDistance()
    {
        return Mathf.Abs(fingerDown.y - fingerUp.y);
    }  
    float HorizontalMovementDistance()
    {
        return Mathf.Abs(fingerDown.x - fingerUp.x);
    }
}