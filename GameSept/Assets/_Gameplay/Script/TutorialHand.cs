﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using BitStrap;
using UnityEngine;

public class TutorialHand : BitStrap.Singleton<TutorialHand>
{

    public Vector2 offset;
    public RectTransform canvasRect;
    private float t = 0;
    private Vector2 anchoredPosition;
    private SwipeDetector.SwipeDirection direction;
    public Jun_TweenRuntime positionTween;
    public float distance = 5;

    void Start()
    {
        TutorialHand.Instance.Unwrap();
        gameObject.SetActive(false);
    }
    public void AnimateHandPosition(Vector3 position, SwipeDetector.SwipeDirection direction)
    {
        gameObject.SetActive(true);

        RectTransform uiElement = transform as RectTransform;

        Vector2 viewportPosition = Camera.main.WorldToViewportPoint(position);
        Vector2 worldObjectScreenPosition = new Vector2(
            ((viewportPosition.x * canvasRect.sizeDelta.x) - (canvasRect.sizeDelta.x * 0.5f)),
            ((viewportPosition.y * canvasRect.sizeDelta.y) - (canvasRect.sizeDelta.y * 0.5f)));
        positionTween.firstTween.fromVector = worldObjectScreenPosition;
        switch (direction)
        {
            case SwipeDetector.SwipeDirection.Up:
                positionTween.firstTween.toVector = worldObjectScreenPosition + Vector2.up * distance;
                break;
            case SwipeDetector.SwipeDirection.Down:
                positionTween.firstTween.toVector = worldObjectScreenPosition + Vector2.down * distance;

                break;
            case SwipeDetector.SwipeDirection.Right:
                positionTween.firstTween.toVector = worldObjectScreenPosition + Vector2.right * distance;
                break;
            case SwipeDetector.SwipeDirection.Left:
                positionTween.firstTween.toVector = worldObjectScreenPosition + Vector2.left * distance;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
        }

        positionTween.Play();

        uiElement.anchoredPosition = worldObjectScreenPosition + offset;
    }
    [Button]
    public void Stop()
    {
        gameObject.SetActive(false);
        positionTween.PlayAtTime(0);
        positionTween.StopPlay();
    }

    private void Update()
    {
        t += Time.deltaTime;
    }
}
