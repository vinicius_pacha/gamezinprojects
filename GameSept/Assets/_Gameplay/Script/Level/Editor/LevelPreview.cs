﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomPreview(typeof(LevelSprite))]
public class LevelPreview : ObjectPreview
{
    public override bool HasPreviewGUI()
    {
        return true;
    }

    public override void OnPreviewGUI(Rect r, GUIStyle background)
    {
        var levelSprite = (target as LevelSprite);
        GUI.DrawTexture(new Rect(levelSprite.initialColors.width*5,levelSprite.initialColors.width*5,levelSprite.initialColors.width*10,levelSprite.initialColors.height*10), levelSprite.initialColors);
        GUI.DrawTexture(new Rect(levelSprite.desiredColors.width*5 + levelSprite.initialColors.width*10+5,levelSprite.desiredColors.width*5,levelSprite.desiredColors.width*10,levelSprite.desiredColors.height*10), levelSprite.desiredColors);
    }
}
