﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using BitStrap;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

[System.Serializable]
public struct Tutorial
{
    public Vector2 tilePosition;
    public SwipeDetector.SwipeDirection direction;
}
[System.Serializable]
public class ChangeColor
{
    public Color from;
    public Color to;
    
}
public class LevelSprite : ScriptableObject
{
    [FormerlySerializedAs("sprite")] public Texture2D initialColors;
    [FormerlySerializedAs("sprite2")] public Texture2D desiredColors;
    public string name;
    public float cameraFov = 20;
    public float yOffset;
    public float scale;
    public ChangeColor[] colors;
    public List<Tutorial> tutorialStep;
    public int Width
    {
        get
        {
            return initialColors.width;
        }
    }
    public int Height
    {
        get
        {
            return initialColors.height;
        }
    }
    [Button]
    private void OnValidate()
    {
#if UNITY_EDITOR
        var assetPath = AssetDatabase.GetAssetPath(this);
        name = Path.GetFileName(assetPath).Split('.')[0];
        initialColors = Resources.Load<Texture2D>(string.Format("LevelSprites/{0}Start",name) );
        desiredColors = Resources.Load<Texture2D>(string.Format("LevelSprites/{0}",name) );
#endif    
    }
    
    [Button]
    private void ChangeColors()
    {
#if UNITY_EDITOR
        var assetPath = AssetDatabase.GetAssetPath(this);
        name = Path.GetFileName(assetPath).Split('.')[0];
        initialColors = Resources.Load<Texture2D>(string.Format("LevelSprites/{0}Start",name) );
        for (int i = 0; i < initialColors.width; i++)
        {
            for (int j = 0; j < initialColors.height; j++)
            {
                var curPixel = initialColors.GetPixel(i, j);
                foreach (var c in colors)
                {
                    if (curPixel == c.from)
                        initialColors.SetPixel(i, j,c.to);
                }
            }
        }
        initialColors.Apply();
        
        desiredColors = Resources.Load<Texture2D>(string.Format("LevelSprites/{0}",name) );
        for (int i = 0; i < desiredColors.width; i++)
        {
            for (int j = 0; j < desiredColors.height; j++)
            {
                var curPixel = desiredColors.GetPixel(i, j);
                foreach (var c in colors)
                {
                    if (curPixel == c.from)
                        desiredColors.SetPixel(i, j,c.to);
                }
            }
        }
        desiredColors.Apply();

#endif    
    }
}
