﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : BitStrap.Singleton<GameController>
{
    public BallPlayer ballPlayer;
    public LayerMask playerMask;
    public Camera camera;

    // Start is called before the first frame update
    void Start()
    {
        SwipeDetector.Instance.Unwrap().Swipe.Register(Swipe);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit,100,playerMask))
            {
                if (ballPlayer != null && ballPlayer.transform != hit.transform)
                    ballPlayer = hit.transform.GetComponent<BallPlayer>();
                else if(ballPlayer == null)
                    ballPlayer = hit.transform.GetComponent<BallPlayer>();

            }
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.GetTouch(0).position);
            if (Physics.Raycast(ray, out hit,100,playerMask))
            {
                if (ballPlayer != null && ballPlayer.transform != hit.transform)
                    ballPlayer = hit.transform.GetComponent<BallPlayer>();
                else if(ballPlayer == null)
                    ballPlayer = hit.transform.GetComponent<BallPlayer>();

            }
        }

    }

    void Swipe(SwipeDetector.SwipeDirection dir)
    {
        if (ballPlayer == null) return;
        switch (dir)
        {
            case SwipeDetector.SwipeDirection.Up:
                ballPlayer.MoveUp();
                break;
            case SwipeDetector.SwipeDirection.Down:
                ballPlayer.MoveDown();
                break;
            case SwipeDetector.SwipeDirection.Right:
                ballPlayer.MoveRight();
                break;
            case SwipeDetector.SwipeDirection.Left:
                ballPlayer.MoveLeft();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
        }

    }
}
