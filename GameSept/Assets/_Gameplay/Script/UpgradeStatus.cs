﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class UpgradeStatus : BitStrap.Singleton<UpgradeStatus>
{
    [FormerlySerializedAs("createBallLevel")] public int startMinionsLevel = 1;
    public int ballSizeLevel = 1;
    [FormerlySerializedAs("ballLaunchLevel")] public int minionStrengthLevel = 1;
    public TMP_Text createPrice;
    public TMP_Text sizePrice;
    public TMP_Text launchPrice;    
    
    public TMP_Text createLevel;
    public TMP_Text sizeLevel;
    public TMP_Text launchLevel;
    public Button createButton;
    //public Button sizeButton;
    public Button launchButton;
    public const string START_MINION_LEVEL = "START_MINION_LEVEL";
    public const string BALL_SIZE_LEVEL = "BALL_SIZE_LEVEL";
    public const string MINION_STRENTH_LEVEL = "MINION_STRENTH_LEVEL";
    private void Start()
    {
        if (PlayerPrefs.HasKey(START_MINION_LEVEL))
        {
            startMinionsLevel = PlayerPrefs.GetInt(START_MINION_LEVEL);
        }
        if (PlayerPrefs.HasKey(BALL_SIZE_LEVEL))
        {
            ballSizeLevel = PlayerPrefs.GetInt(BALL_SIZE_LEVEL);

        }
        if (PlayerPrefs.HasKey(MINION_STRENTH_LEVEL))
        {
            minionStrengthLevel = PlayerPrefs.GetInt(MINION_STRENTH_LEVEL);
        }
    }

    public void UpgradeMinionStart()
    {
        var price = GetPriceValue(startMinionsLevel);
        if(CoinManager.Instance.Unwrap().SpendCoins(price))
        {
            startMinionsLevel++;
            PlayerPrefs.SetInt(START_MINION_LEVEL,startMinionsLevel);
        }
    }
    public void UpgradeSize()
    {
        var price = GetPriceValue(ballSizeLevel);
        if(CoinManager.Instance.Unwrap().SpendCoins(price))
        {
            ballSizeLevel++;
            PlayerPrefs.SetInt(BALL_SIZE_LEVEL,ballSizeLevel);
        }
    }
    public void UpgradeMinionPower()
    {
        var price = GetPriceValue(minionStrengthLevel);
        if(CoinManager.Instance.Unwrap().SpendCoins(price))
        {
            minionStrengthLevel++;
            PlayerPrefs.SetInt(MINION_STRENTH_LEVEL,minionStrengthLevel);
        }
    }

    int GetPriceValue(int level)
    {
        return 50 + (level-1) * 10;
    }

    private void Update()
    {
        launchPrice.text = GetPriceValue(minionStrengthLevel).ToString();
        createPrice.text = GetPriceValue(startMinionsLevel).ToString();
        sizePrice.text = GetPriceValue(ballSizeLevel).ToString();
        createLevel.text = string.Format("{0}", startMinionsLevel-1);
        launchLevel.text = string.Format("LV{0}", minionStrengthLevel);
        sizeLevel.text = string.Format("LV{0}", ballSizeLevel);
        
        launchButton.gameObject.SetActive(LevelManager.instance.level>3);
        //sizeButton.gameObject.SetActive(LevelManager.instance.level>3);
        createButton.gameObject.SetActive(LevelManager.instance.level>3);
        
        launchButton.interactable = CoinManager.Instance.Unwrap().desiredCoins >= GetPriceValue(minionStrengthLevel);
        //sizeButton.interactable = CoinManager.Instance.Unwrap().desiredCoins >= GetPriceValue(ballSizeLevel);
        createButton.interactable = CoinManager.Instance.Unwrap().desiredCoins >= GetPriceValue(startMinionsLevel);
        
        

    }
}
