﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using BitStrap;
using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Random = System.Random;

public class BasicEnvironment : Singleton<BasicEnvironment>
{
    public float fovOffset = 0 ;
    [Header("UI")] public Image referenceParent;
    public Image referenceArt;
    public ParticleSystem victoryEffect;
    public BallPlayer ballPlayer;
    public List<BallPlayer> instantiatedBallPlayers = new List<BallPlayer>();
    public Tile tilePrefab;
    public List<LevelSprite> allLevels = new List<LevelSprite>();
    public LevelSprite currentLevel;
    public int xCount;
    public int yCount;
    public float timeBetween = 0.1f;
    public List<List<Tile>> allTiles;
    public List<ColorPattern> colorPatterns;
    public Camera camera;
    public bool oddsX = false;
    public bool oddsY = false;
    public Vector3 cameraOffset;
    public Texture2D tempTexture;
    public bool isComplete = false;
    private Vector3 initialPosition;
    private int tutorialIndex;
    public Jun_TweenRuntime tween;
    public Jun_TweenRuntime correctTween;
    public Vector3 correctSpritePosition;
    public Light light;
    public LevelSprite firstLevel;
    IEnumerator Start()
    {
        Application.targetFrameRate = 60;
        CreateAll();
        yield return new WaitForSeconds(0.1f);

        LoadLevel(false);
        yield return new WaitForSeconds(0.1f);

        SetSprite();
        yield return new WaitForSeconds(0.1f);
        InGameWindow.instance.onShow.Register(() => LoadLevel());
        SwipeDetector.Instance.Unwrap().Swipe.Register(CheckSwipeTutorial);
    }

    void CheckSwipeTutorial(SwipeDetector.SwipeDirection dir)
    {
        if (GameController.Instance.Unwrap().ballPlayer == null) return;
        if (tutorialIndex < currentLevel.tutorialStep.Count)
        {
            if (currentLevel.tutorialStep[tutorialIndex].direction == dir)
            {
                tutorialIndex++;
                CheckTutorial();
            }
        }
        else
        {
            TutorialHand.Instance.Unwrap().Stop();
        }
    }

    [Button]
    public void SetSprite()
    {
        var rectTransform = referenceParent.transform as RectTransform;
        rectTransform.sizeDelta = new Vector2(currentLevel.Width * 150, currentLevel.Height * 150);
        tempTexture = new Texture2D(currentLevel.Width, currentLevel.Height);
        tempTexture.filterMode = FilterMode.Point;
        for (int i = 0; i < currentLevel.Width; i++)
        {
            for (int j = 0; j < currentLevel.Height; j++)
            {
                var c = currentLevel.desiredColors.GetPixel(i, j);

                foreach (var cp in colorPatterns)
                {
                    if (c == cp.mainColor)
                    {
                        c = cp.groundColor;
                        break;
                    }
                }

                tempTexture.SetPixel(i, j, c);
            }
        }

        tempTexture.Apply();
        var mySprite = Sprite.Create(tempTexture, new Rect(0.0f, 0.0f, tempTexture.width, tempTexture.height),
            new Vector2(0.5f, 0.5f), 100.0f);

        referenceArt.sprite = mySprite;

    }

    private void Awake()
    {
        initialPosition = camera.transform.position;
    }

    public void CreatePlayerOnTile(Tile tile, ColorPattern c)
    {

        var bp = Instantiate(ballPlayer, new Vector3(tile.transform.position.x, 1f, tile.transform.position.z),
            Quaternion.identity);
        instantiatedBallPlayers.Add(bp);
        bp.currentTile = tile;
        bp.SetPlayerColor(c);

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            foreach (var b in instantiatedBallPlayers)
            {
                b.MoveRight();
            }
        }
        Vector3 desiredCamPosition = initialPosition;
        
        if (oddsX)
            desiredCamPosition.x -= 0.5f;
        if (oddsY)
            desiredCamPosition.z -= 0.5f;

        camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, currentLevel.cameraFov + fovOffset, Time.deltaTime * 10);
        camera.transform.position =
            Vector3.Lerp(camera.transform.position,
                desiredCamPosition + cameraOffset + transform.forward * currentLevel.yOffset, Time.deltaTime * 10);
    }

    [Button]
    void CreateAll()
    {
        allTiles = new List<List<Tile>>();
        for (int i = 0; i < xCount; i++)
        {
            var line = new List<Tile>();
            for (int j = 0; j < yCount; j++)
            {
                var t =
                    Instantiate(tilePrefab, new Vector3(-xCount / 2 + i, 0, -yCount / 2 + j),
                        Quaternion.identity) as Tile;
                t.x = i;
                t.y = j;
                line.Add(t);
                t.transform.parent = transform;
            }

            allTiles.Add(line);
        }
    }

    [Button]
    public void DeleteAll()
    {
        foreach (var t in allTiles)
        {
            foreach (var l in t)
            {
                if (Application.isPlaying)
                    Destroy(l.gameObject);
                else
                {
                    DestroyImmediate(l.gameObject);
                }

            }
        }

        allTiles.Clear();
    }

    [Button]
    void SetupEnvironment()
    {
        isComplete = false;
        StartCoroutine(EnvironmentAnimation());
    }

    IEnumerator EnvironmentAnimation()
    {
        for (int i = 0; i < xCount; i++)
        {
            StartCoroutine(ExecuteLine(allTiles[i]));
            yield return new WaitForSeconds(timeBetween);
        }
        
        foreach (var player in instantiatedBallPlayers)
        {
            float t = 0;
            while (!player.currentTile.canWalk && t < 0.5f)
            {
                t += Time.deltaTime;
                yield return null;
            } 

            player.gameObject.SetActive(true);
            player.currentTile.ChangeColor(player.colorPattern);
            player.currentTile.canWalk = false;
        }
        light.shadows = LightShadows.None;

        tutorialIndex = 0;
        yield return new WaitForSeconds(0.3f);
        CheckTutorial();


    }

    void CheckTutorial()
    {
        if (LevelManager.instance.level > 2) return;
        
        if (currentLevel.tutorialStep.Count > tutorialIndex)
        {
            var xCoord = (xCount - currentLevel.Width) / 2 + currentLevel.tutorialStep[tutorialIndex].tilePosition.x;
            var yCoord = (yCount - currentLevel.Height) / 2 + currentLevel.tutorialStep[tutorialIndex].tilePosition.y;
            TutorialHand.Instance.Unwrap().AnimateHandPosition(allTiles[(int) xCoord][(int) yCoord].transform.position,
                currentLevel.tutorialStep[tutorialIndex].direction);
        }
    }

    IEnumerator ExecuteLine(List<Tile> line)
    {
        foreach (var t in line)
        {
            switch (t.currentTileType)
            {
                case Tile.TileType.GROUND:
                    t.Disappear();
                    break;
                case Tile.TileType.WALL:
                    //t.Appear();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            yield return new WaitForSeconds(timeBetween);
        }
    }

    [Button]
    public void ReadTexture()
    {
        var currentLevel = this.currentLevel;
        
        if (LevelManager.instance.level > allLevels.Count)
        {
            if (UnityEngine.Random.Range(0, 100) < 50)
            {
                currentLevel.desiredColors = FlipTexture(currentLevel.desiredColors);
                currentLevel.initialColors = FlipTexture(currentLevel.initialColors);
            }
            if (UnityEngine.Random.Range(0, 100) < 50)
            {
                currentLevel.desiredColors = FlipTextureVertically(currentLevel.desiredColors);
                currentLevel.initialColors = FlipTextureVertically(currentLevel.initialColors);
            }
        }
        
        ResetAllTilesInstant();
        for (int i = 0; i < currentLevel.Width; i++)
        {
            for (int j = 0; j < currentLevel.Height; j++)
            {
                var color = currentLevel.desiredColors.GetPixel(i, j);
                var playerColor = currentLevel.initialColors.GetPixel(i, j);
                oddsX = currentLevel.Width % 2 == 0;
                oddsY = currentLevel.Height % 2 == 0;

                var xCoord = (xCount - currentLevel.Width) / 2 + i;
                var yCoord = (yCount - currentLevel.Height) / 2 + j;
                var currentTile = allTiles[xCoord][yCoord];
                if (color == Color.white)
                {
                    currentTile.currentTileType = Tile.TileType.WALL;
                }
                else
                {
                    if (playerColor != Color.black)
                    {
                        foreach (var cp in colorPatterns)
                        {
                            cp.ComparePlayerColor(currentTile, playerColor);
                        }
                    }

                    if (color != Color.black)
                    {
                        foreach (var cp in colorPatterns)
                        {
                            cp.CompareDesiredColor(currentTile, color);
                        }
                    }

                    currentTile.currentTileType = Tile.TileType.GROUND;
                }
            }
        }
    }

    [Button]
    IEnumerator ResetAllTiles()
    {
        foreach (var line in allTiles)
        {
            StartCoroutine(ResetLineCoroutine(line));
            yield return new WaitForSeconds(timeBetween);
        }
    }

    [Button]
    void ResetAllTilesInstant()
    {

        instantiatedBallPlayers.Clear();

        foreach (var line in allTiles)
        {
            foreach (var t in line)
            {
                t.currentTileType = Tile.TileType.WALL;
                t.currentColor = null;
                t.desiredColor = null;
            }
        }
    }

    [Button]
    public void VerifyCompletion()
    {
        if (isComplete) return;
        for (int i = 0; i < currentLevel.Width; i++)
        {
            for (int j = 0; j < currentLevel.Height; j++)
            {

                var xCoord = (xCount - currentLevel.Width) / 2 + i;
                var yCoord = (yCount - currentLevel.Height) / 2 + j;
                var currentTile = allTiles[xCoord][yCoord];

                if (currentTile.currentTileType == Tile.TileType.GROUND && !currentTile.IsComplete())
                {
                    return;
                }
            }
        }

        isComplete = true;
      
        StartCoroutine(CompleteCoroutine());
    }

    IEnumerator CompleteCoroutine()
    {
        light.shadows = LightShadows.Soft;
        for (int i = 0; i < instantiatedBallPlayers.Count; i++)
        {
            instantiatedBallPlayers[i].Disappear();
            yield return new WaitForSeconds(0.1f);
        }
        correctTween.Play();
        VibrationManager.Instance.Unwrap().MediumVibration();
        victoryEffect.Play();
        for (int i = 0; i < currentLevel.Width; i++)
        {
            var xCoord = (xCount - currentLevel.Width) / 2 + i;
            StartCoroutine(FlashLineCoroutine(allTiles[xCoord]));
            yield return new WaitForSeconds(timeBetween);
        }
        yield return new WaitForSeconds(timeBetween * xCount);

        PachaGamesManager.instance.Victory();

    }

    IEnumerator DisappearLevel()
    {
        tween.firstTween.fromVector = correctSpritePosition;
        tween.firstTween.toVector = tween.firstTween.fromVector - Vector3.right * 1000;
        tween.PlayAtTime(0);
        tween.Play();
        yield return StartCoroutine(ResetAllTiles());
        yield return new WaitForSeconds(timeBetween * xCount * 1.5f);
        ResetAllTilesInstant();
    }

    [Button]
    void LoadLevel(bool disappearLevel = true)
    {
        correctTween.PlayAtTime(0);
        correctTween.StopPlay();
        victoryEffect.Simulate(0);
        victoryEffect.Stop();
        StartCoroutine(LoadLevelCoroutine(disappearLevel));

    }

    IEnumerator LoadLevelCoroutine(bool disappearLevel)
    {
        if(disappearLevel)
            yield return StartCoroutine(DisappearLevel());
        if (LevelManager.instance.level == 1)
            currentLevel = firstLevel;
        else
        {
            currentLevel = allLevels[(LevelManager.instance.level-2) % allLevels.Count];
            if (LevelManager.instance.level-2 > allLevels.Count)
                currentLevel = allLevels[UnityEngine.Random.Range(0, allLevels.Count)];
        }
        tween.transform.localScale = currentLevel.scale * Vector3.one;
        tween.firstTween.toVector = correctSpritePosition;
        tween.firstTween.fromVector = tween.firstTween.toVector + Vector3.right * 1000;
        tween.PlayAtTime(0);
        tween.Play();
        ReadTexture();
        SetSprite();
        SetupEnvironment();
    }

IEnumerator FlashLineCoroutine(List<Tile> line)
    {
       
        foreach (var t in line)
        {
            t.FlashGround();
            yield return new WaitForSeconds(timeBetween);
        }
    }
    
    IEnumerator ResetLineCoroutine(List<Tile> line)
    {
        foreach (var t in line)
        {
            if(t.currentTileType == Tile.TileType.GROUND)
                t.Appear();
            t.currentTileType = Tile.TileType.WALL;
            yield return new WaitForSeconds(timeBetween);
        }
    }
    
    public static Texture2D FlipTextureVertically(Texture2D original)
    {
        Texture2D flipped = new Texture2D(original.width, original.height, TextureFormat.ARGB32, false);

        int xN = original.width;
        int yN = original.height;

        for (int i = 0; i < xN; i++)
        {
            for (int j = 0; j < yN; j++)
            {
                flipped.SetPixel(i, yN - j - 1, original.GetPixel(i, j));
            }
       }
        flipped.Apply();

        return flipped;
    }
    Texture2D FlipTexture(Texture2D original)
    {
        Texture2D flipped = new Texture2D(original.width,original.height);
         
        int xN = original.width;
        int yN = original.height;

        for(int i=0;i<xN;i++){
            for(int j=0;j<yN;j++){
                flipped.SetPixel(xN-i-1, j, original.GetPixel(i,j));
            }
        }
        flipped.Apply();
         
        return flipped;
    }

}
