﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
public class Tile : MonoBehaviour
{
    public int x;
    public int y;

    public AnimationCurve scaleCurve;
    public AnimationCurve localPositionCurve;
    public AnimationCurve flashCurve;
    public Material wallMaterial;
    public Material groundMaterial;
    public TileType currentTileType;
    public ColorPattern desiredColor;
    public float scaleTime;
    public float flashTime;
    public Renderer r;
    public ColorPattern currentColor;
    public bool canWalk = false;
    private void Reset()
    {
        r.material = groundMaterial;
    }

    public enum Dir
    {
        UP,
        DOWN,
        RIGHT,
        LEFT
    }

    public enum TileType
    {
        GROUND,
        WALL
    }

    [Button]
    public void FlashGround()
    {
        if (currentTileType == TileType.WALL) return;
        StartCoroutine(FlashGroundCoroutine());
    }

    public bool IsComplete()
    {
        return desiredColor == currentColor;
    }
    IEnumerator FlashGroundCoroutine()
    {
        float t = 0;
        var tempMaterial = r.sharedMaterial;
        Color startColor = r.material.GetColor("_LightTint");
        while (t < 1)
        {
            t += Time.deltaTime / flashTime;

            var c = Color.Lerp(startColor, startColor + Color.white * 0.2f, flashCurve.Evaluate(t));
            r.material.SetColor("_LightTint",c);
            yield return null;
        }
        r.material.SetColor("_LightTint",startColor);
        r.material = tempMaterial;

    }
    
    IEnumerator ChangeColorCoroutine(Color color)
    {
        float t = 0;
        while (t < 1)
        {
            t += Time.deltaTime / 0.2f;

            var c = Color.Lerp(color + Color.white * 0.2f, color, t);
            r.material.SetColor("_LightTint",c);
            yield return null;
        }
        r.material.SetColor("_LightTint",color);

    }

    public void ChangeColor(ColorPattern cp)
    {
        currentColor = cp;
        StartCoroutine(ChangeColorCoroutine(cp.groundColor));
    }

    public Tile GetTile(Dir dir)
    {
        switch (dir)
        {
            case Dir.UP:
                return Up();
                break;
            case Dir.DOWN:

                return Down();
                break;
            case Dir.RIGHT:
                return Right();
                break;
            case Dir.LEFT:
                return Left();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
        }
    }

    public Tile Left()
    {
        var leftTile = BasicEnvironment.Instance.Unwrap().allTiles[x - 1][y];
        if (leftTile.currentTileType == TileType.GROUND && leftTile.canWalk)
            return leftTile;
        else
        {
            return null;
        }
    }

    public Tile Right()
    {
        var leftTile = BasicEnvironment.Instance.Unwrap().allTiles[x + 1][y];
        if (leftTile.currentTileType == TileType.GROUND && leftTile.canWalk)
            return leftTile;
        else
        {
            return null;
        }
    }

    public Tile Up()
    {
        var leftTile = BasicEnvironment.Instance.Unwrap().allTiles[x][y + 1];
        if (leftTile.currentTileType == TileType.GROUND && leftTile.canWalk)
            return leftTile;
        else
        {
            return null;
        }
    }

    public void MoveDirection()
    {
        
    }

    public Tile Down()
    {
        var leftTile = BasicEnvironment.Instance.Unwrap().allTiles[x][y - 1];
        if (leftTile.currentTileType == TileType.GROUND && leftTile.canWalk)
            return leftTile;
        else
        {
            return null;
        }
    }
   
    private void Awake()
    {
        Appear();
    }

    [Button]
    public void Appear()
    {
        Reset();
        StartCoroutine(CoroutineTileToWall());
    }
    [Button]
    public void Disappear()
    {
        StartCoroutine(CoroutineTileToGround());
    }

    
    
    IEnumerator CoroutineTileToWall()
    {
        float t = 0;
        var tempPosition = transform.localPosition;
        while (t < 1)
        {
            t += Time.deltaTime / scaleTime;
            transform.localScale = scaleCurve.Evaluate(t) * Vector3.one;
            if (t >= 0.1f)
            {
                r.material = wallMaterial;
            }
            tempPosition.y = localPositionCurve.Evaluate(t) * 0.5f;
            transform.localPosition = tempPosition;
            yield return null;
        }

        tempPosition.y = 0.5f;
        transform.localPosition = tempPosition;
        canWalk = false;
        transform.localScale = 1 * Vector3.one;
    }
    
    IEnumerator CoroutineTileToGround()
    {
        float t = 0;
        var tempPosition = transform.localPosition;
        while (t < 1)
        {
            t += Time.deltaTime / scaleTime;
            transform.localScale = scaleCurve.Evaluate(t) * Vector3.one;
            if (t >= 0.9f)
            {
                r.material = groundMaterial;
            }
            tempPosition.y = localPositionCurve.Evaluate(1 - t) * 0.5f;
            transform.position = tempPosition;
            yield return null;
        }

        tempPosition.y = 0;
        transform.position = tempPosition;
        
        transform.localScale = 1 * Vector3.one;
        canWalk = true;
    }
}
