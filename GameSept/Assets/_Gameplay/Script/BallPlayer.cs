﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;
using UnityEngine.Serialization;

public class BallPlayer : MonoBehaviour
{
    public Renderer rend;
    public Tile currentTile;
    public ColorPattern colorPattern;
    public float walkTime;
    public AnimationCurve selectedCurve;
    public AnimationCurve scaleCurve;
    public ParticleSystem movementParticle;
    public void SetPlayerColor(ColorPattern c)
    {
        ParticleSystem.MainModule main = movementParticle.main;
        main.startColor = c.playerColor;
        colorPattern = c;
        rend.material.SetColor("_Color", c.playerColor);
        rend.material.SetColor("_LightTint", c.playerColor );
        rend.material.SetColor("_ColorDim",  c.playerColor * 0.75f);
    }

    void Awake()
    {
        movementTask= new Task(null);
    }
    private Task movementTask; 
    [Button]
    public void MoveLeft()
    {
        if (movementTask != null && movementTask.Running) return;
        movementTask = new Task(MoveCoroutine(Tile.Dir.LEFT));
    }
    [Button]
    public void MoveRight()
    {
        if (movementTask != null && movementTask.Running) return;
        movementTask = new Task(MoveCoroutine(Tile.Dir.RIGHT));
    }
    [Button]
    public void MoveUp()
    {
        if (movementTask != null && movementTask.Running) return;
        movementTask = new Task(MoveCoroutine(Tile.Dir.UP));
    }
    [Button]
    public void MoveDown()
    {
        if (movementTask != null && movementTask.Running) return;
        movementTask = new Task(MoveCoroutine(Tile.Dir.DOWN));
    }

    IEnumerator MoveCoroutine(Tile.Dir direction)
    {
        int i = 0;
        currentTile.canWalk = true;

        while ( currentTile.GetTile(direction) != null)
        {
            Vector3 initialPos = currentTile.transform.position;
            float t = 0;
            currentTile.ChangeColor(colorPattern);

            while (t < 1)
            {
                t += Time.deltaTime / walkTime;
                transform.position = Vector3.Lerp(initialPos, currentTile.GetTile(direction).transform.position, t) + Vector3.up;
                yield return null;
            }
            currentTile = currentTile.GetTile(direction);
            i++;
        }
        
        if(i > 0 )
            VibrationManager.Instance.Unwrap().SmallVibration();
        currentTile.ChangeColor(colorPattern);
        currentTile.canWalk = false;
        BasicEnvironment.Instance.Unwrap().VerifyCompletion();

    }

    private void Update()
    {
        if (GameController.Instance.Unwrap().ballPlayer == this)
        {
            rend.material.SetColor("_LightTint", Color.Lerp(colorPattern.playerColor, colorPattern.playerColor + Color.white * 0.1f, selectedCurve.Evaluate(Time.time*2f%1.0f)));
        }
        else
        {
            rend.material.SetColor("_LightTint", colorPattern.playerColor);
        }
    }

    public void Disappear()
    {
        StartCoroutine(DisappearCoroutine());
    }

    IEnumerator DisappearCoroutine()
    {
        float t = 0;

        while (t < 1)
        {
            t += Time.deltaTime*5;
            transform.localScale = Vector3.one * scaleCurve.Evaluate(t);
            yield return null;
        }
        Destroy(this.gameObject);
    }
}
