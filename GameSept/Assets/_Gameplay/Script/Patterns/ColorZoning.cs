﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorZoning : ScriptableObject
{
    public ColorPattern[] colorPatterns;
}
