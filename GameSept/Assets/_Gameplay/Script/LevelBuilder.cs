﻿using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Monetization;

public class LevelBuilder : BitStrap.Singleton<LevelBuilder>
{
    public GameObject[] levels;
    public GameObject[] bossLevel;
    public List<GameObject> instantiatedObjects = new List<GameObject>();
    private Vector3 lastRoomPosition;
    void Start()
    {
        MenuWindow.instance.onShow.Register(LoadLevel);
    }
    [Button]    

    public void LoadLevel()
    {
        var lvRest = (LevelManager.instance.level - 1)%5;
        var lvIndex = (LevelManager.instance.level - 1)/5;
        var currentSquadLevel = lvIndex * 4 + lvRest;
        if (currentSquadLevel >= levels.Length)
            currentSquadLevel = Random.Range(2, levels.Length);
        var currentLevel = levels[currentSquadLevel];
        
        if (lvRest == 4)
        {
            currentLevel = bossLevel[lvIndex % bossLevel.Length];
        }
        LoadLevel(currentLevel);
    }
    [Button]    
    public void LoadLevel(GameObject currentLevel)
    {
        ClearRoom();
        Vector3 roomDelta =Vector3.forward*4;
        lastRoomPosition = Vector3.forward*2;
      

    }
    
    [Button]
    public void ClearRoom()
    {
        
    }
    
}
