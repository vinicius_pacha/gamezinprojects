using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCompleteWindow : UiWindow {

    public Animator animator;
    public static UiWindow instance;

    public virtual void Awake()
    {
        instance = this;
    }
    public override void Show()
    {
        base.Show();
        animator.SetTrigger("Animate");
    }
    public override void Hide()
    {
        base.Hide();
        animator.SetTrigger("Reset");
    }
    public void LoadMenu()
    {
        PachaGamesManager.instance.LoadMenuWindow();
    }

}
