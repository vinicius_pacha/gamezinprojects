using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InGameWindow : UiWindow {
    public static UiWindow instance;
    public TMP_Text levelText;
    public virtual void Awake()
    {
        instance = this;
    }

    public override void Show()
    {
        base.Show();
        levelText.text = "LEVEL "+LevelManager.instance.level;
    }
}
