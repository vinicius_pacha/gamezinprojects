using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardCompleteWindow : UiWindow {
    public static UiWindow instance;
    public Image itemSprite;
    
    public virtual void Awake()
    {
        instance = this;
    }

    public override void Show()
    {
        base.Show();
        var currentReward = RewardManager.Instance.Unwrap().GetCurrentReward();
        itemSprite.sprite = currentReward.customizationObjectConfig.customizationSprite;
    }

    public void ClaimReward()
    {
        RewardManager.Instance.Unwrap().TryClaimReward();

    }
    public void LoseReward()
    {
        RewardManager.Instance.Unwrap().LoseReward();
    }
}
