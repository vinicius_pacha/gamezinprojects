using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class ChestWindow : UiWindow {
    public static UiWindow instance;
    public int chestKeyQuantity = 0;

    public Image[] chestKeys; 
    public Color hasKey; 
    public Color noKey; 
    public Jun_TweenRuntime noThanks;
    public GameObject keyWidget; 
    public GameObject watchAd; 
    public GameObject continueBtn; 
    public List<Chest> chests;
    public SafeAction onOpenChest = new SafeAction();
    public virtual void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        onOpenChest.Register(SetupGameobjects);
    }

    void RandomizeChests()
    {
        var customizationData = ChestManager.Instance.Unwrap().GetCurrentChestReward();
        int[] coins = ChestManager.Instance.Unwrap().coinValues;
        List<Reward> chestRewards = new List<Reward>();
        foreach (var c in coins)
        {
            Reward r = new Reward();
            r.type = Reward.RewardType.COIN;
            r.value = c;
            chestRewards.Add(r);
        }
        
        Reward currentChestCustomizationReward = new Reward();
        currentChestCustomizationReward.type = Reward.RewardType.CUSTOMIZATION;
        currentChestCustomizationReward.value = customizationData.id;
        chestRewards.Add(currentChestCustomizationReward);
        
        Random rng = new Random();
        chestRewards = chestRewards.OrderBy(x => rng.Next()).ToList();

        for (int i = 0; i < chestRewards.Count; i++)
        {
            chests[i].SetupReward(chestRewards[i]);
        }

    }
    public override void Show()
    {
        base.Show();
        
        RandomizeChests();
        
        foreach (var c in chests)
        {
            c.CloseChest();
        }
        GetKeys();
        SetupGameobjects();
    }

    public bool HasKey
    {
        get { return chestKeyQuantity > 0; }
    }

    public override void Update()
    {
        base.Update();

        var tempKeys = chestKeyQuantity;
        for (int i = 0; i < 3; i++)
        {
            if(tempKeys>0)
                chestKeys[i].color = hasKey;
            else
                chestKeys[i].color = noKey;
            tempKeys--;
        }
    }

    public bool SpendKey()
    {
        bool canSpendKey = HasKey;
        if (!canSpendKey)
            return canSpendKey;
        chestKeyQuantity--;
        if (chestKeyQuantity <= 0)
        {
            chestKeyQuantity = 0;
            SetupGameobjects();
        }
     
        return canSpendKey;
    }

    public bool HasAnyChest
    {
        get { return Enumerable.Any(chests, x => x.CanOpen); }
    }
    
    [Button]
    void SetupGameobjects()
    {
        watchAd.SetActive(!HasKey);
        noThanks.gameObject.SetActive(!HasKey);
        continueBtn.gameObject.SetActive(false);

        if (!HasKey)
        {
            noThanks.Play();
        }

        if (!HasAnyChest)
        {
            keyWidget.SetActive(false);
            watchAd.SetActive(false);
            noThanks.gameObject.SetActive(false);
            continueBtn.gameObject.SetActive(true);
        }
        
    }

    public void TryGetKeys()
    {
        AdManager.Instance.Unwrap().ShowRewardedVideo(GetKeys);
    }

    void GetKeys()
    {
        chestKeyQuantity = 3;
        SetupGameobjects();
    }

    public void NoThanks()
    {
        PachaGamesManager.instance.LoadMenuWindow();
        ChestManager.Instance.Unwrap().IncreaseChestIndexReward();
    }
}
