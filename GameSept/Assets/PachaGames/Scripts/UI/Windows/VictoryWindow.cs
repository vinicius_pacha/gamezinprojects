using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class VictoryWindow : UiWindow
{
    public static UiWindow instance;

    public Image emoticon;

    public Sprite[] availableEmoticons;

    //public GameObject watchAdBtn;
    // public GameObject noThanksBtn;
    public GameObject coinWidget;
    public ProgressionWidget progressionWidget;

    public virtual void Awake()
    {
        instance = this;
    }

    public override void Show()
    {
        base.Show();
        emoticon.sprite = availableEmoticons[Random.Range(0, availableEmoticons.Length)];
        emoticon.sprite = availableEmoticons[Random.Range(0, availableEmoticons.Length)];

        progressionWidget.Setup(LevelManager.instance.level);
        LevelManager.instance.NextLevel();
        Invoke(nameof(VibrateEmoticon), emoticon.gameObject.GetComponent<Jun_TweenRuntime>().delay);
        StartCoroutine(UpdateProgressionWidgetDelayed());
    }

    void VibrateEmoticon()
    {
        VibrationManager.Instance.Unwrap().MediumVibration();
    }

IEnumerator UpdateProgressionWidgetDelayed()
    {
        yield return new WaitForSeconds(1.2f);
        progressionWidget.Setup(LevelManager.instance.level);

    }

    public void TryGet80Coins()
    {
        AdManager.Instance.Unwrap().ShowRewardedVideo(() =>
        {
            /*
            watchAdBtn.SetActive(false);
            noThanksBtn.SetActive(false);
            coinWidget.SetActive(false);
            CoinManager.Instance.Unwrap().EarnCoins(80,NoThanks);
          
            CoinsFeedback.Instance.Unwrap().EarnCoinFromPosition(80, coinWidget.transform.position);
            /*/
        });
    }

    public void EarnCoinsFromVictory(int value)
    {

        CoinManager.Instance.Unwrap().EarnCoins(value);
        CoinsFeedback.Instance.Unwrap().EarnCoinFromPosition(value, coinWidget.transform.position);
    }

    public void NoThanks()
    {
        PachaGamesManager.instance.StartGame();
        //PachaGamesManager.instance.Reward();
    }

}
