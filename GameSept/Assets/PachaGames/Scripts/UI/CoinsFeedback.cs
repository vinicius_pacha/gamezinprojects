using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BitStrap;
using UnityEngine;

public class CoinsFeedback : BitStrap.Singleton<CoinsFeedback>
{
    public CoinsFeedbackEntry[] entries;
    public Transform finalLocation;
    public float randomForce = 1;
    public float timeToIncreaseCoin = 0.5f;
    
    [Button]
    void GetEntries()
    {
        entries = GetComponentsInChildren<CoinsFeedbackEntry>(true);
    }

    [Button]
    public void EarnCoinFromPosition(int quantity, Vector3 position)
    {
        StartCoroutine(EarnCoinFromPositionCoroutine(quantity,position));
        
    }

    IEnumerator EarnCoinFromPositionCoroutine(int quantity, Vector3 position)
    {
        CoinManager.Instance.Unwrap().enabled = false;
        float totalCoins = entries.Length;
        
        if (quantity <= 20)
        {
            totalCoins = 5;
        }
        else if (quantity < 80)
        {
            totalCoins = 10;
        }
        
        for (int i =0; i < totalCoins; i++)
        {
            entries[i].StartFeedback(position,finalLocation.position, randomForce);
            yield return  new WaitForSeconds(0.03f);
        }
    }
   

}
