using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardVideoButton : MonoBehaviour
{
    public Button rewardButton;
    public GameObject watchAdIcon;
    public GameObject loadingAdIcon;

    private void Reset()
    {
        rewardButton = GetComponent<Button>();
    }

    private void Update()
    {
        watchAdIcon.SetActive(AdManager.Instance.Unwrap().HasRewardedVideo);
        loadingAdIcon.SetActive(!AdManager.Instance.Unwrap().HasRewardedVideo);
    }
}
