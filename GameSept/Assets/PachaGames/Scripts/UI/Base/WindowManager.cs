﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowManager : MonoBehaviour {

    public UiWindow[] windows;
    public UiWindow currentWindow;
    public static WindowManager instance;

    private void Awake()
    {
        instance = this;
        foreach (UiWindow ui in windows)
        {
            ui.gameObject.SetActive(true);
        }
    }
    private void Start()
    {
        ShowWindow(InGameWindow.instance);
    }
    public void ShowWindow(UiWindow uiWindow)
    {
        if (currentWindow == uiWindow) return;
        currentWindow = uiWindow;
        foreach (UiWindow ui in windows)
        {
            if (ui.gameObject.Equals(uiWindow.gameObject))
            {
                ui.Show();
            }
            else
            {
                ui.Hide();
            }
        }
    }

}
