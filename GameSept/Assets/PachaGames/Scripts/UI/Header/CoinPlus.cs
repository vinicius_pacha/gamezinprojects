using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;

public class CoinPlus : MonoBehaviour
{
    public Jun_TweenRuntime coinTween;
    public TMP_Text coinText;
    public Color positiveColor;
    public Color negativeColor;

    private void Start()
    {
        CoinManager.Instance.Unwrap().onCoinChange.Register(CoinFeedback);
    }

    private void CoinFeedback(int value)
    {
        coinText.text = string.Format("+{0}", value);
        if (value > 0)
        {
            coinText.color = positiveColor;

        }
        else
        {
            coinText.text = string.Format("{0}", value);

            coinText.color = negativeColor;
        }
        coinTween.Play();
    }
}
