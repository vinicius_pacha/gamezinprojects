﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CustomizationManager : BitStrap.Singleton<CustomizationManager>
{
    public const string equipped = "EQUIPPED";
    public CustomizationObject[] playerCustomizationRefs;
    public CustomizationObject initalCustomizationObject;
    public List<CustomizationData> customizationData = new List<CustomizationData>();
    private EquipCustomization equipCustomization;
    public int selected = -1;

    public List<CustomizationData> OrderedCustomization
    {
        get
        {
            var order = customizationData.OrderByDescending(x => x.owned).ToList();
            order = order.OrderByDescending(x => x.isNew).ToList();
            return order;
        }
    }
    void Start()
    {
        LoadAllCustomizations();
        equipCustomization = PachaGamesManager.instance.player.GetComponent<EquipCustomization>();
        if (!PlayerPrefs.HasKey(equipped))
        {
            CustomizationData c = new CustomizationData();
            c.customizationObjectConfig = initalCustomizationObject;
            c.id = 0;
            c.owned = 1;

            EquipCustomization(c);
        }
        else
        {
            int id = PlayerPrefs.GetInt(equipped);
            EquipCustomization(customizationData[id]);
        }
    }

    public void LoadAllCustomizations()
    {
        int i = 0;
        foreach (CustomizationObject pc in playerCustomizationRefs)
        {
            CustomizationData c = new CustomizationData();
            c.name = pc.name;
            c.id = i;
            c.customizationObjectConfig = pc;
            if (PlayerPrefs.HasKey(c.customizationObjectConfig.CustomizationString))
            {
                c.owned = PlayerPrefs.GetInt(c.customizationObjectConfig.CustomizationString);
            }
            else
            {
                PlayerPrefs.SetInt(c.customizationObjectConfig.CustomizationString,0);
                c.owned = 0;
            }
            if (i == 0) c.owned = 1;
            customizationData.Add(c);
            i++;
        }
    }

    public CustomizationData BuyCustomization()
    {
        var notOwnedCustomizations = customizationData.Where(x => x.owned == 0 && !x.isDailyReward).ToList();
        int rand = Random.Range(0, notOwnedCustomizations.Count());
        Debug.Log(notOwnedCustomizations[rand].id);
        var customization = customizationData.First(x => x.id == notOwnedCustomizations[rand].id);
        var customizationID = customizationData.IndexOf(customization);
        customization.owned = 1;
        PlayerPrefs.SetInt(customization.customizationObjectConfig.CustomizationString, 1);
        customization.isNew = true;

        return customization;
    }

    public CustomizationData AquireCustomization(int index, bool showPopup)
    {
        var customization = customizationData.First(x => x.id == index);
        var customizationID = customizationData.IndexOf(customization);
        if(customization.owned == 0)
        {
            customization.isNew = true;
            customization.owned = 1;
        }
        PlayerPrefs.SetInt(customization.customizationObjectConfig.CustomizationString,1);

        if (showPopup)
        {
            ((NewItemPopup.instance) as NewItemPopup).Setup(index);
            PopupManager.instance.AddPopupToQueue(NewItemPopup.instance);
        }
        return customization;
    }

    public void EquipCustomization(CustomizationData c)
    {
        if (c.owned == 0) return;

        PlayerPrefs.SetInt(equipped, c.id);
        selected = c.id;
        if (equipCustomization)
        {
            equipCustomization.EquipPlayerCustomization(c);
            customizationData[c.id].isNew = false;
        }
    }
    
    public CustomizationClass horse;
    [BitStrap.Button]
    public void GetCategoriesFromHorse()
    {
        GetCategoriesByClass(horse);
    }
    public CustomizationCategory[] GetCategoriesByClass(CustomizationClass customizationClass)
    {
        var a = customizationData.Where(x=> x.customizationObjectConfig.customizationClass == customizationClass).Select(x => x.customizationObjectConfig.category).Distinct().ToList();
        CustomizationCategory[] newArray = a.OrderBy((x) => x.priority).ToArray();
       
        return newArray;
    }

 
    
    public CustomizationClass[] GetAllClassesForChest()
    {
        var a = customizationData.Where(x=> x.owned<1 && x.customizationObjectConfig.category.unlockType == CustomizationCategory.UnlockType.Chest).Select(x => x.customizationObjectConfig.customizationClass).Distinct().ToList();
        CustomizationClass[] newArray = a.OrderBy((x) => x.priority).ToArray();
        return newArray;
    }
    
    
    public CustomizationData[] GetCustomizationsByCategoriesAndByClass(CustomizationCategory customizationCategory, CustomizationClass customizationClass)
    {
        var a = customizationData.Where(x => x.customizationObjectConfig.category == customizationCategory && x.customizationObjectConfig.customizationClass == customizationClass).ToArray();
        
        return a;
    }

    
}
