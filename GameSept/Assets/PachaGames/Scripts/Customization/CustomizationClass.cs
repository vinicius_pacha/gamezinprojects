using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomizationClass : ScriptableObject
{
    public string name;
    public int priority;
}
