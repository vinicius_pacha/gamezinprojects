using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateUIPosition : MonoBehaviour
{
    public Transform worldObject;
    public Vector2 offset;
    public RectTransform canvasRect;
    void Update()
    {
        RectTransform uiElement = transform as RectTransform;

        Vector2 viewportPosition=Camera.main.WorldToViewportPoint(worldObject.position);
        Vector2 worldObjectScreenPosition=new Vector2(
            ((viewportPosition.x*canvasRect.sizeDelta.x)-(canvasRect.sizeDelta.x*0.5f)),
            ((viewportPosition.y*canvasRect.sizeDelta.y)-(canvasRect.sizeDelta.y*0.5f)));
 
        uiElement.anchoredPosition=worldObjectScreenPosition + offset;
    }
}
