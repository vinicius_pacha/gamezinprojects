using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TMPColorAnimation : MonoBehaviour
{
    public TMP_Text text;
    public Color from;
    public Color to;
    public float timeToAnimate = 1;
    private float t = 0;

    private int direction = 1;
    // Update is called once per frame
    void Update()
    {
        t += (Time.deltaTime * 2 * direction)/ timeToAnimate;
        if (t >= 1)
        {
            direction = -1;
        }
        else if(t<=0)
        {
            direction = 1;
        }
        
        text.color = Color.Lerp(from, to, t);
    }

    private void Reset()
    {
        text = GetComponent<TMP_Text>();
        from = Color.white;
        to = Color.white;
        to.a = 0;
    }
}
