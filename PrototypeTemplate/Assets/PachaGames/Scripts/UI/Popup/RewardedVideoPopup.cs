using System;
using UnityEngine;
public class RewardedVideoPopup : UiPopup {

    public static UiPopup instance;
    public virtual void Awake()
    {
        instance = this;
    }
    
    public override void Close()
    {
        base.Close();
        AdManager.Instance.Unwrap().Fail();

    }

    public void Fail()
    {
        Close();
    }
    
    public void Success()
    {
        AdManager.Instance.Unwrap().GetReward();
        Close();
    }
}