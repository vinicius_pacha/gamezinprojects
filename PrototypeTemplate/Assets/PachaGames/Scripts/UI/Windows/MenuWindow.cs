using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuWindow : UiWindow {

    public static UiWindow instance;
    public virtual void Awake()
    {
        instance = this;
    }

    public void StartGame()
    {
        PachaGamesManager.instance.StartGame();
    }
    public override void Show()
    {
        base.Show();
    }
    
}
