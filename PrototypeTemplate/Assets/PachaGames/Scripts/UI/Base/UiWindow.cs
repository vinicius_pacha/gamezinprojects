﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
[RequireComponent(typeof(CanvasGroup))]
public class UiWindow : MonoBehaviour {

    public BitStrap.SafeAction onShow = new BitStrap.SafeAction();
    public BitStrap.SafeAction onHide = new BitStrap.SafeAction();
    public CanvasGroup canvasGroup;
    public Jun_TweenRuntime[] tweens;

    public bool hide;
    public float hideSpeed = 10;
    
    [BitStrap.Button]
    public void GetTweens()
    {
        tweens = transform.GetComponentsInChildren<Jun_TweenRuntime>();
    }

    [BitStrap.Button]
    private void Reset()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    public void Play()
    {
        foreach (var t in tweens)
        {
            t.PlayAtTime(0);
            t.Play();
        }
    }
    [BitStrap.Button]
    public void Rewind()
    {
        foreach (var t in tweens)
        {
            t.Rewind();
        }    
    }
    public virtual void Hide()
    {
        hide = true;
        onHide.Call();
    }
    public virtual void Show()
    {
        canvasGroup.alpha = 1;
        hide = false;
        gameObject.SetActive(true);
        Play();
        onShow.Call();
    }

    public virtual void Update()
    {
        if (hide)
        {
            canvasGroup.alpha -= Time.unscaledDeltaTime * hideSpeed;
            if (canvasGroup.alpha <= 0)
            {
                gameObject.SetActive(false);
            }
        }
        
    }
}
