using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class CoinsFeedbackEntry : MonoBehaviour
{
    public float maxDistance = 1;
    public Vector3 velocity;
    public Vector3 finalPosition;
    public bool animate = false;

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public  void StartFeedback(Vector3 from,Vector3 to, float randomForce)
    {
        animate = true;
        transform.position = from;
        finalPosition = to;
        gameObject.SetActive(true);
        velocity = randomForce * Random.Range(0.5f,1.5f) * UnityEngine.Random.insideUnitCircle;
        finalPosition = CoinsFeedbackFinalLocation.Instance.Unwrap().transform.position;
    }

    private void Update()
    {
        if (animate)
        {
            var desiredDirection = finalPosition - transform.position;
            
            transform.position += velocity * Time.deltaTime;

            transform.position = Vector3.MoveTowards(transform.position, finalPosition, (Time.deltaTime * maxDistance)/ Mathf.Max(velocity.magnitude,1));
            velocity = velocity * 0.8f;
            
        }

        if (Vector3.Distance(transform.position, finalPosition) <= 0.5f)
        {
            CoinManager.Instance.Unwrap().enabled = true;
            animate = false;
            gameObject.SetActive(false);
        }
    }
}
