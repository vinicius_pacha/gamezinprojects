﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProgressionWidget : MonoBehaviour
{
    public enum ProgressionState
    {
        Completed,
        Current,
        Incompleted
    }
    public ProgressionWidgetEntry[] progressionEntries;

    public Color completed;
    public Color current;
    public Color incompleted;
    public TMP_Text previousLevel;
    public TMP_Text nextLevel;
    public float speed = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public int currentIndex;
    [Button]
    public void Setup(int level)
    {
        var previous = (level / 10 ) * 10;
        currentIndex = level % 10;
        var next = (level / 10 + 1) *10;
        previousLevel.text = previous.ToString();
        nextLevel.text = next.ToString();
        int i = 0;
        foreach (var pe in progressionEntries)
        {
            if (i < currentIndex)
                pe.state = ProgressionState.Completed;
            else if (i == currentIndex)
                pe.state = ProgressionState.Current;
            else
            {
                pe.state = ProgressionState.Incompleted;
            }
            i++;
        }
    }
    private void Update()
    {
        foreach (var entry in progressionEntries)
        {
            switch (entry.state)
            {
                case ProgressionState.Completed:
                    entry.transform.localScale = Vector3.Lerp(entry.transform.localScale,Vector3.one,Time.deltaTime*speed);
                    entry.sprite.color = Color.Lerp(entry.sprite.color,completed,Time.deltaTime*speed);
                    (entry.transform as RectTransform).sizeDelta = new Vector2(50,70);
                    break;
                case ProgressionState.Current:
                    var desiredScale = Vector3.one + Mathf.Sin(Time.time*speed*4) * 0.1f * Vector3.one;
                    var t = 0.5f + Mathf.Sin(Time.time*speed*4) * 0.5f;
                    var desiredColor = Color.Lerp(current, Color.white, t);
                    entry.transform.localScale = Vector3.Lerp(entry.transform.localScale,desiredScale,Time.deltaTime*speed*4);
                    entry.sprite.color = Color.Lerp(entry.sprite.color,desiredColor,Time.deltaTime*speed*4);
                    (entry.transform as RectTransform).sizeDelta = new Vector2(50,100);

                    break;
                case ProgressionState.Incompleted:
                    entry.transform.localScale = Vector3.Lerp(entry.transform.localScale,Vector3.one,Time.deltaTime*speed);
                    entry.sprite.color = Color.Lerp(entry.sprite.color,incompleted,Time.deltaTime*speed);
                    (entry.transform as RectTransform).sizeDelta = new Vector2(50,70);

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
