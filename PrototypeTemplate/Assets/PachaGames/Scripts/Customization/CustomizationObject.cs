﻿using System;
using System.Collections;
using System.Collections.Generic;
using BitStrap;
using UnityEditor;
using UnityEngine;

public class CustomizationObject : ScriptableObject
{
    public string name;
    public Sprite customizationSprite;
   
    public CustomizationCategory category;
    public string guid;
    public CustomizationClass customizationClass;
    public string CustomizationString
    {
        get { return guid.ToString(); }
    }

    public virtual void EquipCustomization()
    {
        
    }

    [Button]
    public void GetCode()
    {
        long instanceID;
        AssetDatabase.TryGetGUIDAndLocalFileIdentifier(this,out guid,out instanceID);
    }

    private void Reset()
    {
        long instanceID;
        AssetDatabase.TryGetGUIDAndLocalFileIdentifier(this,out guid,out instanceID);
    }

    private void OnValidate()
    {
        long instanceID;

        AssetDatabase.TryGetGUIDAndLocalFileIdentifier(this,out guid,out instanceID);
    }
}
