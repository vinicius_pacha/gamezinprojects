﻿[System.Serializable]
public class CustomizationData
{
    public string name;
    public CustomizationObject customizationObjectConfig;
    public int id;
    public int owned;
    public bool isDailyReward;
    public bool isNew;
}