﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.XR;

public class PlayerControl : MonoBehaviour
{
    private Vector3 lastMousePoition;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 deltaPosition = Input.mousePosition - lastMousePoition;

        if (Input.anyKeyDown)
        {
            lastMousePoition = Input.mousePosition;
            
        }

        if (Input.anyKey)
        {
            var tempPos = transform.position;
            tempPos.x += deltaPosition.x * Time.deltaTime * speed;
            tempPos.x = Mathf.Clamp(tempPos.x, -2, 2);
            transform.position = tempPos;
        }
        lastMousePoition = Input.mousePosition;
    }

}
