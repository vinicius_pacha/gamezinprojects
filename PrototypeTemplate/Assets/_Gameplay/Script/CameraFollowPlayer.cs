﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    public Transform target;

    private Vector3 initialPosition;
    private Vector3 targetInitialPosition;
    public float speed = 5f;
    // Start is called before the first frame update
    void Start()
    {
        targetInitialPosition = target.position;
        initialPosition = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        var deltaPosition = target.position - targetInitialPosition;
        deltaPosition.x = 0;
        var desiredPosition = initialPosition + deltaPosition;
        
            
        transform.position = Vector3.Lerp(transform.position,desiredPosition,Time.deltaTime*speed);
    }
}
