﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColor : MonoBehaviour
{
    public Renderer rend;
    public Renderer rend2;
    public TrailRenderer trail;
    public TrailRenderer trail2;
    public Animator playerAnimator;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ColorChanger"))
        {
            ColorChanger cc = other.gameObject.GetComponent < ColorChanger>();
            ChangeColor(cc);
        }
    }

    void ChangeColor(ColorChanger cc)
    {
        rend.material = cc.mat;
        rend2.material = cc.mat;
        var c = cc.mat.GetColor("_LightTint");
        c.a = 1;
        trail.startColor = c;
        trail2.startColor = c;
        trail2.endColor = c;
        c.a = 0;
        trail.endColor = c;
        playerAnimator.SetTrigger("ChangeColor");
    }
}
