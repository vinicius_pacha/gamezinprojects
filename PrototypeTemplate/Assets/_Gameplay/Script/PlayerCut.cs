﻿using EzySlice;
using UnityEngine;

public class PlayerCut : MonoBehaviour
{
    public Transform hand;
    public GameObject plane;
    public float force = 1;
    public Material mat;
    public Vector3 offset;

    public Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var tempPosition = plane.transform.position;
        tempPosition.y = hand.position.y;
        plane.transform.position = tempPosition;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cut"))
        {
            if (Random.Range(0, 100) < 50)
            {
                anim.SetTrigger("Slash1");
            }
            else
            {
                anim.SetTrigger("Slash2");
            }

            float randomAngle = Random.Range(75, 290);
            plane.transform.rotation = Quaternion.Euler(0,0,randomAngle);
            var objs = other.gameObject.SliceInstantiate(plane.transform.position,plane.transform.up,mat);
            other.gameObject.SetActive(false);
            foreach (var o in objs)
            {
                o.tag = "Untagged";
                var col = o.AddComponent<MeshCollider>();
                col.convex = true;
                var rbd = o.gameObject.AddComponent<Rigidbody>();
                rbd.useGravity = true;
                rbd.AddExplosionForce(force, other.transform.position + offset, 10);
                //rbd.AddTorque(UnityEngine.Random.insideUnitSphere,ForceMode.Impulse);

            }
        }
    }
}
